﻿using LicenseOperations.Utilities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Linq;

namespace LicenseGenerator
{
    public partial class LicenseGeneratorMainFrm : Form
    {
        private string SelectedSoftware = string.Empty;
        public LicenseGeneratorMainFrm()
        {
            InitializeComponent();
        }

        #region Private

        private void LicenseGeneratorMainFrm_Load(object sender, EventArgs e)
        {
            try
            {
                this.GetSoftwaresList();
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, "Error Occured " + ex.Message, "Error", MessageBoxButtons.OK);
            }
        }

        private void BtnCreateNewLicense_Click(object sender, EventArgs e)
        {
            try
            {
                CreateEditLicenseFrm createEidtLicense = new CreateEditLicenseFrm("0");
                createEidtLicense.ShowDialog();
                this.GetLicensesList();
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, "Error Occured " + ex.Message, "Error", MessageBoxButtons.OK);
            }
        }

        private void ComboBoxSoftware_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                this.GetLicensesList();

            }
            catch (Exception ex)
            {
                MessageBox.Show(this, "Error Occured " + ex.Message, "Error", MessageBoxButtons.OK);
            }

        }

        private void GrdViewExistingLicenses_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex < 7)
                    return;
                else
                {
                    if (e.ColumnIndex == 7)
                    {
                        CreateEditLicenseFrm createEidtLicense = new CreateEditLicenseFrm(this.GrdExistingLicenses.Rows[e.RowIndex].Cells[0].Value.ToString());
                        createEidtLicense.ShowDialog();
                        this.GetLicensesList();
                    }
                    else
                    {
                        var result = MessageBox.Show(this, "Are you sure you want to delete the License?", "Confirmation", MessageBoxButtons.YesNo);
                        if (result == System.Windows.Forms.DialogResult.Yes)
                        {
                            LicenseOperations.Operations operations = new LicenseOperations.Operations();
                            operations.DeleteLicense(Constants.LicensesFileXMLPath, this.GrdExistingLicenses.Rows[e.RowIndex].Cells[0].Value.ToString());
                            this.GetLicensesList();
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, "Error Occured " + ex.Message, "Error", MessageBoxButtons.OK);
            }
        }

        private void btnAddEditSoftware_Click(object sender, EventArgs e)
        {
            try
            {
                SelectedSoftware = this.ComboBoxSoftware.SelectedValue.ToString();
                SoftwareListFrm createEditSoftware = new SoftwareListFrm();
                createEditSoftware.ShowDialog();
                this.GetSoftwaresList();
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, "Error Occured " + ex.Message, "Error", MessageBoxButtons.OK);
            }
        }

        private void GetSoftwaresList()
        {
            XDocument XmlDoc;
            using (StreamReader streamReader = new StreamReader(Constants.SoftwareFileXMLPath))
            {
                XmlDoc = XDocument.Parse(PGPEncryptDecrypt.DecryptPgpData(streamReader.ReadToEnd()));
            }
            var list = XmlDoc.Descendants("software").Select(x => new
            {
                Name = x.Element("name").Value,
                ID = x.Attribute("id").Value
            }).ToList();
            list.Insert(0, new { Name = " - - - Select Software - - -", ID = "-1" });

            BindingSource source = new BindingSource();
            source.DataSource = list;
            ComboBoxSoftware.DataSource = source;
            source.ResetBindings(false);
            this.ComboBoxSoftware.Refresh();
            try
            {
                if (list.FirstOrDefault(a => a.ID.Equals(SelectedSoftware)) != null)
                {
                    if (!string.IsNullOrEmpty(SelectedSoftware))
                        this.ComboBoxSoftware.SelectedValue = SelectedSoftware;
                }
            }
            catch (Exception ex) { throw ex; }

        }

        private void GetLicensesList()
        {
            try
            {
                if (this.ComboBoxSoftware.SelectedValue != null && !this.ComboBoxSoftware.SelectedValue.Equals("-1"))
                {

                    XDocument XmlDoc;
                    using (StreamReader streamReader = new StreamReader(Constants.LicensesFileXMLPath))
                    {
                        XmlDoc = XDocument.Parse(PGPEncryptDecrypt.DecryptPgpData(streamReader.ReadToEnd()));
                    }


                    Constants.SoftwareId = this.ComboBoxSoftware.SelectedValue.ToString();
                    Constants.SoftwareName = this.ComboBoxSoftware.Text.ToString();
                    var licenses = XmlDoc.Element("licenses")
                     .Elements("license")
                     .Where(item => item.Element("softwareid").Value == this.ComboBoxSoftware.SelectedValue.ToString()).Select(x => new
                     {

                         Licenseid = x.Attribute("id").Value,
                         //LicenseName = x.Element("licensename").Value,
                         Trial = (x.Element("istrial").Value.Equals("true")) ? "Yes" : "No",
                         SoftwareName = x.Element("softwarename").Value,
                         ClientName = x.Element("clientname").Value,
                         ExpirationDate = x.Element("expirationdate").Value,
                         MachineId = x.Element("machineid").Value,
                         NumberOfUsers = (x.Element("numberofusers").Value.Equals("-1")) ? "Unlimited" : x.Element("numberofusers").Value,
                         Edit = "Edit",
                         Delete = "Delete"

                     }).ToList();
                    if (licenses.Count > 0)
                    {
                        this.LblExistingLicenseMessage.Visible = false;
                        this.GrdExistingLicenses.Visible = true;
                        BindingSource source = new BindingSource();
                        source.DataSource = licenses;
                        GrdExistingLicenses.DataSource = source;
                        source.ResetBindings(false);
                        this.GrdExistingLicenses.Refresh();
                    }
                    else
                    {
                        this.LblExistingLicenseMessage.Visible = true;
                        this.GrdExistingLicenses.Visible = false;

                    }
                    this.BtnCreateNewLicense.Enabled = true;
                    this.BtnCreateNewLicense.BackColor = Color.PaleGreen;
                }
                else
                {
                    this.BtnCreateNewLicense.Enabled = false;
                    this.BtnCreateNewLicense.BackColor = Color.WhiteSmoke;
                    this.GrdExistingLicenses.Visible = false;
                    this.LblExistingLicenseMessage.Visible = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion
    }
}
