﻿namespace LicenseGenerator
{
    partial class SoftwareListFrm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.btnAddSoftware = new System.Windows.Forms.Button();
            this.LblExistingSoftwareMessage = new System.Windows.Forms.Label();
            this.LblExistingSoftwares = new System.Windows.Forms.Label();
            this.GrdExistingSoftwares = new System.Windows.Forms.DataGridView();
            this.id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SoftwareName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Edit = new System.Windows.Forms.DataGridViewLinkColumn();
            this.Delete = new System.Windows.Forms.DataGridViewLinkColumn();
            this.BtnCancel = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.GrdExistingSoftwares)).BeginInit();
            this.SuspendLayout();
            // 
            // btnAddSoftware
            // 
            this.btnAddSoftware.BackColor = System.Drawing.Color.Cyan;
            this.btnAddSoftware.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.btnAddSoftware.Location = new System.Drawing.Point(367, 14);
            this.btnAddSoftware.Margin = new System.Windows.Forms.Padding(5);
            this.btnAddSoftware.Name = "btnAddSoftware";
            this.btnAddSoftware.Size = new System.Drawing.Size(195, 37);
            this.btnAddSoftware.TabIndex = 0;
            this.btnAddSoftware.Text = "Add Software";
            this.btnAddSoftware.UseVisualStyleBackColor = false;
            this.btnAddSoftware.Click += new System.EventHandler(this.btnAddSoftware_Click);
            // 
            // LblExistingSoftwareMessage
            // 
            this.LblExistingSoftwareMessage.AutoSize = true;
            this.LblExistingSoftwareMessage.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblExistingSoftwareMessage.Location = new System.Drawing.Point(196, 167);
            this.LblExistingSoftwareMessage.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LblExistingSoftwareMessage.Name = "LblExistingSoftwareMessage";
            this.LblExistingSoftwareMessage.Size = new System.Drawing.Size(146, 20);
            this.LblExistingSoftwareMessage.TabIndex = 19;
            this.LblExistingSoftwareMessage.Text = "No Software found!";
            // 
            // LblExistingSoftwares
            // 
            this.LblExistingSoftwares.AutoSize = true;
            this.LblExistingSoftwares.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.LblExistingSoftwares.Location = new System.Drawing.Point(12, 43);
            this.LblExistingSoftwares.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblExistingSoftwares.Name = "LblExistingSoftwares";
            this.LblExistingSoftwares.Size = new System.Drawing.Size(89, 17);
            this.LblExistingSoftwares.TabIndex = 17;
            this.LblExistingSoftwares.Text = "Software List";
            // 
            // GrdExistingSoftwares
            // 
            this.GrdExistingSoftwares.AllowUserToAddRows = false;
            this.GrdExistingSoftwares.AllowUserToDeleteRows = false;
            this.GrdExistingSoftwares.AllowUserToResizeColumns = false;
            this.GrdExistingSoftwares.AllowUserToResizeRows = false;
            this.GrdExistingSoftwares.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.GrdExistingSoftwares.BackgroundColor = System.Drawing.SystemColors.ControlDarkDark;
            this.GrdExistingSoftwares.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.GrdExistingSoftwares.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.id,
            this.SoftwareName,
            this.Edit,
            this.Delete});
            this.GrdExistingSoftwares.Location = new System.Drawing.Point(13, 65);
            this.GrdExistingSoftwares.Margin = new System.Windows.Forms.Padding(4);
            this.GrdExistingSoftwares.MultiSelect = false;
            this.GrdExistingSoftwares.Name = "GrdExistingSoftwares";
            this.GrdExistingSoftwares.ReadOnly = true;
            this.GrdExistingSoftwares.RowHeadersVisible = false;
            this.GrdExistingSoftwares.Size = new System.Drawing.Size(549, 355);
            this.GrdExistingSoftwares.TabIndex = 20;
            this.GrdExistingSoftwares.Visible = false;
            this.GrdExistingSoftwares.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.GrdExistingSoftwares_CellContentClick);
            // 
            // id
            // 
            this.id.DataPropertyName = "id";
            this.id.HeaderText = "Software Id";
            this.id.Name = "id";
            this.id.ReadOnly = true;
            this.id.Visible = false;
            // 
            // SoftwareName
            // 
            this.SoftwareName.DataPropertyName = "SoftwareName";
            this.SoftwareName.HeaderText = "Software Name";
            this.SoftwareName.Name = "SoftwareName";
            this.SoftwareName.ReadOnly = true;
            // 
            // Edit
            // 
            this.Edit.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.Edit.DataPropertyName = "Edit";
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.Edit.DefaultCellStyle = dataGridViewCellStyle1;
            this.Edit.HeaderText = "Edit";
            this.Edit.Name = "Edit";
            this.Edit.ReadOnly = true;
            this.Edit.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Edit.ToolTipText = "Edit License";
            this.Edit.Width = 38;
            // 
            // Delete
            // 
            this.Delete.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.Delete.DataPropertyName = "Delete";
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.Delete.DefaultCellStyle = dataGridViewCellStyle2;
            this.Delete.HeaderText = "Delete";
            this.Delete.Name = "Delete";
            this.Delete.ReadOnly = true;
            this.Delete.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Delete.ToolTipText = "Delete the License";
            this.Delete.Width = 55;
            // 
            // BtnCancel
            // 
            this.BtnCancel.BackColor = System.Drawing.Color.OrangeRed;
            this.BtnCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.BtnCancel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.BtnCancel.Location = new System.Drawing.Point(439, 433);
            this.BtnCancel.Margin = new System.Windows.Forms.Padding(5);
            this.BtnCancel.Name = "BtnCancel";
            this.BtnCancel.Size = new System.Drawing.Size(123, 37);
            this.BtnCancel.TabIndex = 1;
            this.BtnCancel.Text = "Close";
            this.BtnCancel.UseVisualStyleBackColor = false;
            this.BtnCancel.Click += new System.EventHandler(this.BtnCancel_Click);
            // 
            // SoftwareListFrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(576, 486);
            this.Controls.Add(this.BtnCancel);
            this.Controls.Add(this.GrdExistingSoftwares);
            this.Controls.Add(this.btnAddSoftware);
            this.Controls.Add(this.LblExistingSoftwareMessage);
            this.Controls.Add(this.LblExistingSoftwares);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "SoftwareListFrm";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Software List";
            this.Load += new System.EventHandler(this.SoftwareListFrm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.GrdExistingSoftwares)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnAddSoftware;
        private System.Windows.Forms.Label LblExistingSoftwareMessage;
        private System.Windows.Forms.Label LblExistingSoftwares;
        private System.Windows.Forms.DataGridView GrdExistingSoftwares;
        private System.Windows.Forms.DataGridViewTextBoxColumn id;
        private System.Windows.Forms.DataGridViewTextBoxColumn SoftwareName;
        private System.Windows.Forms.DataGridViewLinkColumn Edit;
        private System.Windows.Forms.DataGridViewLinkColumn Delete;
        private System.Windows.Forms.Button BtnCancel;
    }
}