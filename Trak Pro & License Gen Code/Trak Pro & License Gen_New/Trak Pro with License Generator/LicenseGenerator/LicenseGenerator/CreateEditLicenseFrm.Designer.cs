﻿namespace LicenseGenerator
{
    partial class CreateEditLicenseFrm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.TextBoxMachineId = new System.Windows.Forms.TextBox();
            this.MachineId = new System.Windows.Forms.Label();
            this.maskedTextBoxPhone = new System.Windows.Forms.MaskedTextBox();
            this.labelPhone = new System.Windows.Forms.Label();
            this.textBoxEmail = new System.Windows.Forms.TextBox();
            this.labelEmail = new System.Windows.Forms.Label();
            this.textBoxLastName = new System.Windows.Forms.TextBox();
            this.labelLastName = new System.Windows.Forms.Label();
            this.textBoxFirstName = new System.Windows.Forms.TextBox();
            this.labelFirstName = new System.Windows.Forms.Label();
            this.RadioBtnTrialNo = new System.Windows.Forms.RadioButton();
            this.RadioBtnTrialYes = new System.Windows.Forms.RadioButton();
            this.LblTrial = new System.Windows.Forms.Label();
            this.dtPickerExpirationDate = new System.Windows.Forms.DateTimePicker();
            this.PanelModules = new System.Windows.Forms.Panel();
            this.BtnCancel = new System.Windows.Forms.Button();
            this.BtnGenerateNewLicense = new System.Windows.Forms.Button();
            this.TextBoxNumberOfUsers = new System.Windows.Forms.TextBox();
            this.TextBoxClientName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.LblExpirationDate = new System.Windows.Forms.Label();
            this.LblNumberOfUsers = new System.Windows.Forms.Label();
            this.LblClientName = new System.Windows.Forms.Label();
            this.errorProviderCreateEditLicense = new System.Windows.Forms.ErrorProvider(this.components);
            this.toolTipCreateEditLicense = new System.Windows.Forms.ToolTip(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProviderCreateEditLicense)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.TextBoxMachineId);
            this.groupBox1.Controls.Add(this.MachineId);
            this.groupBox1.Controls.Add(this.maskedTextBoxPhone);
            this.groupBox1.Controls.Add(this.labelPhone);
            this.groupBox1.Controls.Add(this.textBoxEmail);
            this.groupBox1.Controls.Add(this.labelEmail);
            this.groupBox1.Controls.Add(this.textBoxLastName);
            this.groupBox1.Controls.Add(this.labelLastName);
            this.groupBox1.Controls.Add(this.textBoxFirstName);
            this.groupBox1.Controls.Add(this.labelFirstName);
            this.groupBox1.Controls.Add(this.RadioBtnTrialNo);
            this.groupBox1.Controls.Add(this.RadioBtnTrialYes);
            this.groupBox1.Controls.Add(this.LblTrial);
            this.groupBox1.Controls.Add(this.dtPickerExpirationDate);
            this.groupBox1.Controls.Add(this.PanelModules);
            this.groupBox1.Controls.Add(this.BtnCancel);
            this.groupBox1.Controls.Add(this.BtnGenerateNewLicense);
            this.groupBox1.Controls.Add(this.TextBoxNumberOfUsers);
            this.groupBox1.Controls.Add(this.TextBoxClientName);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.LblExpirationDate);
            this.groupBox1.Controls.Add(this.LblNumberOfUsers);
            this.groupBox1.Controls.Add(this.LblClientName);
            this.groupBox1.Location = new System.Drawing.Point(12, 7);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(426, 643);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "License Detail";
            // 
            // TextBoxMachineId
            // 
            this.TextBoxMachineId.Location = new System.Drawing.Point(151, 53);
            this.TextBoxMachineId.Multiline = true;
            this.TextBoxMachineId.Name = "TextBoxMachineId";
            this.TextBoxMachineId.Size = new System.Drawing.Size(255, 102);
            this.TextBoxMachineId.TabIndex = 1;
            this.TextBoxMachineId.MouseEnter += new System.EventHandler(this.TextBoxMachineId_MouseEnter);
            this.TextBoxMachineId.MouseLeave += new System.EventHandler(this.TextBoxMachineId_MouseLeave);
            this.TextBoxMachineId.Validating += new System.ComponentModel.CancelEventHandler(this.TextBoxMachineId_Validating);
            // 
            // MachineId
            // 
            this.MachineId.AutoSize = true;
            this.MachineId.Location = new System.Drawing.Point(6, 56);
            this.MachineId.Name = "MachineId";
            this.MachineId.Size = new System.Drawing.Size(76, 17);
            this.MachineId.TabIndex = 24;
            this.MachineId.Text = "Machine Id";
            // 
            // maskedTextBoxPhone
            // 
            this.maskedTextBoxPhone.Location = new System.Drawing.Point(151, 254);
            this.maskedTextBoxPhone.Mask = "(999)-000-0000";
            this.maskedTextBoxPhone.Name = "maskedTextBoxPhone";
            this.maskedTextBoxPhone.Size = new System.Drawing.Size(255, 23);
            this.maskedTextBoxPhone.TabIndex = 5;
            // 
            // labelPhone
            // 
            this.labelPhone.AutoSize = true;
            this.labelPhone.Location = new System.Drawing.Point(6, 257);
            this.labelPhone.Name = "labelPhone";
            this.labelPhone.Size = new System.Drawing.Size(49, 17);
            this.labelPhone.TabIndex = 22;
            this.labelPhone.Text = "Phone";
            // 
            // textBoxEmail
            // 
            this.textBoxEmail.Location = new System.Drawing.Point(151, 223);
            this.textBoxEmail.Name = "textBoxEmail";
            this.textBoxEmail.Size = new System.Drawing.Size(255, 23);
            this.textBoxEmail.TabIndex = 4;
            this.textBoxEmail.Validating += new System.ComponentModel.CancelEventHandler(this.textBoxEmail_Validating);
            // 
            // labelEmail
            // 
            this.labelEmail.AutoSize = true;
            this.labelEmail.Location = new System.Drawing.Point(6, 226);
            this.labelEmail.Name = "labelEmail";
            this.labelEmail.Size = new System.Drawing.Size(42, 17);
            this.labelEmail.TabIndex = 20;
            this.labelEmail.Text = "Email";
            // 
            // textBoxLastName
            // 
            this.textBoxLastName.Location = new System.Drawing.Point(151, 192);
            this.textBoxLastName.Name = "textBoxLastName";
            this.textBoxLastName.Size = new System.Drawing.Size(255, 23);
            this.textBoxLastName.TabIndex = 3;
            this.textBoxLastName.Validating += new System.ComponentModel.CancelEventHandler(this.textBoxLastName_Validating);
            // 
            // labelLastName
            // 
            this.labelLastName.AutoSize = true;
            this.labelLastName.Location = new System.Drawing.Point(6, 195);
            this.labelLastName.Name = "labelLastName";
            this.labelLastName.Size = new System.Drawing.Size(76, 17);
            this.labelLastName.TabIndex = 18;
            this.labelLastName.Text = "Last Name";
            // 
            // textBoxFirstName
            // 
            this.textBoxFirstName.Location = new System.Drawing.Point(151, 161);
            this.textBoxFirstName.Name = "textBoxFirstName";
            this.textBoxFirstName.Size = new System.Drawing.Size(255, 23);
            this.textBoxFirstName.TabIndex = 2;
            this.textBoxFirstName.Validating += new System.ComponentModel.CancelEventHandler(this.textBoxFirstName_Validating);
            // 
            // labelFirstName
            // 
            this.labelFirstName.AutoSize = true;
            this.labelFirstName.Location = new System.Drawing.Point(6, 164);
            this.labelFirstName.Name = "labelFirstName";
            this.labelFirstName.Size = new System.Drawing.Size(76, 17);
            this.labelFirstName.TabIndex = 16;
            this.labelFirstName.Text = "First Name";
            // 
            // RadioBtnTrialNo
            // 
            this.RadioBtnTrialNo.AutoSize = true;
            this.RadioBtnTrialNo.Checked = true;
            this.RadioBtnTrialNo.Location = new System.Drawing.Point(204, 352);
            this.RadioBtnTrialNo.Name = "RadioBtnTrialNo";
            this.RadioBtnTrialNo.Size = new System.Drawing.Size(44, 21);
            this.RadioBtnTrialNo.TabIndex = 9;
            this.RadioBtnTrialNo.TabStop = true;
            this.RadioBtnTrialNo.Text = "No";
            this.RadioBtnTrialNo.UseVisualStyleBackColor = true;
            // 
            // RadioBtnTrialYes
            // 
            this.RadioBtnTrialYes.AutoSize = true;
            this.RadioBtnTrialYes.Location = new System.Drawing.Point(151, 352);
            this.RadioBtnTrialYes.Name = "RadioBtnTrialYes";
            this.RadioBtnTrialYes.Size = new System.Drawing.Size(50, 21);
            this.RadioBtnTrialYes.TabIndex = 8;
            this.RadioBtnTrialYes.Text = "Yes";
            this.RadioBtnTrialYes.UseVisualStyleBackColor = true;
            // 
            // LblTrial
            // 
            this.LblTrial.AutoSize = true;
            this.LblTrial.Location = new System.Drawing.Point(6, 354);
            this.LblTrial.Name = "LblTrial";
            this.LblTrial.Size = new System.Drawing.Size(36, 17);
            this.LblTrial.TabIndex = 10;
            this.LblTrial.Text = "Trial";
            // 
            // dtPickerExpirationDate
            // 
            this.dtPickerExpirationDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtPickerExpirationDate.Location = new System.Drawing.Point(151, 318);
            this.dtPickerExpirationDate.Name = "dtPickerExpirationDate";
            this.dtPickerExpirationDate.Size = new System.Drawing.Size(113, 23);
            this.dtPickerExpirationDate.TabIndex = 7;
            // 
            // PanelModules
            // 
            this.PanelModules.AutoScroll = true;
            this.PanelModules.Location = new System.Drawing.Point(149, 391);
            this.PanelModules.Name = "PanelModules";
            this.PanelModules.Size = new System.Drawing.Size(257, 193);
            this.PanelModules.TabIndex = 8;
            // 
            // BtnCancel
            // 
            this.BtnCancel.BackColor = System.Drawing.Color.OrangeRed;
            this.BtnCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.BtnCancel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.BtnCancel.Location = new System.Drawing.Point(71, 602);
            this.BtnCancel.Margin = new System.Windows.Forms.Padding(4);
            this.BtnCancel.Name = "BtnCancel";
            this.BtnCancel.Size = new System.Drawing.Size(164, 30);
            this.BtnCancel.TabIndex = 11;
            this.BtnCancel.Text = "Cancel";
            this.BtnCancel.UseVisualStyleBackColor = false;
            this.BtnCancel.Click += new System.EventHandler(this.BtnCancel_Click);
            // 
            // BtnGenerateNewLicense
            // 
            this.BtnGenerateNewLicense.BackColor = System.Drawing.Color.PaleGreen;
            this.BtnGenerateNewLicense.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.BtnGenerateNewLicense.Location = new System.Drawing.Point(243, 602);
            this.BtnGenerateNewLicense.Margin = new System.Windows.Forms.Padding(4);
            this.BtnGenerateNewLicense.Name = "BtnGenerateNewLicense";
            this.BtnGenerateNewLicense.Size = new System.Drawing.Size(164, 30);
            this.BtnGenerateNewLicense.TabIndex = 10;
            this.BtnGenerateNewLicense.Text = "Generate License";
            this.BtnGenerateNewLicense.UseVisualStyleBackColor = false;
            this.BtnGenerateNewLicense.Click += new System.EventHandler(this.BtnGenerateNewLicense_Click);
            // 
            // TextBoxNumberOfUsers
            // 
            this.TextBoxNumberOfUsers.Location = new System.Drawing.Point(151, 286);
            this.TextBoxNumberOfUsers.Name = "TextBoxNumberOfUsers";
            this.TextBoxNumberOfUsers.Size = new System.Drawing.Size(113, 23);
            this.TextBoxNumberOfUsers.TabIndex = 6;
            this.TextBoxNumberOfUsers.Text = "-1";
            this.TextBoxNumberOfUsers.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextBoxNumberOfUsers_KeyPress);
            this.TextBoxNumberOfUsers.MouseEnter += new System.EventHandler(this.TextBoxNumberOfUsers_MouseEnter);
            this.TextBoxNumberOfUsers.MouseLeave += new System.EventHandler(this.TextBoxNumberOfUsers_MouseLeave);
            this.TextBoxNumberOfUsers.Validating += new System.ComponentModel.CancelEventHandler(this.TextBoxNumberOfUsers_Validating);
            // 
            // TextBoxClientName
            // 
            this.TextBoxClientName.Location = new System.Drawing.Point(151, 24);
            this.TextBoxClientName.Name = "TextBoxClientName";
            this.TextBoxClientName.Size = new System.Drawing.Size(255, 23);
            this.TextBoxClientName.TabIndex = 0;
            this.TextBoxClientName.MouseEnter += new System.EventHandler(this.TextBoxClientName_MouseEnter);
            this.TextBoxClientName.MouseLeave += new System.EventHandler(this.TextBoxClientName_MouseLeave);
            this.TextBoxClientName.Validating += new System.ComponentModel.CancelEventHandler(this.TextBoxClientName_Validating);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 381);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(61, 17);
            this.label2.TabIndex = 7;
            this.label2.Text = "Modules";
            // 
            // LblExpirationDate
            // 
            this.LblExpirationDate.AutoSize = true;
            this.LblExpirationDate.Location = new System.Drawing.Point(6, 323);
            this.LblExpirationDate.Name = "LblExpirationDate";
            this.LblExpirationDate.Size = new System.Drawing.Size(104, 17);
            this.LblExpirationDate.TabIndex = 7;
            this.LblExpirationDate.Text = "Expiration Date";
            // 
            // LblNumberOfUsers
            // 
            this.LblNumberOfUsers.AutoSize = true;
            this.LblNumberOfUsers.Location = new System.Drawing.Point(6, 289);
            this.LblNumberOfUsers.Name = "LblNumberOfUsers";
            this.LblNumberOfUsers.Size = new System.Drawing.Size(115, 17);
            this.LblNumberOfUsers.TabIndex = 7;
            this.LblNumberOfUsers.Text = "Number of Users";
            // 
            // LblClientName
            // 
            this.LblClientName.AutoSize = true;
            this.LblClientName.Location = new System.Drawing.Point(6, 27);
            this.LblClientName.Name = "LblClientName";
            this.LblClientName.Size = new System.Drawing.Size(84, 17);
            this.LblClientName.TabIndex = 6;
            this.LblClientName.Text = "Client Name";
            // 
            // errorProviderCreateEditLicense
            // 
            this.errorProviderCreateEditLicense.ContainerControl = this;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Blue;
            this.label1.Location = new System.Drawing.Point(6, 75);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(133, 13);
            this.label1.TabIndex = 25;
            this.label1.Text = "Enter multiple Machine Id\'s";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Blue;
            this.label3.Location = new System.Drawing.Point(6, 94);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(117, 13);
            this.label3.TabIndex = 26;
            this.label3.Text = "separated by comma (,)";
            // 
            // CreateEditLicenseFrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(457, 662);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.Name = "CreateEditLicenseFrm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Create Edit License";
            this.Load += new System.EventHandler(this.CreateEditLicense_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProviderCreateEditLicense)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox TextBoxNumberOfUsers;
        private System.Windows.Forms.TextBox TextBoxClientName;
        private System.Windows.Forms.Label LblExpirationDate;
        private System.Windows.Forms.Label LblNumberOfUsers;
        private System.Windows.Forms.Label LblClientName;
        private System.Windows.Forms.Button BtnCancel;
        private System.Windows.Forms.Button BtnGenerateNewLicense;
        private System.Windows.Forms.Panel PanelModules;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dtPickerExpirationDate;
        private System.Windows.Forms.Label LblTrial;
        private System.Windows.Forms.RadioButton RadioBtnTrialYes;
        private System.Windows.Forms.RadioButton RadioBtnTrialNo;
        private System.Windows.Forms.ErrorProvider errorProviderCreateEditLicense;
        private System.Windows.Forms.ToolTip toolTipCreateEditLicense;
        private System.Windows.Forms.Label labelPhone;
        private System.Windows.Forms.TextBox textBoxEmail;
        private System.Windows.Forms.Label labelEmail;
        private System.Windows.Forms.TextBox textBoxLastName;
        private System.Windows.Forms.Label labelLastName;
        private System.Windows.Forms.TextBox textBoxFirstName;
        private System.Windows.Forms.Label labelFirstName;
        private System.Windows.Forms.MaskedTextBox maskedTextBoxPhone;
        private System.Windows.Forms.TextBox TextBoxMachineId;
        private System.Windows.Forms.Label MachineId;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;

    }
}