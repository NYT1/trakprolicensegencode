﻿using LicenseGenerator.Model;
using LicenseOperations.Utilities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Linq;

namespace LicenseGenerator
{
    public partial class SoftwareListFrm : Form
    {
        public SoftwareListFrm()
        {
            InitializeComponent();
        }

        private void btnAddSoftware_Click(object sender, EventArgs e)
        {
            Constants.SoftwareId = "0";
            Constants.ModuleList.Clear();
            CreateEditSoftwareFrm createEidtLicense = new CreateEditSoftwareFrm();
            createEidtLicense.ShowDialog();
            this.LoadSoftwareList();
        }

        private void SoftwareListFrm_Load(object sender, EventArgs e)
        {
            try
            {
                this.LoadSoftwareList();
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, "Error Occured " + ex.Message, "Error", MessageBoxButtons.OK);
            }
        }


        private void LoadSoftwareList()
        {
            try
            {
                //  XDocument XMLDoc = XDocument.Load(Constants.SoftwareFileXMLPath);//.Concat(Environment.CurrentDirectory,"App_Data//Softwares.xml"));

                XDocument XmlDoc;
                //  using (StreamReader streamReader = new StreamReader(Directory.GetFiles(Environment.CurrentDirectory, "*.licx", SearchOption.AllDirectories)[0]))
                using (StreamReader streamReader = new StreamReader(Constants.SoftwareFileXMLPath))
                {
                    XmlDoc = XDocument.Parse(PGPEncryptDecrypt.DecryptPgpData(streamReader.ReadToEnd()));
                }
                var softwareList = XmlDoc.Element("softwares")
                 .Elements("software").Select(x => new
                 {
                     id = x.Attribute("id").Value,
                     // LicenseName = x.Element("licensename").Value,
                     SoftwareName = x.Element("name").Value,
                     //istrial = x.Element("istrial").Value.Equals("1") ? "Yes" : "No"
                     Edit = "Edit",
                     Delete = "Delete"
                 }).ToList();

                //  foreach (var item in licenseDetail)
                //  {

                if (softwareList != null && softwareList.Count > 0)
                {
                    this.GrdExistingSoftwares.Visible = true;


                    BindingSource source = new BindingSource();
                    source.DataSource = softwareList;
                    GrdExistingSoftwares.DataSource = source;
                    source.ResetBindings(false);
                    this.GrdExistingSoftwares.Refresh();

                    //this.GrdExistingSoftwares.DataSource = softwareList;
                    this.LblExistingSoftwareMessage.Visible = false;
                }
                else
                {
                    this.GrdExistingSoftwares.Visible = false;
                    this.LblExistingSoftwareMessage.Visible = true;
                }
                //List<string>  ddd = obj.Descendants("licenses")
                //    // obj.Descendants("softwares")
                //              .Elements("license")
                //              .Where(item => item.Element("licenseid").Value == this.licenseId)
                //                 // .Descendants("license")
                //                  //.Where(er => er.Parent.Name == "license")
                //                  .Select(p => p.Value)
                //                  .ToList();

                // int i = 0;
                //foreach (var item in modules)
                // {
                //  CheckBox box; if (i == 10)
                // int index = 0;
                //for ( i = 0; i < 10; i++)
                //{
                //    modules.Add("item number" + i);

                //} 
                //i = 0;
                //int innitialOffset = 20;
                //int xDistance = 200;
                //int yDistance = 50;

                //foreach (string _module in allModules)
                //{
                //    CheckBox checkBox = new CheckBox();
                //    checkBox.Tag = _module.ToString();
                //    checkBox.Text = _module;
                //    checkBox.AutoSize = true;

                //    checkBox.Location = new Point(0, index * 25); //vertical
                //    //   checkBox.Location = new Point(innitialOffset + index % 2 * xDistance, innitialOffset + index / 2 * yDistance); 

                //    foreach (string _accessiblemodule in accessibleModules)
                //    {
                //        if (_accessiblemodule.Equals(_module))
                //        {
                //            checkBox.Checked = true;
                //        }
                //    }
                //    //box.Location = new Point(i * 50, 10); //horizontal
                //    this.PanelModules.Controls.Add(checkBox);
                //    index++;

                //    //  }
                //}
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        private void GrdExistingSoftwares_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex < 2)
                    return;
                else
                {
                    if (e.ColumnIndex == 2)
                    {
                        Constants.SoftwareId = this.GrdExistingSoftwares.Rows[e.RowIndex].Cells[0].Value.ToString();
                        CreateEditSoftwareFrm createEidtLicense = new CreateEditSoftwareFrm();
                        createEidtLicense.ShowDialog();
                        this.LoadSoftwareList();
                    }
                    else
                    {
                        var result = MessageBox.Show(this, "Are you sure you want to delete the License?", "Confirmation", MessageBoxButtons.YesNo);
                        if (result == System.Windows.Forms.DialogResult.Yes)
                        {
                            LicenseOperations.Operations operations = new LicenseOperations.Operations();
                            operations.DeleteSoftwareWithLicenses(Constants.SoftwareFileXMLPath, Constants.LicensesFileXMLPath, this.GrdExistingSoftwares.Rows[e.RowIndex].Cells[0].Value.ToString());
                            this.LoadSoftwareList();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, "Error Occured " + ex.Message, "Error", MessageBoxButtons.OK);
            }
        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        //private void BtnClose_Click(object sender, EventArgs e)
        //{
        //    this.DialogResult = DialogResult.Cancel;
        //    this.Close();
        //}
    }
}
