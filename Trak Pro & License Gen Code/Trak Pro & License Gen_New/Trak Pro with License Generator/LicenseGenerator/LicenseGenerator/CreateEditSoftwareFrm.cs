﻿using LicenseGenerator.Model;
using LicenseOperations.Utilities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Linq;

namespace LicenseGenerator
{
    public partial class CreateEditSoftwareFrm : Form
    {
       
        public CreateEditSoftwareFrm()
        {
            InitializeComponent();
        }

        private void BtnAddNewSoftware_Click(object sender, EventArgs e)
        {
            try
            {
                if (!this.ValidateSoftwareName())
                    return;
                LicenseOperations.Operations operations = new LicenseOperations.Operations();
                string softwareName = this.TextBoxSoftwareName.Text.Trim();
                List<string> modules = new List<string>();
                foreach (Module item in Constants.ModuleList)
                {
                    modules.Add(item.ModuleName);
                }
                if (!Constants.SoftwareId.Equals("0"))
                {
                    if (operations.CheckSoftwareNameExists(Constants.SoftwareFileXMLPath, softwareName, Constants.SoftwareId))
                    {
                        MessageBox.Show(this, "Software already exists with this name!", "Error", MessageBoxButtons.OK);
                    }
                    else
                    {
                        if (operations.UpdateSoftware(Constants.SoftwareFileXMLPath, softwareName, modules, Constants.SoftwareId))
                            MessageBox.Show(this, "Software successfully Updated", "Success", MessageBoxButtons.OK);
                        else
                            MessageBox.Show(this, "Error while Updating Software!", "Error", MessageBoxButtons.OK);
                        this.Close();
                    }
                }
                else
                {
                    if (operations.CheckSoftwareNameExists(Constants.SoftwareFileXMLPath, softwareName))
                    {
                        MessageBox.Show(this, "Software already exists with this name!", "Error", MessageBoxButtons.OK);
                    }
                    else
                    {
                        if (operations.AddNewSoftware(Constants.SoftwareFileXMLPath, softwareName, modules))
                        {
                            MessageBox.Show(this, "New Software successfully Added", "Success", MessageBoxButtons.OK);
                        }
                        else
                            MessageBox.Show(this, "Error while Updating Software!", "Error", MessageBoxButtons.OK);
                        this.Close();
                    }

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, "Error Occured " + ex.Message, "Error", MessageBoxButtons.OK);
            }
        }

        private void CreateEditSoftware_Load(object sender, EventArgs e)
        {
            try
            {
                XDocument XmlDoc;
                using (StreamReader streamReader = new StreamReader(Constants.SoftwareFileXMLPath))
                {
                    XmlDoc = XDocument.Parse(PGPEncryptDecrypt.DecryptPgpData(streamReader.ReadToEnd()));
                }

                List<string> accessibleModules = new List<string>();
                if (!Constants.SoftwareId.Equals("0"))
                {

                    var allModules = XmlDoc.Descendants("softwares")
                                 .Elements("software")
                                 .Where(item => item.Attribute("id").Value == Constants.SoftwareId)
                                     .Descendants("modulename")
                                     .Where(er => er.Parent.Name == "modules")
                                    .Select(p => new Module { ModuleName = p.Value, Delete = "Delete" }).ToList();

                    var softwareDetail = XmlDoc.Element("softwares")
                     .Elements("software")
                     .Where(item => item.Attribute("id").Value == Constants.SoftwareId).Select(x => new
                     {
                         SoftwareName = x.Element("name").Value,
                     }).SingleOrDefault();

                    this.TextBoxSoftwareName.Text = softwareDetail.SoftwareName;
                    this.GrdModuleList.DataSource = allModules;
                    Constants.ModuleList = allModules;
                    this.BtnAddNewSoftware.Text = "Update";
                }
                else
                    this.BtnAddNewSoftware.Text = "Add";

            }
            catch (Exception ex)
            {
                MessageBox.Show(this, "Error Occured " + ex.Message, "Error", MessageBoxButtons.OK);
            }
        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BtnAddModule_Click(object sender, EventArgs e)
        {
            try
            {
                InputBoxFrm frm = new InputBoxFrm();
                frm.ShowDialog();
                var item = Constants.ModuleList.FirstOrDefault(x => x.ModuleName.ToLower() == Constants.ModuleName.ToLower());
                if (item == null)
                {
                    Constants.ModuleList.Add(new Model.Module { ModuleName = Constants.ModuleName, Delete = "Delete" });
                    BindingSource source = new BindingSource();
                    source.DataSource = Constants.ModuleList;
                    GrdModuleList.DataSource = source;
                    source.ResetBindings(false);
                    this.GrdModuleList.Refresh();
                }
                else
                {
                    MessageBox.Show(this, "Module already added", "Information", MessageBoxButtons.OK);

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, "Error Occured " + ex.Message, "Error", MessageBoxButtons.OK);
            }
        }

        private bool ValidateSoftwareName()
        {
            bool bStatus = true;
            try
            {
                if (this.TextBoxSoftwareName.Text.Trim() == "")
                {
                    errorPrvSoftwareName.SetError(this.TextBoxSoftwareName, "Please enter Software Name");
                    bStatus = false;
                }
                else
                    errorPrvSoftwareName.SetError(this.TextBoxSoftwareName, "");
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, "Error Occured " + ex.Message, "Error", MessageBoxButtons.OK);
            }
            return bStatus;
        }

        private void TextBoxSoftwareName_Validating(object sender, CancelEventArgs e)
        {
            this.ValidateSoftwareName();
        }

        private void GrdModuleList_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex < 1)
                    return;
                else
                {
                    var item = Constants.ModuleList.SingleOrDefault(x => x.ModuleName == this.GrdModuleList.Rows[e.RowIndex].Cells[0].Value.ToString());
                    if (item != null)
                        Constants.ModuleList.Remove(item);
                    BindingSource source = new BindingSource();
                    source.DataSource = Constants.ModuleList;
                    GrdModuleList.DataSource = source;
                    source.ResetBindings(false);
                    this.GrdModuleList.Refresh();

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, "Error Occured " + ex.Message, "Error", MessageBoxButtons.OK);
            }
        }

    }


}