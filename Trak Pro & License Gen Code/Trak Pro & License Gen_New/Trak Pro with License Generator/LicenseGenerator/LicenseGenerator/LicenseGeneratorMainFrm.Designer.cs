﻿namespace LicenseGenerator
{
    partial class LicenseGeneratorMainFrm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.BtnCreateNewLicense = new System.Windows.Forms.Button();
            this.ComboBoxSoftware = new System.Windows.Forms.ComboBox();
            this.LblSoftware = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnAddEditSoftware = new System.Windows.Forms.Button();
            this.LblExistingLicenseMessage = new System.Windows.Forms.Label();
            this.LblExistingLicenses = new System.Windows.Forms.Label();
            this.GrdExistingLicenses = new System.Windows.Forms.DataGridView();
            this.id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Trial = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SoftwareName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ClientName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MachineId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ExpirationDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NumberOfUsers = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Edit = new System.Windows.Forms.DataGridViewLinkColumn();
            this.Delete = new System.Windows.Forms.DataGridViewLinkColumn();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GrdExistingLicenses)).BeginInit();
            this.SuspendLayout();
            // 
            // BtnCreateNewLicense
            // 
            this.BtnCreateNewLicense.BackColor = System.Drawing.Color.PaleGreen;
            this.BtnCreateNewLicense.Enabled = false;
            this.BtnCreateNewLicense.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.BtnCreateNewLicense.Location = new System.Drawing.Point(579, 25);
            this.BtnCreateNewLicense.Margin = new System.Windows.Forms.Padding(4);
            this.BtnCreateNewLicense.Name = "BtnCreateNewLicense";
            this.BtnCreateNewLicense.Size = new System.Drawing.Size(178, 30);
            this.BtnCreateNewLicense.TabIndex = 1;
            this.BtnCreateNewLicense.Text = "Create New License";
            this.BtnCreateNewLicense.UseVisualStyleBackColor = false;
            this.BtnCreateNewLicense.Click += new System.EventHandler(this.BtnCreateNewLicense_Click);
            // 
            // ComboBoxSoftware
            // 
            this.ComboBoxSoftware.DisplayMember = "name";
            this.ComboBoxSoftware.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.ComboBoxSoftware.FormattingEnabled = true;
            this.ComboBoxSoftware.Location = new System.Drawing.Point(110, 28);
            this.ComboBoxSoftware.Margin = new System.Windows.Forms.Padding(4);
            this.ComboBoxSoftware.Name = "ComboBoxSoftware";
            this.ComboBoxSoftware.Size = new System.Drawing.Size(324, 24);
            this.ComboBoxSoftware.TabIndex = 0;
            this.ComboBoxSoftware.ValueMember = "id";
            this.ComboBoxSoftware.SelectedIndexChanged += new System.EventHandler(this.ComboBoxSoftware_SelectedIndexChanged);
            // 
            // LblSoftware
            // 
            this.LblSoftware.AutoSize = true;
            this.LblSoftware.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.LblSoftware.Location = new System.Drawing.Point(6, 32);
            this.LblSoftware.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LblSoftware.Name = "LblSoftware";
            this.LblSoftware.Size = new System.Drawing.Size(63, 17);
            this.LblSoftware.TabIndex = 4;
            this.LblSoftware.Text = "Software";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnAddEditSoftware);
            this.groupBox1.Controls.Add(this.BtnCreateNewLicense);
            this.groupBox1.Controls.Add(this.ComboBoxSoftware);
            this.groupBox1.Controls.Add(this.LblSoftware);
            this.groupBox1.Location = new System.Drawing.Point(16, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(950, 68);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            // 
            // btnAddEditSoftware
            // 
            this.btnAddEditSoftware.BackColor = System.Drawing.Color.Cyan;
            this.btnAddEditSoftware.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.btnAddEditSoftware.Location = new System.Drawing.Point(765, 25);
            this.btnAddEditSoftware.Margin = new System.Windows.Forms.Padding(4);
            this.btnAddEditSoftware.Name = "btnAddEditSoftware";
            this.btnAddEditSoftware.Size = new System.Drawing.Size(178, 30);
            this.btnAddEditSoftware.TabIndex = 2;
            this.btnAddEditSoftware.Text = "Add/Edit Software";
            this.btnAddEditSoftware.UseVisualStyleBackColor = false;
            this.btnAddEditSoftware.Click += new System.EventHandler(this.btnAddEditSoftware_Click);
            // 
            // LblExistingLicenseMessage
            // 
            this.LblExistingLicenseMessage.AutoSize = true;
            this.LblExistingLicenseMessage.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblExistingLicenseMessage.Location = new System.Drawing.Point(417, 158);
            this.LblExistingLicenseMessage.Name = "LblExistingLicenseMessage";
            this.LblExistingLicenseMessage.Size = new System.Drawing.Size(137, 20);
            this.LblExistingLicenseMessage.TabIndex = 15;
            this.LblExistingLicenseMessage.Text = "No License found!";
            // 
            // LblExistingLicenses
            // 
            this.LblExistingLicenses.AutoSize = true;
            this.LblExistingLicenses.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.LblExistingLicenses.Location = new System.Drawing.Point(13, 71);
            this.LblExistingLicenses.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LblExistingLicenses.Name = "LblExistingLicenses";
            this.LblExistingLicenses.Size = new System.Drawing.Size(116, 17);
            this.LblExistingLicenses.TabIndex = 13;
            this.LblExistingLicenses.Text = "Existing Licenses";
            // 
            // GrdExistingLicenses
            // 
            this.GrdExistingLicenses.AllowUserToAddRows = false;
            this.GrdExistingLicenses.AllowUserToDeleteRows = false;
            this.GrdExistingLicenses.AllowUserToResizeColumns = false;
            this.GrdExistingLicenses.AllowUserToResizeRows = false;
            this.GrdExistingLicenses.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.GrdExistingLicenses.BackgroundColor = System.Drawing.SystemColors.ControlDarkDark;
            this.GrdExistingLicenses.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.GrdExistingLicenses.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.id,
            this.Trial,
            this.SoftwareName,
            this.ClientName,
            this.MachineId,
            this.ExpirationDate,
            this.NumberOfUsers,
            this.Edit,
            this.Delete});
            this.GrdExistingLicenses.EnableHeadersVisualStyles = false;
            this.GrdExistingLicenses.Location = new System.Drawing.Point(12, 92);
            this.GrdExistingLicenses.MultiSelect = false;
            this.GrdExistingLicenses.Name = "GrdExistingLicenses";
            this.GrdExistingLicenses.ReadOnly = true;
            this.GrdExistingLicenses.RowHeadersVisible = false;
            this.GrdExistingLicenses.Size = new System.Drawing.Size(954, 408);
            this.GrdExistingLicenses.TabIndex = 14;
            this.GrdExistingLicenses.Visible = false;
            this.GrdExistingLicenses.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.GrdViewExistingLicenses_CellContentClick);
            // 
            // id
            // 
            this.id.DataPropertyName = "Licenseid";
            this.id.HeaderText = "License ID";
            this.id.Name = "id";
            this.id.ReadOnly = true;
            // 
            // Trial
            // 
            this.Trial.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.Trial.DataPropertyName = "Trial";
            this.Trial.HeaderText = "Trial";
            this.Trial.Name = "Trial";
            this.Trial.ReadOnly = true;
            this.Trial.Width = 61;
            // 
            // SoftwareName
            // 
            this.SoftwareName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.SoftwareName.DataPropertyName = "SoftwareName";
            this.SoftwareName.HeaderText = "Software Name";
            this.SoftwareName.Name = "SoftwareName";
            this.SoftwareName.ReadOnly = true;
            this.SoftwareName.Width = 129;
            // 
            // ClientName
            // 
            this.ClientName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.ClientName.DataPropertyName = "ClientName";
            this.ClientName.HeaderText = "Client Name";
            this.ClientName.Name = "ClientName";
            this.ClientName.ReadOnly = true;
            this.ClientName.Width = 109;
            // 
            // MachineId
            // 
            this.MachineId.DataPropertyName = "MachineId";
            this.MachineId.HeaderText = "Machine Id";
            this.MachineId.Name = "MachineId";
            this.MachineId.ReadOnly = true;
            // 
            // ExpirationDate
            // 
            this.ExpirationDate.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.ExpirationDate.DataPropertyName = "ExpirationDate";
            this.ExpirationDate.HeaderText = "Expiration Date";
            this.ExpirationDate.Name = "ExpirationDate";
            this.ExpirationDate.ReadOnly = true;
            this.ExpirationDate.Width = 129;
            // 
            // NumberOfUsers
            // 
            this.NumberOfUsers.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.NumberOfUsers.DataPropertyName = "NumberofUsers";
            this.NumberOfUsers.HeaderText = "Number Of Users";
            this.NumberOfUsers.Name = "NumberOfUsers";
            this.NumberOfUsers.ReadOnly = true;
            this.NumberOfUsers.Width = 143;
            // 
            // Edit
            // 
            this.Edit.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCellsExceptHeader;
            this.Edit.DataPropertyName = "Edit";
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.Edit.DefaultCellStyle = dataGridViewCellStyle1;
            this.Edit.HeaderText = "";
            this.Edit.Name = "Edit";
            this.Edit.ReadOnly = true;
            this.Edit.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Edit.ToolTipText = "Edit License";
            this.Edit.Width = 5;
            // 
            // Delete
            // 
            this.Delete.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCellsExceptHeader;
            this.Delete.DataPropertyName = "Delete";
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.Delete.DefaultCellStyle = dataGridViewCellStyle2;
            this.Delete.HeaderText = "";
            this.Delete.Name = "Delete";
            this.Delete.ReadOnly = true;
            this.Delete.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Delete.ToolTipText = "Delete the License";
            this.Delete.Width = 5;
            // 
            // LicenseGeneratorMainFrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(979, 512);
            this.Controls.Add(this.LblExistingLicenseMessage);
            this.Controls.Add(this.GrdExistingLicenses);
            this.Controls.Add(this.LblExistingLicenses);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.Name = "LicenseGeneratorMainFrm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "License Generator";
            this.Load += new System.EventHandler(this.LicenseGeneratorMainFrm_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GrdExistingLicenses)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button BtnCreateNewLicense;
        private System.Windows.Forms.ComboBox ComboBoxSoftware;
        private System.Windows.Forms.Label LblSoftware;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label LblExistingLicenseMessage;
        private System.Windows.Forms.Label LblExistingLicenses;
        private System.Windows.Forms.DataGridView GrdExistingLicenses;
        private System.Windows.Forms.Button btnAddEditSoftware;
        private System.Windows.Forms.DataGridViewTextBoxColumn id;
        private System.Windows.Forms.DataGridViewTextBoxColumn Trial;
        private System.Windows.Forms.DataGridViewTextBoxColumn SoftwareName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ClientName;
        private System.Windows.Forms.DataGridViewTextBoxColumn MachineId;
        private System.Windows.Forms.DataGridViewTextBoxColumn ExpirationDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn NumberOfUsers;
        private System.Windows.Forms.DataGridViewLinkColumn Edit;
        private System.Windows.Forms.DataGridViewLinkColumn Delete;


    }
}

