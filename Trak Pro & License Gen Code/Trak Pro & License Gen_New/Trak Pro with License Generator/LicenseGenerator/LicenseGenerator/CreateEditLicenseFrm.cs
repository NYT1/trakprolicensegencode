﻿using LicenseOperations;
using LicenseOperations.Utilities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Linq;


namespace LicenseGenerator
{
    public partial class CreateEditLicenseFrm : Form
    {
        string licenseId = string.Empty;

        public CreateEditLicenseFrm(string LicenseId)
        {
            this.licenseId = LicenseId;
            InitializeComponent();
        }

        #region Private

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void CreateEditLicense_Load(object sender, EventArgs e)
        {
            try
            {
                XDocument XmlDoc;
                using (StreamReader streamReader = new StreamReader(Constants.SoftwareFileXMLPath))
                {
                    XmlDoc = XDocument.Parse(PGPEncryptDecrypt.DecryptPgpData(streamReader.ReadToEnd()));
                }


                List<string> allModules = XmlDoc.Descendants("softwares")
                                .Elements("software")
                                .Where(item => item.Attribute("id").Value == Constants.SoftwareId)
                                    .Descendants("modulename")
                                    .Where(er => er.Parent.Name == "modules")
                                    .Select(p => p.Value)
                                    .ToList();


                List<string> accessibleModules = new List<string>();
                if (!licenseId.Equals("0"))
                {

                    using (StreamReader streamReader = new StreamReader(Constants.LicensesFileXMLPath))
                    {
                        XmlDoc = XDocument.Parse(PGPEncryptDecrypt.DecryptPgpData(streamReader.ReadToEnd()));
                    }

                    accessibleModules = XmlDoc.Descendants("licenses")
                                    .Elements("license")
                                    .Where(item => item.Attribute("id").Value == this.licenseId)
                                        .Descendants("modulename")
                                        .Where(er => er.Parent.Name == "modules")
                                        .Select(p => p.Value)
                                        .ToList();

                    var licenseDetail = XmlDoc.Element("licenses")
                     .Elements("license")
                     .Where(item => item.Attribute("id").Value == this.licenseId).Select(x => new
                     {
                         SoftwareName = x.Element("softwarename").Value,
                         ClientName = x.Element("clientname").Value,
                         MachineId = x.Element("machineid").Value,
                         ExpirationDate = x.Element("expirationdate").Value,
                         FirstName = x.Element("firstname").Value,
                         LastName = x.Element("lastname").Value,
                         Email = x.Element("email").Value,
                         Phone = x.Element("phone").Value,
                        // NumberOfClients = x.Element("numberofclients").Value,
                         NumberOfUsers = x.Element("numberofusers").Value,
                         istrial = x.Element("istrial").Value//.Equals("1") ? "Yes" : "No"
                     }).SingleOrDefault();

                    this.TextBoxClientName.Text = licenseDetail.ClientName;
                    this.TextBoxMachineId.Text = licenseDetail.MachineId;
                    this.dtPickerExpirationDate.Value = DateTime.ParseExact(licenseDetail.ExpirationDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                   //is.TextBoxMachineId.Text = licenseDetail.NumberOfClients;
                    this.TextBoxNumberOfUsers.Text = licenseDetail.NumberOfUsers;
                    this.textBoxFirstName.Text = licenseDetail.FirstName;
                    this.textBoxLastName.Text = licenseDetail.LastName;
                    this.maskedTextBoxPhone.Text = licenseDetail.Phone;
                    this.textBoxEmail.Text = licenseDetail.Email;
                    this.RadioBtnTrialYes.Checked = Convert.ToBoolean(licenseDetail.istrial);
                    //if (Convert.ToBoolean(licenseDetail.istrial))//.Equals("Yes"))
                    //{
                    //    this.RadioBtnTrialYes.Checked = true;
                    //}
                    //else
                    //{
                    //    this.RadioBtnTrialNo.Checked = true;
                    //}
                    this.BtnGenerateNewLicense.Text = "Update License";

                }
                int index = 0;
                foreach (string _module in allModules)
                {
                    CheckBox checkBox = new CheckBox();
                    checkBox.Tag = _module.ToString();
                    checkBox.Text = _module;
                    checkBox.AutoSize = true;
                    checkBox.Location = new Point(0, index * 25); //vertical
                    foreach (string _accessiblemodule in accessibleModules)
                    {
                        if (_accessiblemodule.Equals(_module))
                        {
                            checkBox.Checked = true;
                        }
                    }
                    this.PanelModules.Controls.Add(checkBox);
                    index++;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, "Error Occured " + ex.Message, "Error", MessageBoxButtons.OK);
            }
        }

        private void BtnGenerateNewLicense_Click(object sender, EventArgs e)
        {
            try
            {

                if (!this.ValidateLicenseInfo())
                    return;
                LicenseOperations.Operations operations = new LicenseOperations.Operations();

                //List<string> elements = new List<string>();
                LicenseOperations.ClientLicense clientLicense = new ClientLicense();
                clientLicense.SoftwareId = Constants.SoftwareId;
                clientLicense.SoftwareName = Constants.SoftwareName;
                clientLicense.ClientName = this.TextBoxClientName.Text.Trim();
                clientLicense.MachineId = this.TextBoxMachineId.Text.Trim();
                clientLicense.ExpirationDate = this.dtPickerExpirationDate.Value.Date.ToString("dd/MM/yyyy");
                clientLicense.FirstName = this.textBoxFirstName.Text.Trim();
                clientLicense.LastName = this.textBoxLastName.Text.Trim();
                clientLicense.Email = this.textBoxEmail.Text.Trim();
                clientLicense.Phone = this.maskedTextBoxPhone.Text.Trim();
               // clientLicense.NumberOfClients = this.TextBoxMachineId.Text.Trim();
                clientLicense.NumberOfUsers = this.TextBoxNumberOfUsers.Text.Trim();
                if (this.RadioBtnTrialYes.Checked)
                    clientLicense.IsTrial = true;
                else
                    clientLicense.IsTrial = false;
                List<string> modules = new List<string>();
                foreach (Control c in PanelModules.Controls)
                {
                    if (c is CheckBox)
                    {
                        CheckBox ch = (CheckBox)c;
                        if (ch.Checked)
                        {
                            modules.Add(ch.Text.Trim());
                        }
                    }
                }
                XElement NewLicense;
                if (!this.licenseId.Equals("0"))
                {
                    if (operations.UpdateLicense(Constants.LicensesFileXMLPath, clientLicense, modules, this.licenseId, out NewLicense))
                    {
                        this.SaveLicenseFile(NewLicense);
                        MessageBox.Show(this, "License successfully Updated", "Success", MessageBoxButtons.OK);
                        this.Close();
                    }
                    else
                        MessageBox.Show(this, "Error while Updating License!", "Error", MessageBoxButtons.OK);
                }
                else
                {
                    if (operations.AddNewLicense(Constants.LicensesFileXMLPath, clientLicense, modules, out NewLicense))
                    {

                        this.SaveLicenseFile(NewLicense);
                        MessageBox.Show(this, "License successfully Generated", "Success", MessageBoxButtons.OK);
                        this.Close();
                    }
                    else
                        MessageBox.Show(this, "Error while Generating License!", "Error", MessageBoxButtons.OK);

                }


            }
            catch (Exception ex)
            {
                MessageBox.Show(this, "Error Occured " + ex.Message, "Error", MessageBoxButtons.OK);
            }
        }

        /// <summary>
        /// Method to save the license data to file
        /// </summary>
        /// <param name="data">Data as XElement</param>
        private void SaveLicenseFile(XElement data)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            saveFileDialog.Title = "Browse Text Files";
            saveFileDialog.DefaultExt = "licx";
            // set a default file name
            string fileName = Constants.GetSafeFileName(this.TextBoxClientName.Text.Trim() + ".licx");
            saveFileDialog.FileName = fileName;
            saveFileDialog.Filter = "License files (*.licx)|*.licx";
            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                using (StreamWriter sw = new StreamWriter(saveFileDialog.FileName))
                    sw.WriteLine(data);
            }
            PGPEncryptDecrypt.EncryptPgpFile(saveFileDialog.FileName, saveFileDialog.FileName);
        }

        #endregion

        #region Validation

        private void TextBoxClientName_Validating(object sender, CancelEventArgs e)
        {
            this.ValidateTextBoxClientName();
        }

        private void TextBoxNumberOfUsers_Validating(object sender, CancelEventArgs e)
        {
            this.ValidateTextBoxNumberOfUsers();
        }

        private bool ValidateLicenseInfo()
        {
            bool bStatus = true;
            if (this.TextBoxClientName.Text.Trim() == "")
            {
                errorProviderCreateEditLicense.SetError(this.TextBoxClientName, "Please enter Client Name");
                bStatus = false;
            }
            else
                errorProviderCreateEditLicense.SetError(this.TextBoxClientName, "");


            if (this.TextBoxMachineId.Text.Trim() == "")
            {
                errorProviderCreateEditLicense.SetError(this.TextBoxMachineId, "Please enter Number of Clients");
                bStatus = false;
            }
            else
                errorProviderCreateEditLicense.SetError(this.TextBoxMachineId, "");
            if (this.TextBoxNumberOfUsers.Text.Trim() == "")
            {
                errorProviderCreateEditLicense.SetError(this.TextBoxNumberOfUsers, "Please enter Number of Users");
                bStatus = false;
            }
            else
                errorProviderCreateEditLicense.SetError(this.TextBoxNumberOfUsers, "");

            if (this.TextBoxNumberOfUsers.Text.Trim().Contains('-'))
            {
                var dd = this.TextBoxNumberOfUsers.Text.Trim().Split('-');
                if (Convert.ToInt32(dd[1]) > 1)
                {
                    errorProviderCreateEditLicense.SetError(this.TextBoxNumberOfUsers, "Value cann't be less than -1");
                    bStatus = false;
                }
                else
                    errorProviderCreateEditLicense.SetError(this.TextBoxNumberOfUsers, "");
            }

            if (this.textBoxFirstName.Text.Trim() == "")
            {
                errorProviderCreateEditLicense.SetError(this.textBoxFirstName, "Please enter Client's Frist Name");
                bStatus = false;
            }
            else
                errorProviderCreateEditLicense.SetError(this.textBoxFirstName, "");

            if (this.textBoxLastName.Text.Trim() == "")
            {
                errorProviderCreateEditLicense.SetError(this.textBoxLastName, "Please enter Client's Last name");
                bStatus = false;
            }
            else
            {
                errorProviderCreateEditLicense.SetError(this.textBoxLastName, "");

            }

            if (this.textBoxEmail.Text.Trim() == "")
            {
                errorProviderCreateEditLicense.SetError(this.textBoxEmail, "Please enter Email Id");
                bStatus = false;
            }
            else
            {
                errorProviderCreateEditLicense.SetError(this.textBoxEmail, "");
                var regex = new Regex(@"^[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$");
                if (regex.IsMatch(this.textBoxEmail.Text.Trim()))
                {
                    errorProviderCreateEditLicense.SetError(this.textBoxEmail, "");
                }
                else
                {
                    errorProviderCreateEditLicense.SetError(this.textBoxEmail, "Please enter a Valid Email Id");
                    bStatus = false;
                }
            }

            if (this.TextBoxMachineId.Text.Trim() == "")
            {
                errorProviderCreateEditLicense.SetError(this.TextBoxMachineId, "Please enter Mac/Machine Id");
                bStatus = false;
            }
            else
            {
                errorProviderCreateEditLicense.SetError(this.TextBoxMachineId, "");

            }
            return bStatus;
        }

        private bool ValidateTextBoxClientName()
        {
            bool bStatus = true;
            if (this.TextBoxClientName.Text.Trim() == "")
            {
                errorProviderCreateEditLicense.SetError(this.TextBoxClientName, "Please enter Client Name");
                bStatus = false;
            }
            else
                errorProviderCreateEditLicense.SetError(this.TextBoxClientName, "");
            return bStatus;
        }

        private bool ValidateTextBoxNumberOfUsers()
        {
            bool bStatus = true;

            if (this.TextBoxNumberOfUsers.Text.Trim() == "")
            {
                errorProviderCreateEditLicense.SetError(this.TextBoxNumberOfUsers, "Please enter Number of Users");
                bStatus = false;
            }
            // else
            //   errorProviderCreateEditLicense.SetError(this.TextBoxNumberOfUsers, "");
            else if (this.TextBoxNumberOfUsers.Text.Trim().Contains('-'))
            {
                var number = this.TextBoxNumberOfUsers.Text.Trim().Split('-');
                if (Convert.ToInt32(number[1]) > 1)
                {
                    errorProviderCreateEditLicense.SetError(this.TextBoxNumberOfUsers, "Value cann't be less than -1");
                    bStatus = false;
                }
                else
                    errorProviderCreateEditLicense.SetError(this.TextBoxNumberOfUsers, "");
            }
            return bStatus;
        }

        private bool ValidatetextBoxFirstName()
        {
            bool bStatus = true;

            if (this.textBoxFirstName.Text.Trim() == "")
            {
                errorProviderCreateEditLicense.SetError(this.textBoxFirstName, "Please enter Client's Frist Name");
                bStatus = false;
            }
            else
                errorProviderCreateEditLicense.SetError(this.textBoxFirstName, "");

            return bStatus;
        }

        private bool ValidatetextBoxLastName()
        {
            bool bStatus = true;

            if (this.textBoxLastName.Text.Trim() == "")
            {
                errorProviderCreateEditLicense.SetError(this.textBoxLastName, "Please enter Client's Last name");
                bStatus = false;
            }
            else
            {
                errorProviderCreateEditLicense.SetError(this.textBoxLastName, "");

            }
            return bStatus;
        }

        private bool ValidatetextBoxEmail()
        {
            bool bStatus = true;

            if (this.textBoxEmail.Text.Trim() == "")
            {
                errorProviderCreateEditLicense.SetError(this.textBoxEmail, "Please enter Email Id");
                bStatus = false;
            }
            else
            {
                errorProviderCreateEditLicense.SetError(this.textBoxEmail, "");
                var regex = new Regex(@"^[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$");
                if (regex.IsMatch(this.textBoxEmail.Text.Trim()))
                {
                    errorProviderCreateEditLicense.SetError(this.textBoxEmail, "");
                }
                else
                {
                    errorProviderCreateEditLicense.SetError(this.textBoxEmail, "Please enter a Valid Email Id");
                    bStatus = false;
                }
            }

            if (this.TextBoxMachineId.Text.Trim() == "")
            {
                errorProviderCreateEditLicense.SetError(this.TextBoxMachineId, "Please enter Mac/Machine Id");
                bStatus = false;
            }
            else
            {
                errorProviderCreateEditLicense.SetError(this.TextBoxMachineId, "");

            }
            return bStatus;
        }

        private bool ValidateTextBoxMachineId()
        {
            bool bStatus = true;


            if (this.TextBoxMachineId.Text.Trim() == "")
            {
                errorProviderCreateEditLicense.SetError(this.TextBoxMachineId, "Please enter Mac/Machine Id");
                bStatus = false;
            }
            else
            {
                errorProviderCreateEditLicense.SetError(this.TextBoxMachineId, "");

            }
            return bStatus;
        }


        private void TextBoxNumberOfUsers_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '-' && (sender as TextBox).Text.Length > 0)
                e.Handled = true;
            if (!char.IsControl(e.KeyChar) && (!char.IsDigit(e.KeyChar)) && (e.KeyChar != '-'))
                e.Handled = true;
        }

        private void TextBoxClientName_MouseEnter(object sender, EventArgs e)
        {
            this.toolTipCreateEditLicense.Show("Enter Name of the Client", TextBoxClientName);
        }

        private void TextBoxClientName_MouseLeave(object sender, EventArgs e)
        {
            this.toolTipCreateEditLicense.Hide(TextBoxClientName);
        }

        private void TextBoxNumberOfUsers_MouseEnter(object sender, EventArgs e)
        {
            this.toolTipCreateEditLicense.Show("Enter Number of Users with -1 as Unlimited Users", TextBoxNumberOfUsers);
        }

        private void TextBoxNumberOfUsers_MouseLeave(object sender, EventArgs e)
        {
            this.toolTipCreateEditLicense.Hide(TextBoxNumberOfUsers);

        }



        private void textBoxEmail_Validating(object sender, CancelEventArgs e)
        {
            this.ValidatetextBoxEmail();
        }

        private void textBoxFirstName_Validating(object sender, CancelEventArgs e)
        {
            this.ValidatetextBoxFirstName();
        }

        private void textBoxLastName_Validating(object sender, CancelEventArgs e)
        {
            this.ValidatetextBoxLastName();
        }

        private void TextBoxMachineId_Validating(object sender, CancelEventArgs e)
        {
            this.ValidateTextBoxMachineId();
        }

        private void TextBoxMachineId_MouseEnter(object sender, EventArgs e)
        {
            this.toolTipCreateEditLicense.Show("Enter Mac/Machine Id", TextBoxMachineId);
        }

        private void TextBoxMachineId_MouseLeave(object sender, EventArgs e)
        {
            this.toolTipCreateEditLicense.Hide(TextBoxMachineId);
        }

        #endregion


    }
}
