﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LicenseOperations
{
    public class ClientLicense
    {

        private string softwareid;

        public string SoftwareId
        {
            get { return softwareid; }
            set { softwareid = value; }
        }
        private string softwarename;

        public string SoftwareName
        {
            get { return softwarename; }
            set { softwarename = value; }
        }
        private string clientname;

        public string ClientName
        {
            get { return clientname; }
            set { clientname = value; }
        }

        private string machineId;

        public string MachineId
        {
            get { return machineId; }
            set { machineId = value; }
        }

        private string expirationdate;

        public string ExpirationDate
        {
            get { return expirationdate; }
            set { expirationdate = value; }
        }
        private string firstname;

        public string FirstName
        {
            get { return firstname; }
            set { firstname = value; }
        }
        private string lastname;

        public string LastName
        {
            get { return lastname; }
            set { lastname = value; }
        }
        private string email;

        public string Email
        {
            get { return email; }
            set { email = value; }
        }
        private string phone;

        public string Phone
        {
            get { return phone; }
            set { phone = value; }
        }
        private string numberofclients;

        public string NumberOfClients
        {
            get { return numberofclients; }
            set { numberofclients = value; }
        }
        private string numberofusers;

        public string NumberOfUsers
        {
            get { return numberofusers; }
            set { numberofusers = value; }
        }
        private bool istrial;

        public bool IsTrial
        {
            get { return istrial; }
            set { istrial = value; }
        }

    }
}
