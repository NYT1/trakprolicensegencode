﻿using LicenseOperations.Utilities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.AccessControl;
using System.Security.Cryptography;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace LicenseOperations
{
    public class Operations
    {

        /// <summary>  
        /// Method to Give Full read/write permission to file 
        /// <para>
        ///<param name="fileName">name of the file</param>
        ///<param name="account">account</param>
        ///<param name="rights">File System Rights</param>
        ///<param name="controlType">Access Type</param>
        /// </para>
        /// </summary> 
        private static void AddFileSecurity(string fileName, string account, FileSystemRights rights, AccessControlType controlType)
        {
            try
            {
                // Get a FileSecurity object that represents the
                // current security settings.
                FileSecurity fSecurity = File.GetAccessControl(fileName);
                // Add the FileSystemAccessRule to the security settings.
                fSecurity.AddAccessRule(new FileSystemAccessRule(account,
                    rights, controlType));
                // Set the new access settings.
                File.SetAccessControl(fileName, fSecurity);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        /// <summary>  
        /// Method to remove a Modules in a License Node from and XML Document 
        /// <para>
        /// <param name="LicenseId">ID of the license from which modules need to be removed</param>
        /// <param name="XmlDoc">XML Document</param>
        /// </para>
        /// </summary> 
        private XDocument RemoveModulesFromLicenseXML(XDocument XmlDoc, string LicenseId)
        {
            XmlDoc.Descendants("licenses")
                              .Elements("license")
                              .Where(item => item.Attribute("id").Value == LicenseId).Elements("modules").Remove();
            return XmlDoc;

        }


        /// <summary>
        /// Method to copy file 
        /// </summary>
        /// <param name="sourceFilePath">Source file Path</param>
        /// <param name="destFilePath">Destination File Path</param>
        public bool CopyFile(string sourceFilePath, string destFilePath)
        {
            try
            {
                NTAccount ntAccount = new NTAccount("everyone");
                AddFileSecurity(sourceFilePath, ntAccount.ToString(), FileSystemRights.FullControl, AccessControlType.Allow);
                AddFileSecurity(destFilePath, ntAccount.ToString(), FileSystemRights.FullControl, AccessControlType.Allow);
                System.IO.File.Copy(sourceFilePath, destFilePath, true);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }

        }


        /// <summary>  
        /// Method to remove a Node from software XML
        /// <para>
        /// <param name="LicenseId">ID of the Software To Be Removed</param>
        /// <param name="XmlDoc">XML Document</param>
        /// </para>
        /// </summary> 
        private XDocument RemoveSoftwareNodeXML(XDocument XmlDoc, string SoftwareId)
        {
            XmlDoc.Descendants("softwares")
                              .Elements("software")
                              .Where(item => item.Attribute("id").Value == SoftwareId).Elements("modules").Remove();
            return XmlDoc;

        }


        /// <summary>  
        /// Method to Update a Particular license in a Licenses file  
        /// <para>
        /// <param name="FilePath">Path of the XML file</param>
        /// <param name="LicenseId">ID of the License to be Updated</param>
        /// <param name="LiceseModules">List of mudules that will be added to the license</param>
        /// <param name="OrderedLicenseElements"> The elements of Licnese XML Node shoud in same order as in XML File</param>
        /// </para>
        /// </summary>  
        public bool UpdateLicense(string FilePath, ClientLicense LicenseDetail, List<string> LiceseModules, string LicenseId, out XElement NewLicense)
        {

            bool result = false;
            try
            {
                NTAccount ntAccount = new NTAccount("everyone");
                AddFileSecurity(FilePath, ntAccount.ToString(), FileSystemRights.FullControl, AccessControlType.Allow);
                XDocument XmlDoc;
                using (StreamReader streamReader = new StreamReader(FilePath))
                {
                    XmlDoc = XDocument.Parse(PGPEncryptDecrypt.DecryptPgpData(streamReader.ReadToEnd()));
                }

                var licenseInfo = XmlDoc.Descendants("license")
                    .Single(r => r.Attribute("id").Value == LicenseId);
                licenseInfo.Element("softwareid").Value = LicenseDetail.SoftwareId;
                licenseInfo.Element("softwarename").Value = LicenseDetail.SoftwareName;
                licenseInfo.Element("clientname").Value = LicenseDetail.ClientName;
                licenseInfo.Element("machineid").Value = LicenseDetail.MachineId;
                licenseInfo.Element("expirationdate").Value = LicenseDetail.ExpirationDate;
                licenseInfo.Element("firstname").Value = LicenseDetail.FirstName;
                licenseInfo.Element("lastname").Value = LicenseDetail.LastName;
                licenseInfo.Element("email").Value = LicenseDetail.Email;
                licenseInfo.Element("phone").Value = LicenseDetail.Phone;
                // licenseInfo.Element("numberofclients").Value = Convert.ToString(LicenseDetail.NumberOfClients);
                licenseInfo.Element("numberofusers").Value = Convert.ToString(LicenseDetail.NumberOfUsers);
                licenseInfo.Element("istrial").Value = Convert.ToString(LicenseDetail.IsTrial).ToLower();

                XmlDoc = RemoveModulesFromLicenseXML(XmlDoc, LicenseId);

                NewLicense = XmlDoc.Element("licenses").Elements("license")
                                  .Where(item => item.Attribute("id").Value == LicenseId)
                                  .FirstOrDefault();
                if (NewLicense != null)
                    NewLicense.Add(new XElement("modules"));

                foreach (string module in LiceseModules)
                {
                    NewLicense.Element("modules").Add(new XElement("modulename", module));
                }

                XmlDoc.Save(FilePath);
                PGPEncryptDecrypt.EncryptPgpFile(FilePath, FilePath);
                result = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return result;
        }



        //TO DO remove this method
        /// <summary>  
        /// Method to Update a Particular license in a Licenses file  
        /// <para>
        /// <param name="FilePath">Path of the XML file</param>
        /// <param name="LicenseId">ID of the License to be Updated</param>
        /// <param name="Modules">List of mudules that will be added to the license</param>
        /// <param name="OrderedLicenseElements"> The elements of Licnese XML Node shoud in same order as in XML File</param>
        /// </para>
        /// </summary>  
        public bool AddMachineInfoToLicense(string FilePath, List<string> OrderedLicenseElements, List<string> Modules, out XElement NewLicense)
        {
            bool result = false;
            try
            {
                NTAccount ntAccount = new NTAccount("everyone");
                AddFileSecurity(FilePath, ntAccount.ToString(), FileSystemRights.FullControl, AccessControlType.Allow);
                XDocument XmlDoc;
                using (StreamReader streamReader = new StreamReader(FilePath))
                {
                    XmlDoc = XDocument.Parse(PGPEncryptDecrypt.DecryptPgpData(streamReader.ReadToEnd()));
                }

                NewLicense = XmlDoc.Descendants("license").Single();// .Single(r => r.Attribute("id").Value == LicenseId);
                NewLicense.Add(new XElement("machineid", OrderedLicenseElements[0]));
                NewLicense.Add(new XElement("installedon", OrderedLicenseElements[1]));
                XmlDoc.Save(FilePath);
                PGPEncryptDecrypt.EncryptPgpFile(FilePath, FilePath);
                result = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return result;
        }


        /// <summary>  
        /// Method to Update a Particular license in a Licenses file  
        /// <para>
        /// <param name="FilePath">Path of the XML file</param>
        /// <param name="LicenseId">ID of the License to be Updated</param>
        /// <param name="Modules">List of mudules that will be added to the license</param>
        /// <param name="OrderedLicenseElements"> The elements of Licnese XML Node shoud in same order as in XML File</param>
        /// </para>
        /// </summary>  
        public bool AddNewLicense(string FilePath, ClientLicense LicenseDetail, List<string> Modules, out XElement NewLicense)
        {

            bool result = false;
            try
            {
                NTAccount ntAccount = new NTAccount("everyone");
                AddFileSecurity(FilePath, ntAccount.ToString(), FileSystemRights.FullControl, AccessControlType.Allow);


                XDocument XmlDoc;
                using (StreamReader streamReader = new StreamReader(FilePath))
                {
                    XmlDoc = XDocument.Parse(PGPEncryptDecrypt.DecryptPgpData(streamReader.ReadToEnd()));
                }

                NewLicense = new XElement("license");
                XAttribute attribute = new XAttribute("id", Guid.NewGuid().ToString("N").Substring(0, 16).ToUpper());
                NewLicense.Add(attribute);

                NewLicense.Add(new XElement("softwareid", LicenseDetail.SoftwareId));
                NewLicense.Add(new XElement("softwarename", LicenseDetail.SoftwareName));
                NewLicense.Add(new XElement("clientname", LicenseDetail.ClientName));
                NewLicense.Add(new XElement("machineid", LicenseDetail.MachineId));
                NewLicense.Add(new XElement("expirationdate", LicenseDetail.ExpirationDate));
                NewLicense.Add(new XElement("firstname", LicenseDetail.FirstName));
                NewLicense.Add(new XElement("lastname", LicenseDetail.LastName));
                NewLicense.Add(new XElement("email", LicenseDetail.Email));
                NewLicense.Add(new XElement("phone", LicenseDetail.Phone));
                NewLicense.Add(new XElement("numberofusers", LicenseDetail.NumberOfUsers));
                NewLicense.Add(new XElement("istrial", LicenseDetail.IsTrial));
                NewLicense.Add(new XElement("modules"));
                foreach (string module in Modules)
                {
                    NewLicense.Element("modules").Add(new XElement("modulename", module));
                }
                XmlDoc.Element("licenses").Add(NewLicense);
                XmlDoc.Save(FilePath);
                PGPEncryptDecrypt.EncryptPgpFile(FilePath, FilePath);
                result = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return result;
        }


        /// <summary>  
        /// Method to delete a LIcense from XML File
        /// <para>
        /// <param name="FilePath">Path of the License list file</param>
        /// <param name="LicenseId">ID of the License that needs to be Deleted</param>
        /// </para>
        /// </summary> 
        public bool DeleteLicense(string FilePath, string LicenseId)
        {
            try
            {
                bool result = false;
                XDocument XmlDoc;
                using (StreamReader streamReader = new StreamReader(FilePath))
                {
                    XmlDoc = XDocument.Parse(PGPEncryptDecrypt.DecryptPgpData(streamReader.ReadToEnd()));
                }

                XmlDoc.Descendants("licenses")
                                  .Elements("license")
                                  .Where(item => item.Attribute("id").Value == LicenseId).Remove();
                XmlDoc.Save(FilePath);
                PGPEncryptDecrypt.EncryptPgpFile(FilePath, FilePath);
                result = true;
                return result;
            }
            catch (Exception ex) { throw ex; }

        }

        /// <summary>  
        /// Method to delete a software from XML File
        /// <para>
        /// <param name="FilePath">Path of the software list file</param>
        /// <param name="SoftwareId">ID of the Software that needs to be Deleted</param>
        /// </para>
        /// </summary> 
        public bool DeleteSoftware(string FilePath, string SoftwareId)
        {
            try
            {
                bool result = false;
                XDocument XmlDoc;
                using (StreamReader streamReader = new StreamReader(FilePath))
                {
                    XmlDoc = XDocument.Parse(PGPEncryptDecrypt.DecryptPgpData(streamReader.ReadToEnd()));
                }

                XmlDoc.Descendants("softwares")
                                  .Elements("software")
                                  .Where(item => item.Attribute("id").Value == SoftwareId).Remove();
                XmlDoc.Save(FilePath);
                PGPEncryptDecrypt.EncryptPgpFile(FilePath, FilePath);
                result = true;
                return result;
            }
            catch (Exception ex) { throw ex; }

        }


        /// <summary>
        /// Method to delete the software along with the Associated licences
        /// </summary>
        /// <param name="SoftwareFilePath">Path of the software list file</param>
        /// <param name="LicenseFilePath">Path of the License File </param>
        /// <param name="SoftwareId">Id of the software to be deleted</param>
        /// <returns></returns>
        public bool DeleteSoftwareWithLicenses(string SoftwareFilePath, string LicenseFilePath, string SoftwareId)
        {
            try
            {
                bool result = false;
                XDocument XmlDoc;
                //  using (StreamReader streamReader = new StreamReader(Directory.GetFiles(Environment.CurrentDirectory, "*.licx", SearchOption.AllDirectories)[0]))
                using (StreamReader streamReader = new StreamReader(SoftwareFilePath))
                {
                    XmlDoc = XDocument.Parse(PGPEncryptDecrypt.DecryptPgpData(streamReader.ReadToEnd()));
                }

                XmlDoc.Descendants("softwares")
                                  .Elements("software")
                                  .Where(item => item.Attribute("id").Value == SoftwareId).Remove();
                XmlDoc.Save(SoftwareFilePath);
                PGPEncryptDecrypt.EncryptPgpFile(SoftwareFilePath, SoftwareFilePath);




                using (StreamReader streamReader = new StreamReader(LicenseFilePath))
                {
                    XmlDoc = XDocument.Parse(PGPEncryptDecrypt.DecryptPgpData(streamReader.ReadToEnd()));
                }

                //  doc.Descendants("CreditCard").Where(x => (string)x.Element("Name") == name).Remove();

                XmlDoc.Descendants("licenses")
                                  .Elements("license")
                                  .Where(item => item.Element("softwareid").Value == SoftwareId).Remove();
                XmlDoc.Save(LicenseFilePath);
                PGPEncryptDecrypt.EncryptPgpFile(LicenseFilePath, LicenseFilePath);
                result = true;
                return result;
            }
            catch (Exception ex) { throw ex; }

        }


        #region Client


        /// <summary>  
        /// Method to check whether license is valid or Not
        /// <para>
        /// <param name="FilePath">Path of the XML file</param>
        /// </summary>  
        public bool IsLicenseValid(string FilePath)
        {
            bool result = false;
            try
            {
                NTAccount ntAccount = new NTAccount("everyone");
                AddFileSecurity(FilePath, ntAccount.ToString(), FileSystemRights.FullControl, AccessControlType.Allow);
                XDocument XmlDoc;
                using (StreamReader streamReader = new StreamReader(FilePath))
                {
                    XmlDoc = XDocument.Parse(PGPEncryptDecrypt.DecryptPgpData(streamReader.ReadToEnd()));
                }
                //TO DO set the path where original license repository
                XDocument OriginalDoc = XDocument.Load(IoHelper.BasePath + @"\Licenses");
                if ((OriginalDoc.Descendants("license").Single(r => r.Attribute("id").Value == XmlDoc.Descendants("license").Single().Attribute("id").Value) != null))
                    result = true;
                else
                    result = false;
            }
            catch (Exception ex) { throw ex; }

            return result;
        }




        /// <summary>
        /// Method to get the Accessible Modules in the License
        /// </summary>
        /// <param name="FilePath">Path of the License File</param>
        /// <returns>List of accessible modules</returns>
        public List<string> GetAccessibleModulesOfLicense(string FilePath)
        {
            try
            {
                NTAccount ntAccount = new NTAccount("everyone");
                AddFileSecurity(FilePath, ntAccount.ToString(), FileSystemRights.FullControl, AccessControlType.Allow);
                XDocument doc;
                using (StreamReader streamReader = new StreamReader(FilePath))
                {
                    doc = XDocument.Parse(PGPEncryptDecrypt.DecryptPgpData(streamReader.ReadToEnd()));
                }
                List<string> accessibleModules = doc.Descendants("license")
                                                      .Descendants("modulename")
                                                      .Where(er => er.Parent.Name == "modules")
                                                      .Select(p => p.Value)
                                                      .ToList();
                return accessibleModules;
            }
            catch (Exception ex) { throw ex; }

        }

        /// <summary>
        /// Method to return the Detail of the License
        /// </summary>
        /// <param name="FilePath">Path of the license File</param>
        /// <param name="LicenseId">License Id</param>
        /// <returns>List of the Modules</returns>
        public Dictionary<string, string> GetLicenseDetails(string FilePath)
        {
            try
            {
                XDocument doc;
                using (StreamReader streamReader = new StreamReader(FilePath))
                {
                    doc = XDocument.Parse(PGPEncryptDecrypt.DecryptPgpData(streamReader.ReadToEnd()));
                }
                Dictionary<string, string> elementDictionary = new Dictionary<string, string>();
                XElement root = doc.Element("license");
                elementDictionary.Add("licenseid", root.Attribute("id").Value);
                IEnumerable<XElement> list = root.Elements();
                foreach (XElement element in list)
                {
                    if (element.Name != "modules")
                        elementDictionary.Add(element.Name.ToString(), element.Value);
                }

                return elementDictionary;
            }
            catch (Exception ex) { throw ex; }
        }

        #endregion

        /// <summary>
        /// Method to add a new software
        /// </summary>
        /// <param name="FilePath">File path of the Software List</param>
        /// <param name="SoftwareName">name of the software</param>
        /// <param name="Modules">Modules of that software</param>
        /// <returns>True if operation is successfule, false otherwise</returns>
        public bool AddNewSoftware(string FilePath, string SoftwareName, List<string> Modules)
        {
            //bool result = false;
            // SoftwareId = System.Guid.NewGuid().ToString().ToUpper();
            try
            {
                NTAccount ntAccount = new NTAccount("everyone");
                AddFileSecurity(FilePath, ntAccount.ToString(), FileSystemRights.FullControl, AccessControlType.Allow);

                XDocument XmlDoc;
                //  using (StreamReader streamReader = new StreamReader(Directory.GetFiles(Environment.CurrentDirectory, "*.licx", SearchOption.AllDirectories)[0]))
                using (StreamReader streamReader = new StreamReader(FilePath))
                {
                    XmlDoc = XDocument.Parse(PGPEncryptDecrypt.DecryptPgpData(streamReader.ReadToEnd()));
                }

                //var doc = XmlDoc.Descendants("softwares")
                //                   .Elements("software")
                //                   .Where(item => item.Element("name").Value.ToLower() == SoftwareName.ToLower());
                //if (doc.ToList().Any())
                //{
                //    return false;
                //}
                // XDocument XmlDoc = XDocument.Load(FilePath);
                XElement Software = new XElement("software");
                XAttribute attribute = new XAttribute("id", System.Guid.NewGuid().ToString("N").ToUpper());
                Software.Add(attribute);

                //license.Attribute()
                // XmlDoc.Add(license);

                Software.Add(new XElement("name", SoftwareName));
                // foreach (var item in OrderedLicenseElements)
                //{
                //    license.Add(new XElement("temp", item));
                // }
                Software.Add(new XElement("modules"));
                foreach (string module in Modules)
                {
                    Software.Element("modules").Add(new XElement("modulename", module));
                    //     // xmlDoc.Element("license").Add(new XElement("modules"),
                }
                XmlDoc.Element("softwares").Add(Software);
                XmlDoc.Save(FilePath);
                PGPEncryptDecrypt.EncryptPgpFile(FilePath, FilePath);
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            // return result;
        }

        /// <summary>
        /// Method to check if the software already exists
        /// </summary>
        /// <param name="FilePath">Software list file Path</param>
        /// <param name="SoftwareName">Name of the software</param>
        /// <param name="SoftwareId">ID of the software</param>
        /// <returns>returns true if name exists, false otherwise</returns>
        public bool CheckSoftwareNameExists(string FilePath, string SoftwareName, string SoftwareId)
        {
            try
            {
                NTAccount ntAccount = new NTAccount("everyone");
                AddFileSecurity(FilePath, ntAccount.ToString(), FileSystemRights.FullControl, AccessControlType.Allow);

                XDocument XmlDoc;
                using (StreamReader streamReader = new StreamReader(FilePath))
                {
                    XmlDoc = XDocument.Parse(PGPEncryptDecrypt.DecryptPgpData(streamReader.ReadToEnd()));
                }

                var doc = XmlDoc.Descendants("softwares")
                                   .Elements("software")
                                   .Where(item => item.Element("name").Value.ToLower() == SoftwareName.ToLower() && item.Attribute("id").Value != SoftwareId);
                if (doc.ToList().Any())
                {
                    return true;
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            // return result;
        }

        /// <summary>
        /// Method to check if the software already exists
        /// </summary>
        /// <param name="FilePath">Software list file Path</param>
        /// <param name="SoftwareName">Name of the software</param>
        /// <returns>returns true if the name exists , false otherwise</returns>
        public bool CheckSoftwareNameExists(string FilePath, string SoftwareName)
        {
            try
            {
                NTAccount ntAccount = new NTAccount("everyone");
                AddFileSecurity(FilePath, ntAccount.ToString(), FileSystemRights.FullControl, AccessControlType.Allow);
                XDocument XmlDoc;
                using (StreamReader streamReader = new StreamReader(FilePath))
                {
                    XmlDoc = XDocument.Parse(PGPEncryptDecrypt.DecryptPgpData(streamReader.ReadToEnd()));
                }
                var doc = XmlDoc.Descendants("softwares")
                                   .Elements("software")
                                   .Where(item => item.Element("name").Value.ToLower() == SoftwareName.ToLower());
                if (doc.ToList().Any())
                {
                    return true;
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            // return result;
        }

        /// <summary>
        /// Method to Update the Software 
        /// </summary>
        /// <param name="FilePath">Software List file Path</param>
        /// <param name="SoftwareName">Name of the Software</param>
        /// <param name="Modules">Modules of that software</param>
        /// <param name="SoftwareId">ID of the Software</param>
        /// <returns>return true if operation is successfull , false otherwise</returns>
        public bool UpdateSoftware(string FilePath, string SoftwareName, List<string> Modules, string SoftwareId)
        {
            bool result = false;
            try
            {
                NTAccount ntAccount = new NTAccount("everyone");
                AddFileSecurity(FilePath, ntAccount.ToString(), FileSystemRights.FullControl, AccessControlType.Allow);
                XDocument XmlDoc;
                using (StreamReader streamReader = new StreamReader(FilePath))
                {
                    XmlDoc = XDocument.Parse(PGPEncryptDecrypt.DecryptPgpData(streamReader.ReadToEnd()));
                }

                var licenseDetail = XmlDoc.Descendants("software")
                    .Single(r => r.Attribute("id").Value == SoftwareId);

                licenseDetail.Element("name").Value = SoftwareName;
                XmlDoc = RemoveSoftwareNodeXML(XmlDoc, SoftwareId);
                XElement software = XmlDoc.Element("softwares").Elements("software")
                                    .Where(item => item.Attribute("id").Value == SoftwareId)
                                    .FirstOrDefault();
                if (software != null)
                    software.Add(new XElement("modules"));

                foreach (string module in Modules)
                {
                    software.Element("modules").Add(new XElement("modulename", module));
                }

                XmlDoc.Save(FilePath);
                PGPEncryptDecrypt.EncryptPgpFile(FilePath, FilePath);
                result = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return result;
        }
    
    }
}
