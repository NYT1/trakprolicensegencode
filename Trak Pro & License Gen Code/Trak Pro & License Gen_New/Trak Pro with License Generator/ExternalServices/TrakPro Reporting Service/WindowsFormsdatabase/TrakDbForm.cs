﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TrackService;
using System.ServiceProcess;
using System.Data.OleDb;
using System.IO;
using System.Security.AccessControl;
using System.Security.Principal;


namespace WindowsFormsdatabase
{
    public partial class TrakDbForm : Form
    {
        public TrakDbForm()
        {
            InitializeComponent();
            btnRun.Enabled = false;
            lblError.Visible = false; lblSuccess.Visible = false;
        }

        static void lineChanger(string newText, string fileName, int line_to_edit)
        {
            string[] arrLine = File.ReadAllLines(fileName);
            arrLine[line_to_edit - 1] = newText;
            File.WriteAllLines(fileName, arrLine);
        }
        private static void fullacc(string path)
        {
            DirectorySecurity sec = Directory.GetAccessControl(path);
            SecurityIdentifier everyone = new SecurityIdentifier(WellKnownSidType.WorldSid, null);
            sec.AddAccessRule(new FileSystemAccessRule(everyone, FileSystemRights.Modify | FileSystemRights.Synchronize, InheritanceFlags.ContainerInherit | InheritanceFlags.ObjectInherit, PropagationFlags.None, AccessControlType.Allow));
            Directory.SetAccessControl(path, sec);
        }
        private void btnRun_Click(object sender, EventArgs e)
        {
            fullacc(txtPath.Text);
            ////Code to update config with textbox values...          
            String NewConnectionString = @"<add name=""TRAKI"" connectionString=""Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + txtPath.Text + ";Persist Security Info=True;Jet OLEDB:Database Password=" + txtPassword.Text + "\" providerName=\"System.Data.OleDb\" />";
            lineChanger(NewConnectionString, "TrackService.exe.config", 4);
            Application.Exit();
            System.Diagnostics.Process.Start("TrackService.exe", "-i");
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            //System.Diagnostics.Process.Start("TrackService.exe", "-u");
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                string strfilename = openFileDialog.FileName;
                txtPath.Text = strfilename;
            }
        }

        private void txtTestConnection_Click(object sender, EventArgs e)
        {
            try
            {
                OleDbConnection con = new OleDbConnection();

                con.ConnectionString = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source='" + txtPath.Text + "';Persist Security Info=True;Jet OLEDB:Database Password='" + txtPassword.Text + "';";
                {

                    con.Open();
                    if (con.State == ConnectionState.Open) // if connection.Open was successful
                    {
                        lblSuccess.Visible = true; lblError.Visible = false;
                        lblSuccess.Text = "Successfully Connected, Run the Service!";
                        btnRun.Enabled = true;
                    }
                    else
                    {
                        lblError.Visible = true; lblSuccess.Visible = false;
                        lblError.Text = "Connection failed.";
                        btnRun.Enabled = false;
                    }
                }


            }
            catch (Exception ex)
            {
                lblError.Visible = true; lblSuccess.Visible = false;
                lblError.Text = "Connection failed.";
            }
        }

        private void txtPath_Click(object sender, EventArgs e)
        {
            lblError.Visible = false; lblSuccess.Visible = false;
        }

        private void txtPassword_Click(object sender, EventArgs e)
        {
            lblError.Visible = false; lblSuccess.Visible = false;
        }

    }
}
