﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TRAKI.App.Interfaces;
using TRAKI.App.Model;
using Dapper;
using System.Data;
using TRAKI.App.Common;
using System.Threading;
using TRAKI.App.Account;



namespace TRAKI.App.DapperQueries.AssetQueries
{
    class UpdateAsset : IQuery<int>
    {
        private Asset _asset;


        /// <summary>
        /// initializes the instance of the <see cref="UpdateAsset"/> class.
        /// </summary>
        public UpdateAsset(Asset Asset)
        {
            this._asset = Asset;
        }


        /// <summary>
        /// excecutes db query against database
        /// </summary>
        /// <param name="db"></param>
        /// <returns>Number of rows Affected</returns>
        public int Execute(System.Data.IDbConnection db)
        {
            IDbTransaction transaction = null;
            try
            {
                int result = 0;
                using (transaction = db.BeginTransaction())
                {
                    CommandDefinition cmd = new CommandDefinition(@"Update [Asset] set
                                        CatalogNumber = @CatalogNumber,
                                        SerialNumber = @SerialNumber,
                                        Cost= @Cost,
                                        DateAcquired = @DateAcquired ,
                                        WarrantyExpiryDate = @WarrantyExpiryDate,
                                        ManufacturerCode= @ManufacturerCode,
                                        DepartmentNumber = @DepartmentNumber,
                                        Invoice = @Invoice,
                                        DispositionCode = @DispositionCode,
                                        AssetRelocated = @AssetRelocated, 
                                        ModelNumber = @ModelNumber,
                                        ModelName = @ModelName,
                                        ModifiedBy = @ModifiedBy,
                                        ModifiedDate=@ModifiedDate,
                                        IsMissing = @IsMissing,
                                        IsActive = @IsActive,
                                        Status=@Status WHERE AssetId=@AssetId",
                                         new
                                         {
                                             @CatalogNumber = this._asset.CatalogNumber,
                                             @SerialNumber = this._asset.SerialNumber,
                                             @Cost = this._asset.Cost,
                                             @DateAcquired = this._asset.DateAcquired,
                                             @WarrantyExpiryDate = this._asset.WarrantyExpiryDate,
                                             @ManufacturerCode = this._asset.ManufacturerCode,
                                             @DepartmentNumber = this._asset.DepartmentNumber,
                                             @Invoice = this._asset.Invoice,
                                             @DispositionCode = this._asset.DispositionCode,
                                             @AssetRelocated = (Convert.ToBoolean(this._asset.AssetRelocated)) ? 1 : 0,
                                             @ModelNumber = this._asset.ModelNumber,
                                             @ModelName = this._asset.ModelName,
                                             @ModifiedBy = (Thread.CurrentPrincipal as CustomPrincipal).Identity.CurrentUser.UserName,
                                             @ModifiedDate = DateTime.Now.ToString(),
                                             @IsMissing = (Convert.ToBoolean(this._asset.IsMissing)) ? 1 : 0,
                                             @IsActive = (Convert.ToBoolean(this._asset.IsActive)) ? 1 : 0,
                                             @Status = this._asset.Status,
                                             @AssetId = this._asset.AssetId

                                         }, transaction);

                    db.Execute(cmd);

                    if (Convert.ToBoolean(this._asset.AssetRelocated))
                    {

                        db.Execute(@"Update [AssetLocation] 
                                       set IsActive =@IsActive
                                          WHERE AssetId=@AssetId", 
                                           new
                                           {
                                               @IsActive = 0,
                                               @AssetId = this._asset.AssetId,
                                           }, transaction);

                        db.Execute(@"Insert into [AssetLocation] (
                                        AssetId,
                                        BuildingNumber,
                                        RoomNumber,
                                        MoveType
                                          )
                                   values ( 
                                        @AssetId,
                                        @BuildingNumber,
                                        @RoomNumber,
                                        @MoveType
                                        ) ",
                                           new
                                           {
                                               @AssetId = this._asset.AssetId,
                                               @BuildingNumber = this._asset.BuildingNumber,
                                               @RoomNumber = this._asset.RoomNumber,
                                               @MoveType = MoveType.AssetEdited,
                                           }, transaction);

                        IDbCommand NewIdCommand = new System.Data.OleDb.OleDbCommand();
                        NewIdCommand.Transaction = transaction;
                        NewIdCommand.CommandText = "SELECT @@IDENTITY";
                        NewIdCommand.Connection = db;
                        var newLocationId = NewIdCommand.ExecuteScalar();

                        db.Execute(@"Update [Asset] set AssetLocationId=@AssetLocationId where AssetId= @AssetId",
                                         new
                                         {
                                             @AssetLocationId = newLocationId,
                                             @AssetId = this._asset.AssetId
                                         }, transaction);

                    }
                    transaction.Commit();
                    result = 1;

                }

                return result;
            }

            catch (Exception ex)
            {
                if (transaction != null)
                    transaction.Rollback();
                throw ex;
            }
        }
    }
}