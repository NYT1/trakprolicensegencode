﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TRAKI.App.Model;
using Dapper;
using System.Data;
using TRAKI.App.Interfaces;

namespace TRAKI.App.DapperQueries.AssetQueries
{
    class UpdateAssetBarcode : IQuery<int>
    {
        private AssetBarcode _assetBarcode;


        /// <summary>
        /// initializes the instance of the <see cref="UpdateAssetBarcode"/> class.
        /// </summary>
        public UpdateAssetBarcode(AssetBarcode AssetBarcode)
        {
            this._assetBarcode = AssetBarcode;
        }

        /// <summary>
        /// excecutes db query against database
        /// </summary>
        /// <param name="db"></param>
        /// <returns>Number of rows Affected</returns>
        public int Execute(System.Data.IDbConnection db)
        {
            IDbTransaction transaction = null;
            try
            {
                int result = 0;
                using (transaction = db.BeginTransaction())
                {

                    result = db.Execute(@"Insert into [AssetBarcode] (
                                        AssetId,
                                        PreviousAssetNumber
                                          )
                                   values ( 
                                        @AssetId,
                                        @PreviousAssetNumber
                                        ) ",
                                       new
                                       {
                                           @AssetId = this._assetBarcode.AssetId,
                                           @PreviousAssetNumber = this._assetBarcode.PreviousAssetNumber,
                                       }, transaction);

                    IDbCommand NewIdCommand = new System.Data.OleDb.OleDbCommand();
                    NewIdCommand.Transaction = transaction;
                    NewIdCommand.CommandText = "SELECT @@IDENTITY";
                    NewIdCommand.Connection = db;
                    var newBarcodeIdId = NewIdCommand.ExecuteScalar();

                    CommandDefinition cmd = new CommandDefinition(@"Update [Asset] set AssetNumber=@AssetNumber, AssetBarcodeId=@AssetBarcodeId
                                       where AssetId=@AssetId",
                                      new
                                      {
                                          @AssetNumber = this._assetBarcode.AssetNumber,
                                          @AssetBarcodeId = newBarcodeIdId,
                                          @AssetId = this._assetBarcode.AssetId

                                      }, transaction);
                    result = db.Execute(cmd);
                    transaction.Commit();
                    result = 1;
                }

                return result;
            }

            catch (Exception ex)
            {
                if (transaction != null)
                    transaction.Rollback();
                throw ex;
            }
        }
    }
}