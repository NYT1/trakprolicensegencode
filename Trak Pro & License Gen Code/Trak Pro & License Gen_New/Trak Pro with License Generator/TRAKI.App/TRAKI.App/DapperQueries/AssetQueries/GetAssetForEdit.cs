﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TRAKI.App.Interfaces;
using Dapper;
using TRAKI.App.Model;

namespace TRAKI.App.DapperQueries.AssetQueries
{
    class GetAssetForEdit : IQuery<List<Asset>>
    {
        private int _assetId;


        /// <summary>
        /// initializes the instance of the <see cref="GetAssetForEdit"/> class.
        /// </summary>
        public GetAssetForEdit(int AssetId)
        {
            this._assetId = AssetId;
        }


        /// <summary>
        /// excecutes db query against database
        /// </summary>
        /// <param name="db"></param>
        /// <returns>Return the Asset datail for Edit/Update</returns>
        public List<Asset> Execute(System.Data.IDbConnection db)
        {
            string query = @" SELECT Asset.AssetNumber, Asset.CatalogNumber , Asset.SerialNumber , Asset.Cost , Asset.Status, Asset.DateAcquired , Asset.WarrantyExpiryDate , Asset.ManufacturerCode , Asset.DepartmentNumber , Asset.Invoice , Asset.DispositionCode , Asset.InventoryDate , Asset.IsMissing , Asset.AssetLocationId , Asset.AssetRelocated , Asset.AssetBarcodeId , Asset.AssetId , Asset.IsActive , Asset.ModelNumber, Manufacturer.ManufacturerName, 
                       [Catalog].CatalogDescription  ,Building.BuildingId,  Building.BuildingDescription,  Department.DepartmentDescription , Room.RoomDescription  , AssetLocation.RoomNumber, AssetLocation.BuildingNumber,  Disposition.DispositionDescription
                        from ((((((Asset 
                          INNER JOIN
                        [Catalog] on [Catalog].CatalogNumber =Asset.CatalogNumber)
                        LEFT JOIN
                         Manufacturer on Manufacturer.ManufacturerCode =Asset.ManufacturerCode)
                        INNER JOIN
                         AssetLocation on AssetLocation.AssetLocationId= Asset.AssetLocationId)
                        INNER JOIN
                        Building on Building.BuildingNumber =AssetLocation.BuildingNumber)
                        LEFT JOIN
                        Department on Department.DepartmentNumber =Asset.DepartmentNumber)
                        LEFT JOIN
                        Disposition on Disposition.DispositionCode =Asset.DispositionCode)
                        INNER JOIN
                        Room on Room.RoomNumber =AssetLocation.RoomNumber  where(@AssetId is null or  Asset.AssetId =@AssetId)";

            return (List<Asset>)db.Query<Asset>(query, new { @AssetId= this._assetId});
        }
    }
}
