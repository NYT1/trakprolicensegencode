﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TRAKI.App.Interfaces;
using TRAKI.App.Model;
using Dapper;
using System.Data;
using TRAKI.App.Common;
using System.Threading;
using TRAKI.App.Account;


namespace TRAKI.App.DapperQueries.AssetQueries
{
    class InsertAsset : IQuery<int>
    {
        private Asset _asset;


        /// <summary>
        /// initializes the instance of the <see cref="InsertAsset"/> class.
        /// </summary>
        public InsertAsset(Asset Asset)
        {
            this._asset = Asset;
        }

        /// <summary>
        /// excecutes db query against database
        /// </summary>
        /// <param name="db"></param>
        /// <returns>Number of rows Affected</returns>
        public int Execute(System.Data.IDbConnection db)
        {
            IDbTransaction transaction = null;
            try
            {
                int result = 0;
                using (transaction = db.BeginTransaction())
                {
                    CommandDefinition cmd = new CommandDefinition(@"Insert into [Asset] (
                                        AssetNumber,
                                        CatalogNumber,
                                        SerialNumber,
                                        Cost,
                                        DateAcquired ,
                                        WarrantyExpiryDate,
                                        ManufacturerCode,
                                        DepartmentNumber,
                                        Invoice,
                                        DispositionCode,
                                        AssetRelocated, 
                                        ModelNumber,
                                        CreatedBy,
                                        IsMissing ,
                                        IsActive,
                                        Status
                                        )
                                   values ( 
                                        @AssetNumber,
                                        @CatalogNumber,
                                        @SerialNumber,
                                        @Cost,
                                        @DateAcquired ,
                                        @WarrantyExpiryDate,
                                        @ManufacturerCode,
                                        @DepartmentNumber,
                                        @Invoice,
                                        @DispositionCode,
                                        @AssetRelocated,
                                        @ModelNumber,
                                        @CreatedBy,
                                        @IsMissing ,
                                        @IsActive ,
                                        @Status
                                        ) ",
                                         new
                                         {
                                             @AssetNumber = this._asset.AssetNumber,
                                             @CatalogNumber = this._asset.CatalogNumber,
                                             @SerialNumber = this._asset.SerialNumber,
                                             @Cost = this._asset.Cost,
                                             @DateAcquired = this._asset.DateAcquired,
                                             @WarrantyExpiryDate = this._asset.WarrantyExpiryDate,
                                             @ManufacturerCode = this._asset.ManufacturerCode,
                                             @DepartmentNumber = this._asset.DepartmentNumber,
                                             @Invoice = this._asset.Invoice,
                                             @DispositionCode = this._asset.DispositionCode,
                                             // @AssetLocationId = 0,//this._asset.AssetLocationId,
                                             @AssetRelocated = (Convert.ToBoolean(this._asset.AssetRelocated)) ? 1 : 0,
                                             @ModelNumber = this._asset.ModelNumber,
                                             @CreatedBy = (Thread.CurrentPrincipal as CustomPrincipal).Identity.CurrentUser.UserName,
                                             @IsMissing = (Convert.ToBoolean(this._asset.IsMissing)) ? 1 : 0,
                                             @IsActive = (Convert.ToBoolean(this._asset.IsActive)) ? 1 : 0,
                                             @Status = this._asset.Status

                                         }, transaction);

                    db.Execute(cmd);

                    IDbCommand NewIdCommand = new System.Data.OleDb.OleDbCommand();
                    NewIdCommand.Transaction = transaction;
                    NewIdCommand.CommandText = "SELECT @@IDENTITY";
                    NewIdCommand.Connection = db;
                    var newAssetId = NewIdCommand.ExecuteScalar();

                    db.Execute(@"Insert into [AssetLocation] (
                                        AssetId,
                                        BuildingNumber,
                                        RoomNumber
                                )
                                   values ( 
                                        @AssetId,
                                        @BuildingNumber,
                                        @RoomNumber
                                        ) ",
                                        new
                                        {
                                            @AssetId = newAssetId,
                                            @BuildingNumber = this._asset.BuildingNumber,
                                            @RoomNumber = this._asset.RoomNumber,
                                            //@MoveType = MoveType.AcceptInventoryData
                                        }, transaction);

                    NewIdCommand = new System.Data.OleDb.OleDbCommand();
                    NewIdCommand.Transaction = transaction;
                    NewIdCommand.CommandText = "SELECT @@IDENTITY";
                    NewIdCommand.Connection = db;
                    var newLocationId = NewIdCommand.ExecuteScalar();

                    db.Execute(@"Update [Asset] set AssetLocationId=@AssetLocationId where AssetNumber= @AssetNumber",
                                      new
                                      {
                                          @AssetLocationId = newLocationId,
                                          @AssetNumber = this._asset.AssetNumber,
                                      }, transaction);
                    transaction.Commit();
                    result = (int)newAssetId;
                }

                return result;
            }

            catch (Exception ex)
            {
                if (transaction != null)
                    transaction.Rollback();
                throw ex;
            }
        }
    }
}