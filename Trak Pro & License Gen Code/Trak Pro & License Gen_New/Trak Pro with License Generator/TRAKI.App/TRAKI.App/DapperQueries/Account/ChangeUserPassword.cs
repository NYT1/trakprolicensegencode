﻿using System;
using TRAKI.App.Interfaces;
using TRAKI.App.Model.Account;
using Dapper;

namespace TRAKI.App.DapperQueries.Account
{
    class ChangeUserPassword : IQuery<int>
    {
        private Password _password;

        /// <summary>
        /// initializes the instance of the <see cref="UpdateUser"/> class.
        /// </summary>
        public ChangeUserPassword(Password password)
        {
            this._password = password;
        }

        /// <summary>
        /// excecutes db query against database
        /// </summary>
        /// <param name="db"></param>
        /// <returns>Number of rows Affected</returns>
        public int Execute(System.Data.IDbConnection db)
        {
            try
            {
                int result = db.Execute(@"UPDATE [User]
                                   SET 
                                      [Password] = @Password, IsTempPassword = @IsTempPassowrd  WHERE [UserId] = @UserId",
                                    new
                                    {
                                        @Password = this._password.NewPassword,
                                        @IsTempPassowrd = (Convert.ToBoolean(this._password.IsTempPassword)) ? 1 : 0,
                                        @UserId = this._password.UserId
                                    });

                return result;
            }

            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}