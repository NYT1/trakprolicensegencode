﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TRAKI.App.Interfaces;
using Dapper;
using TRAKI.App.Model;

namespace TRAKI.App.DapperQueries.DispositionQueries
{
    class GetDisposition : IQuery<List<Disposition>>
    {
        private int? _dispositionId;
        private int? _isActive;

        /// <summary>
        /// initializes the instance of the <see cref="GetDisposition"/> class.
        /// </summary>
        public GetDisposition(int? DispositionId, int? IsActive)
        {
            this._dispositionId = DispositionId;
            this._isActive = IsActive;
        }

        /// <summary>
        /// excecutes db query against database
        /// </summary>
        /// <param name="db"></param>
        /// <returns>List of Dispositions</returns>
        public List<Disposition> Execute(System.Data.IDbConnection db)
        {
          
            if (this._isActive > 0)
                this._isActive = null;

            string query = @"SELECT DispositionId,DispositionCode,DispositionDescription, IsActive from [Disposition] where ((@IsActive is null or [Disposition].IsActive=@IsActive) and (@DispositionId is null or  [Disposition].DispositionId=@DispositionId)) order by DispositionCode";
            return (List<Disposition>)db.Query<Disposition>(query, new
            {
                @IsActive = this._isActive,
                @DispositionId = this._dispositionId
            });

        }
    }
}

