﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using TRAKI.App.Model;
using TRAKI.App.Interfaces;

namespace TRAKI.App.DapperQueries.DispositionQueries
{
    public class SearchDisposition : IQuery<List<Disposition>>
    {
        private string _searchText;
        private int? _isActive;

        /// <summary>
        /// initializes the instance of the <see cref="SearchDisposition"/> class.
        /// </summary>
        public SearchDisposition(string SearchText, int? IsActive)
        {
            this._searchText = SearchText;
            this._isActive = IsActive;
        }

        /// <summary>
        /// excecutes db query against database
        /// </summary>
        /// <param name="db"></param>
        /// <returns>List of Dispositions</returns>
        public List<Disposition> Execute(System.Data.IDbConnection db)
        {

            if (this._isActive > 0)
                this._isActive = null;
            string query = @"SELECT [Disposition].DispositionId,[Disposition].DispositionCode, [Disposition].DispositionDescription , IsActive
                        from  [Disposition] where((@IsActive is null or [Disposition].IsActive= @IsActive )and  ([Disposition].DispositionCode  like '%" + this._searchText + "%' OR [Disposition].DispositionDescription like '%" + this._searchText + "%')) order by DispositionCode";
            return (List<Disposition>)db.Query<Disposition>(query,

                new
                {
                    @IsActive = this._isActive
                });
        }
    }
}