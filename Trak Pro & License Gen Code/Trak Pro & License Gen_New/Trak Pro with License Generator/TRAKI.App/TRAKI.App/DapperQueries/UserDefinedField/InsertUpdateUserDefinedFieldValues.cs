﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TRAKI.App.Interfaces;
using Dapper;
using TRAKI.App.Model;
using System.Threading;
using TRAKI.App.Account;


namespace TRAKI.App.DapperQueries.UserDefinedField
{
    class InsertUpdateUserDefinedFieldValues : IQuery<int>
    {
        private CustomField _customField;
        private int _operation;
        /// <summary>
        /// initializes the instance of the <see cref="UpdateDepartment"/> class.
        /// </summary>
        public InsertUpdateUserDefinedFieldValues(CustomField CustomField, int opr)
        {
            this._customField = CustomField;
            this._operation = opr;
        }

        /// <summary>
        /// excecutes db query against database
        /// </summary>
        /// <param name="db"></param>
        /// <returns>Number of rows Affected</returns>
        public int Execute(System.Data.IDbConnection db)
        {
            try
            {
                //string query  = @"select Assetid from UserDefinedFieldValues where assetId=" + this._customField.AssetId;
                // var assetIdsList = (List<string>)db.Query<string>(query);
                if (this._operation > 0)
                {
                    return Update(db);
                }
                else
                {
                    return Insert(db);
                }


                //                int result = db.Execute(@"UPDATE UserDefinedFieldValues
                //                                          SET 
                //                                          UserDefinedFieldId = @UserDefinedFieldId,
                //                                          FieldValue = @FieldValue, 
                //                                          UpdatedBy = @UpdatedBy,
                //                                          UpdateDate=@UpdateDate
                //                			              WHERE UserDefinedFieldId = @UserDefinedFieldId and AssetId=@AssetId",
                //                                           new
                //                                           {
                //                                               @UserDefinedFieldId = _customField.UserDefinedFieldId,
                //                                               @FieldValue = this._customField.FieldValue,
                //                                               @UpdatedBy = (Thread.CurrentPrincipal as CustomPrincipal).Identity.CurrentUser.UserName,
                //                                               @UpdateDate = DateTime.Now.ToString(),
                //                                               @AssetId = this._customField.AssetId
                //                                           });

                //                return result;
            }

            catch (Exception ex)
            {

                throw ex;
            }
        }


        private int Update(System.Data.IDbConnection db)
        {
            int result = db.Execute(@"UPDATE UserDefinedFieldValues
                                          SET 
                                          UserDefinedFieldId = @UserDefinedFieldId,
                                          FieldValue = @FieldValue, 
                                          UpdatedBy = @UpdatedBy,
                                          UpdateDate=@UpdateDate
                			              WHERE UserDefinedFieldId = @UserDefinedFieldId and AssetId=@AssetId",
                                       new
                                       {
                                           @UserDefinedFieldId = _customField.UserDefinedFieldId,
                                           @FieldValue = this._customField.FieldValue,
                                           @UpdatedBy = (Thread.CurrentPrincipal as CustomPrincipal).Identity.CurrentUser.UserName,
                                           @UpdateDate = DateTime.Now.ToString(),
                                           @AssetId = this._customField.AssetId
                                       });

            return result;
        }


        private int Insert(System.Data.IDbConnection db)
        {
            int result = db.Execute(@"Insert Into UserDefinedFieldValues(
                                          UserDefinedFieldId ,
                                          FieldValue , 
                                          UpdatedBy ,
                                          UpdateDate,
                                          AssetId
                                        )
                                    values
                                    (
                                        @UserDefinedFieldId,
                                        @FieldValue, 
                                        @UpdatedBy,
                                        @UpdateDate,
                                        @AssetId
                                    ) ",
                                          new
                                          {
                                              @UserDefinedFieldId = _customField.UserDefinedFieldId,
                                              @FieldValue = this._customField.FieldValue,
                                              @UpdatedBy = (Thread.CurrentPrincipal as CustomPrincipal).Identity.CurrentUser.UserName,
                                              @UpdateDate = DateTime.Now.ToString(),
                                              @AssetId = this._customField.AssetId
                                          });

            return result;
        }

    }
}