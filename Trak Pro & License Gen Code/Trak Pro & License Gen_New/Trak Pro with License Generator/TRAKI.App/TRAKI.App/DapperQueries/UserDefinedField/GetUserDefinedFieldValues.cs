﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dapper;
using System.Threading.Tasks;
using TRAKI.App.Model;
using TRAKI.App.Interfaces;
using TRAKI.App.Common;

namespace TRAKI.App.DapperQueries.UserDefinedField
{
    class GetUserDefinedFieldValues : IQuery<List<CustomField>>
    {
        private string _searchText;
        private int? _sActive;
        private int? _assetNumber;

        /// <summary>
        /// initializes the instance of the <see cref="SearchDepartment"/> class.
        /// </summary>
        public GetUserDefinedFieldValues(string SearchText, int? IsActive, int? AssetNumber)
        {
            this._searchText = SearchText;
            this._sActive = IsActive;
            this._assetNumber = AssetNumber;
        }

        /// <summary>
        /// excecutes db query against database
        /// </summary>
        /// <param name="db"></param>
        /// <returns>List of Custom Fields</returns>
        public List<CustomField> Execute(System.Data.IDbConnection db)
        {

            if (this._sActive > 0)
                this._sActive = null;
            string query = string.Empty;
            //query = @"select Assetid from Asset where Assetid=" + this._assetNumber;
            query = @"select Assetid from UserDefinedFieldValues where Assetid=" + this._assetNumber;
            var assetIdsList = (List<string>)db.Query<string>(query);
            if (assetIdsList.Count > 0)
            {
                query = @"SELECT UDF.UserDefinedFieldId, UDF.UserDefinedField, UDF.Description, UDF.IsActive, UDF.DisplayOrder,
                                UDF.DataType,
                                UDF.PickListValues,
                                UDF.MinNumber,
                                UDF.MaxNumber,
                                UDF.MinDate,
                                UDF.MaxDate,
                                UDF.MinStringLength,
                                UDF.MaxStringLength,
                                UDF.DefaultValue,
                                UDF.FieldCaption,
                                UDF.FieldType,
                                UDV.FieldValue,
                                UDV.AssetId,
                                UDF.Required
                                FROM UserDefinedField UDF 
                                left join UserDefinedFieldValues UDV
                                on UDV.UserDefinedFieldId =UDF.UserDefinedFieldId
                                where ((@AssetNumber is null or UDV.AssetId= @AssetNumber ) and (@IsActive is null or UDF.IsActive= @IsActive ) and (UDF.Published= -1) and  (UDF.UserDefinedField   like '%" + this._searchText + "%' OR UDF.Description like '%" + this._searchText + "%' )) order by UDF.DisplayOrder";
            }
            else
            {
                query = @"SELECT UDF.UserDefinedFieldId, UDF.UserDefinedField, UDF.Description, UDF.IsActive, UDF.DisplayOrder,
                                UDF.DataType,
                                UDF.PickListValues,
                                UDF.MinNumber,
                                UDF.MaxNumber,
                                UDF.MinDate,
                                UDF.MaxDate,
                                UDF.MinStringLength,
                                UDF.MaxStringLength,
                                UDF.DefaultValue,
                                UDF.FieldCaption,
                                UDF.FieldType,
                                UDV.FieldValue,
                                UDV.AssetId,
                                UDF.Required
                                FROM UserDefinedField UDF 
                                left join UserDefinedFieldValues UDV
                                on UDV.UserDefinedFieldId =UDF.UserDefinedFieldId
                                where (( UDV.AssetId=0 ) and (@IsActive is null or UDF.IsActive= @IsActive ) and (UDF.Published= -1) and  (UDF.UserDefinedField   like '%" + this._searchText + "%' OR UDF.Description like '%" + this._searchText + "%' )) order by UDF.DisplayOrder";

            }

            return (List<CustomField>)db.Query<CustomField>(query, new { @AssetNumber = this._assetNumber, @IsActive = this._sActive});

        }
    }
}
