﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TRAKI.App.Interfaces;
using Dapper;
using TRAKI.App.Model;
using System.Threading;
using TRAKI.App.Account;
using System.Data;


namespace TRAKI.App.DapperQueries.UserDefinedField
{
    class InsertUserDefinedField : IQuery<int>
    {
        private CustomField _userDefinedField;

        /// <summary>
        /// initializes the instance of the <see cref="InsertDepartment"/> class.
        /// </summary>
        public InsertUserDefinedField(CustomField UserDefinedField)
        {
            this._userDefinedField = UserDefinedField;
        }


        /// <summary>
        /// excecutes db query against database
        /// </summary>
        /// <param name="db"></param>
        /// <returns>Number of rows Affected</returns>
        public int Execute(System.Data.IDbConnection db)
        {
            IDbTransaction transaction = null;
            try
            {
                int result = 0;
                using (transaction = db.BeginTransaction())
                {

                    db.Execute(@"Insert into UserDefinedField (UserDefinedField,  Description, FieldType,DataType ,
                                           IsActive,DisplayOrder,
                                           PickListValues,
                                           MinNumber ,
                                           MaxNumber,
                                           MinDate ,
                                           MaxDate  ,
                                           Required ,
                                           MinStringLength ,
                                           MaxStringLength ,
                                           DefaultValue,
                                           CreatedBy ,
                                           FieldCaption,
                                            [Published]
                                            )
                                         values
                                           (@UserDefinedField, @Description,  @FieldType, @DataType,
                                           @IsActive,@DisplayOrder,
                                           @PickListValues,
                                           @MinNumber ,
                                           @MaxNumber,
                                           @MinDate ,
                                           @MaxDate  ,
                                           @Required ,
                                           @MinStringLength ,
                                           @MaxStringLength ,
                                           @DefaultValue,
                                           @CreatedBy ,
                                           @FieldCaption,
                                           @Published
                                        ) ",
                                            new
                                            {
                                                @UserDefinedField = this._userDefinedField.UserDefinedField,
                                                @Description = this._userDefinedField.Description,
                                                @FieldType = this._userDefinedField.FieldType,
                                                @DataType = (Convert.ToBoolean(this._userDefinedField.IsNumeric)) ? "number" : this._userDefinedField.DataType,
                                                @IsActive = (Convert.ToBoolean(this._userDefinedField.IsActive)) ? 1 : 0,
                                                @DisplayOrder = this._userDefinedField.DisplayOrder,
                                                @PickListValues = this._userDefinedField.PickListValues,
                                                @MinNumber = this._userDefinedField.MinNumber,
                                                @MaxNumber = this._userDefinedField.MaxNumber,
                                                @MinDate = this._userDefinedField.MinDate,
                                                @MaxDate = this._userDefinedField.MaxDate,
                                                @Required = (Convert.ToBoolean(this._userDefinedField.Required)) ? 1 : 0,
                                                @MinStringLength = this._userDefinedField.MinStringLength,
                                                @MaxStringLength = this._userDefinedField.MaxStringLength,
                                                @DefaultValue = this._userDefinedField.DefaultValue,
                                                @CreatedBy = (Thread.CurrentPrincipal as CustomPrincipal).Identity.CurrentUser.UserName,
                                                @FieldCaption = this._userDefinedField.FieldCaption,
                                                @IsNumeric = (Convert.ToBoolean(this._userDefinedField.IsNumeric)) ? 1 : 0,
                                                @Published = (Convert.ToBoolean(this._userDefinedField.Published)) ? 1 : 0,
                                            }, transaction);

                    IDbCommand NewIdCommand = new System.Data.OleDb.OleDbCommand();
                    NewIdCommand.Transaction = transaction;
                    NewIdCommand.CommandText = "SELECT @@IDENTITY";
                    NewIdCommand.Connection = db;
                    var newFieldId = NewIdCommand.ExecuteScalar();

                    string query = @"select distinct Assetid from UserDefinedFieldValues";
                    var assetIdsList = (List<string>)db.Query<string>(query, new{},transaction);
                   // assetIdsList.Add("0");

                    foreach (var assetId in assetIdsList)
                    {
                        db.Execute(@"Insert into [UserDefinedFieldValues] (UserDefinedFieldId, FieldValue, CreatedBy, AssetId) 
                                                            values(@UserDefinedFieldId,  @FieldValue, @CreatedBy, @AssetId)",
                                               new
                                               {
                                                   @UserDefinedFieldId = newFieldId,
                                                   @FieldValue = this._userDefinedField.DefaultValue,
                                                   @CreatedBy = (Thread.CurrentPrincipal as CustomPrincipal).Identity.CurrentUser.UserName,
                                                   @AssetId = Convert.ToInt32(assetId)
                                               }, transaction);
                    }


                    transaction.Commit();
                    result = (int)newFieldId;
                }

                return result;
            }

            catch (Exception ex)
            {

                if (transaction != null)
                    transaction.Rollback();
                throw ex;
            }
        }
    }
}