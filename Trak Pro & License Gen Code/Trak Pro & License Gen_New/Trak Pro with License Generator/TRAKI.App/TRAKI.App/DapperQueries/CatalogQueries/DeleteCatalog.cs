﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TRAKI.App.Interfaces;
using Dapper;

namespace TRAKI.App.DapperQueries.CatalogQueries
{
    class DeleteCatalog: IQuery<int>
    {
        private int _catalogId;

        /// <summary>
        /// initializes the instance of the <see cref="DeleteCatalog"/> class.
        /// </summary>
        public DeleteCatalog(int CatalogId)
        {
            this._catalogId = CatalogId;
        }

        /// <summary>
        /// excecutes db query against database
        /// </summary>
        /// <param name="db"></param>
        /// <returns>Number of rows Affected</returns>
        public int Execute(System.Data.IDbConnection db)
        {
            try
            {
                var query = @"Delete from  [Catalog]  WHERE [CatalogId] = @CatalogId";
                int result= db.Execute(query, new { @CatalogId = this._catalogId });
                return result;            
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
