﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using TRAKI.App.Model;
using TRAKI.App.Interfaces;

namespace TRAKI.App.DapperQueries.CatalogQueries
{
    public class SearchCatalog : IQuery<List<Catalog>>
    {
        private string _searchText;
        private int? _isActive;

        /// <summary>
        /// initializes the instance of the <see cref="SearchCatalog"/> class.
        /// </summary>
        public SearchCatalog(string SearchText, int? IsActive)
        {
            this._searchText = SearchText;
            this._isActive = IsActive;

        }


        /// <summary>
        /// excecutes db query against database
        /// </summary>
        /// <param name="db"></param>
        /// <returns>List of Catalogs</returns>
        public List<Catalog> Execute(System.Data.IDbConnection db)
        {

            if (this._isActive > 0)
                this._isActive = null;
            string query = @"SELECT [Catalog].CatalogId,  [Catalog].CatalogNumber, [Catalog].CatalogDescription ,  [Catalog].IsActive
                        from 
                        [Catalog]  where ((@IsActive is null or [Catalog].IsActive = @IsActive)and ([Catalog].CatalogNumber  like '%" + this._searchText + "%' OR [Catalog].CatalogDescription like '%" + this._searchText + "%')) order by [Catalog].CatalogNumber";
            return (List<Catalog>)db.Query<Catalog>(query, new { @IsActive = this._isActive });
        }
    }
}