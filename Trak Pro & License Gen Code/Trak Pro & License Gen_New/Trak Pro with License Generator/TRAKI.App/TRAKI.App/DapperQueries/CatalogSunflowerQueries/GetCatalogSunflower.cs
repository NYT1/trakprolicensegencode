﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TRAKI.App.Interfaces;
using Dapper;
using TRAKI.App.Model;

namespace TRAKI.App.DapperQueries.CatalogSunflowerQueries
{
    public class GetCatalogSunflower : IQuery<List<CatalogSunflower>>
    {
        private int _lineNo;

        public GetCatalogSunflower(int LineNo)
        {
            this._lineNo = LineNo;
        }


        public List<CatalogSunflower> Execute(System.Data.IDbConnection db)
        {

            string query = @"SELECT  catkey, orgmfgcode, mfgname, model,modelname,offname,cdate,sdate,mdate,edate,LineNo FROM nvCatalog where (@LineNo is null or @LineNo=LineNo) order by LineNo";
            return (List<CatalogSunflower>)db.Query<CatalogSunflower>(query,
                new
                {
                    @LineNo = this._lineNo,
                }
         );
        }
    }
}
