﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TRAKI.App.Interfaces;
using Dapper;
using TRAKI.App.Model;


namespace TRAKI.App.DapperQueries.CatalogSunflowerQueries
{
    class UpdateCatalogSun : IQuery<int>
    {
        private CatalogSunflower _catalogSunflower;
        //private string query = "";

        /// <summary>
        /// initializes the instance of the <see cref="UpdateCatlogSun"/> class.
        /// </summary>
        public UpdateCatalogSun(CatalogSunflower CatalogSunflower)
        {
            this._catalogSunflower = CatalogSunflower;
        }

        /// <summary>
        /// excecutes db query against database
        /// </summary>
        /// <param name="db"></param>
        /// <returns>Number of rows Affected</returns>
        public int Execute(System.Data.IDbConnection db)
        {


            try
            {
                int result = db.Execute(@"UPDATE [nvCatalog] 
                                   SET 
                                        catkey=@catkey,
                                        orgmfgcode=@orgmfgcode,
                                        mfgname=@mfgname,
                                        model=@model,
                                        modelname=@modelname,
                                        offname=@offname,
                                        cdate=@cdate,
                                        sdate=@sdate,
                                        mdate=@mdate,
                                        edate=@edate
                			            WHERE [LineNo] = @LineNo",
                                                   new
                                                   {
                                                       @catkey = this._catalogSunflower.Catkey,
                                                       @orgmfgcode = this._catalogSunflower.orgmfgcode,
                                                       @mfgname = this._catalogSunflower.MfgName,
                                                       @model = this._catalogSunflower.Model,
                                                       @modelname = this._catalogSunflower.ModelName,
                                                       @offname = this._catalogSunflower.OffName,
                                                       @cdate = this._catalogSunflower.CDate,
                                                       @sdate = this._catalogSunflower.SDate,
                                                       @mdate = this._catalogSunflower.MDate,
                                                       @edate = this._catalogSunflower.EDate,
                                                       @LineNo = this._catalogSunflower.LineNo
                                                   });
                return result;
            }

            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}
