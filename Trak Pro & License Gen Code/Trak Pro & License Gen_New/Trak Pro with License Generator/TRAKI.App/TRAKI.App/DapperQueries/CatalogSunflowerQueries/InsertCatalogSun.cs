﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TRAKI.App.Interfaces;
using Dapper;
using TRAKI.App.Model;


namespace TRAKI.App.DapperQueries.CatalogSunflowerQueries
{
    class InsertCatalogSun : IQuery<int>
    {
        private CatalogSunflower _catalogsunflwr;

        /// <summary>
        /// initializes the instance of the <see cref="InsertCatalogSun"/> class.
        /// </summary>

        public InsertCatalogSun(CatalogSunflower CatalogSunflower)
        {
            this._catalogsunflwr = CatalogSunflower;
        }

        /// <summary>
        /// excecutes db query against database
        /// </summary>
        /// <param name="db"></param>
        /// <returns>Number of rows Affected</returns>
        /// 
        public int Execute(System.Data.IDbConnection db)
        {
            try
            {

                int result = db.Execute(@"Insert into nvCatalog (catkey,orgmfgcode,mfgname,model,modelname,offname,cdate,sdate,mdate,edate)
                                        Values(@catkey,@orgmfgcode,@mfgname,@model,@modelname,@offname,@cdate,@sdate,@mdate,@edate)",
                                        new
                                        {
                                            @catkey = this._catalogsunflwr.Catkey,
                                            @orgmfgcode = this._catalogsunflwr.orgmfgcode,
                                            @mfgname = this._catalogsunflwr.MfgName,
                                            @model = this._catalogsunflwr.Model,
                                            @modelname = this._catalogsunflwr.ModelName,
                                            @offname = this._catalogsunflwr.OffName,
                                            @cdate = this._catalogsunflwr.CDate,
                                            @sdate = this._catalogsunflwr.SDate,
                                            @mdate = this._catalogsunflwr.MDate,
                                            @edate = this._catalogsunflwr.EDate

                                        });

                return result;
            }

            catch (Exception ex)
            {

                throw ex;
            }
        }


    }
}
