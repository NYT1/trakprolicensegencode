﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TRAKI.App.Interfaces;
using TRAKI.App.Model;
using Dapper;
using System.Data;
using TRAKI.App.Common;

namespace TRAKI.App.DapperQueries.InventoryQueries
{
    class SaveInventoryData : IQuery<int>
    {
        private List<Inventory> _inventoryList;


        /// <summary>
        /// initializes the instance of the <see cref="InsertAsset"/> class.
        /// </summary>
        public SaveInventoryData(List<Inventory> InventoryList)
        {
            this._inventoryList = InventoryList;
        }

        /// <summary>
        /// excecutes db query against database
        /// </summary>
        /// <param name="db"></param>
        /// <returns>Number of rows Affected</returns>
        public int Execute(System.Data.IDbConnection db)
        {
            IDbTransaction transaction = null;
            try
            {
                int result = 0;
                using (transaction = db.BeginTransaction())
                {

                    foreach (var inventory in this._inventoryList)
                    {

                        db.Execute(@"Insert into [Inventory] (
                                        QA,
                                        UserID,
                                        AssetNumber,
                                        BuildingNumber,
                                        RoomNumber ,
                                        InventoryDate,
                                        Description ,
                                        Status
                                        )
                                   values ( 
                                        @QA,
                                        @UserID,
                                        @AssetNumber,
                                        @BuildingNumber,
                                        @RoomNumber ,
                                        @InventoryDate,
                                        @Description, 
                                        @Status
                                        ) ",
                                            new
                                            {
                                                @QA = (Convert.ToBoolean(inventory.QA)) ? 1 : 0,
                                                @UserID = inventory.UserId,
                                                @AssetNumber = inventory.AssetNumber,
                                                @BuildingNumber = inventory.BuildingNumber,
                                                @RoomNumber = inventory.RoomNumber,
                                                @InventoryDate = inventory.InventoryDate,
                                                @Description = inventory.Description,
                                                @Status = inventory.Status
                                            }, transaction);

                        db.Execute(@"Update [Asset] set InventoryDate=@InventoryDate, Status =@Status WHERE AssetNumber=@AssetNumber",
                                        new
                                        {
                                            @InventoryDate = inventory.InventoryDate,
                                            @Status = AssetStatus.Active.ToString(),
                                            @AssetNumber = inventory.AssetNumber
                                        }, transaction);
                    }

                    transaction.Commit();
                    result = 1;
                }

                return result;
            }

            catch (Exception ex)
            {
                if (transaction != null)
                    transaction.Rollback();
                throw ex;
            }
        }
    }
}