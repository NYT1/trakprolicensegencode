﻿using System.Collections.Generic;
using TRAKI.App.Interfaces;
using TRAKI.App.Model;
using Dapper;


namespace TRAKI.App.DapperQueries.InventoryQueries
{
    class ValidateAssetLocation : IQuery<int>
    {
        private Inventory _inventory;

        /// <summary>
        /// initializes the instance of the <see cref="GetPreviousLocation"/> class.
        /// </summary>
        public ValidateAssetLocation(Inventory Inventory)
        {
            this._inventory = Inventory;

        }


        /// <summary>
        /// excecutes db query against database
        /// </summary>
        /// <param name="db"></param>
        /// <returns>Number of rows Affected</returns>
        public int Execute(System.Data.IDbConnection db)
        {
            string query = @"SELECT count(1)
                         from 
                         ((AssetLocation 
                         INNER JOIN
                         Asset on Asset.AssetId =AssetLocation.AssetId)
                         INNER JOIN
                         Building on Building.BuildingNumber =AssetLocation.BuildingNumber)
                         INNER JOIN
                         Room on Room.RoomNumber =AssetLocation.RoomNumber  
                         where (Asset.AssetNumber =@AssetNumber and AssetLocation.BuildingNumber = @BuildingNumber and AssetLocation.RoomNumber = @RoomNumber)";
            var result = (int)db.ExecuteScalar(query, new
            {
                @AssetNumber = this._inventory.AssetNumber,
                @BuildingNumber = this._inventory.BuildingNumber,
                @RoomNumber = this._inventory.RoomNumber
            });

//            if (result <= 0)
//            {
//                query = @"SELECT IIF((count(Asset.AssetNumber) >0), 'Mismatch', 'Overage') AS Status
//                                  FROM Asset
//                                   Inner join UploadData 
//                                                on  asset.AssetNumber = UploadData.AssetNumber
//                                       where (Asset.AssetNumber =@AssetNumber)";
//                _inventory.Status = (string)db.ExecuteScalar(query, new
//                  {
//                      @AssetNumber = this._inventory.AssetNumber,
//                  });
//            }
            return result;
        }
    }
}



