﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TRAKI.App.Interfaces;
using TRAKI.App.Model;
using Dapper;
using System.Data;

namespace TRAKI.App.DapperQueries.InventoryQueries.UploadData
{
    class UpdateUploadData : IQuery<int>
    {
        private List<Inventory> _inventoryList;


        /// <summary>
        /// initializes the instance of the <see cref="InsertAsset"/> class.
        /// </summary>
        public UpdateUploadData(List<Inventory> InventoryList)
        {
            this._inventoryList = InventoryList;
        }

        /// <summary>
        /// excecutes db query against database
        /// </summary>
        /// <param name="db"></param>
        /// <returns>Number of rows Affected</returns>
        public int Execute(System.Data.IDbConnection db)
        {
            IDbTransaction transaction = null;
            try
            {
                int result = 0;
                using (transaction = db.BeginTransaction())
                {

                    foreach (var inventory in this._inventoryList)
                    {

                        result = db.Execute(@"Update [UploadData] set
                                                QA= @QA,
                                                AssetNumber=@AssetNumber,
                                                BuildingNumber= @BuildingNumber,
                                                RoomNumber  = @RoomNumber , 
                                                InventoryDate@InventoryDate,
                                                Description= @Description WHERE UploadDataId=@UploadDataId) ",
                                             new
                                             {
                                                 @QA = (Convert.ToBoolean(inventory.QA)) ? 1 : 0,
                                                 @AssetNumber = inventory.AssetNumber,
                                                 @BuildingNumber = inventory.BuildingNumber,
                                                 @RoomNumber = inventory.RoomNumber,
                                                 @InventoryDate = inventory.InventoryDate,
                                                 @Description = inventory.Description,
                                                 @UploadDataId = inventory.UploadDataId
                                             }, transaction);
                    }

                    transaction.Commit();
                    result = 1;
                }

                return result;
            }

            catch (Exception ex)
            {
                if (transaction != null)
                    transaction.Rollback();
                throw ex;
            }
        }
    }
}