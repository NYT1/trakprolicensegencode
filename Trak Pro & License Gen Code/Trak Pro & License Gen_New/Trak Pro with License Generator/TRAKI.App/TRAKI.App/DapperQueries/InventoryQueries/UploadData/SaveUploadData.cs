﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TRAKI.App.Model;
using Dapper;
using TRAKI.App.Interfaces;
using TRAKI.App.Account;
using System.Threading;

namespace TRAKI.App.DapperQueries.InventoryQueries.UploadData
{
    class SaveUploadData : IQuery<int>
    {
        private List<Inventory> _inventoryList;


        /// <summary>
        /// initializes the instance of the <see cref="InsertAsset"/> class.
        /// </summary>
        public SaveUploadData(List<Inventory> InventoryList)
        {
            this._inventoryList = InventoryList;
        }

        /// <summary>
        /// excecutes db query against database
        /// </summary>
        /// <param name="db"></param>
        /// <returns>Number of rows Affected</returns>
        public int Execute(System.Data.IDbConnection db)
        {
            IDbTransaction transaction = null;
            try
            {
                int result = 0;
                using (transaction = db.BeginTransaction())
                {

                    foreach (var inventory in this._inventoryList)
                    {

                        result = db.Execute(@"Insert into [UploadData] (
                                        QA,
                                        UserID,
                                        AssetNumber,
                                        BuildingNumber,
                                        RoomNumber ,
                                        InventoryDate,
                                        Description,
                                        UploadedBy 
                                        )
                                   values ( 
                                        @QA,
                                        @UserID,
                                        @AssetNumber,
                                        @BuildingNumber,
                                        @RoomNumber ,
                                        @InventoryDate,
                                        @Description,
                                        @UploadedBy                                      
                                        ) ",
                                             new
                                             {
                                                 @QA = inventory.QA,
                                                 @UserID = inventory.UserId,
                                                 @AssetNumber = inventory.AssetNumber,
                                                 @BuildingNumber = inventory.BuildingNumber,
                                                 @RoomNumber = inventory.RoomNumber,
                                                 @InventoryDate = inventory.InventoryDate,
                                                 @Description = inventory.Description,
                                                 @UploadedBy = (Thread.CurrentPrincipal as CustomPrincipal).Identity.CurrentUser.UserName
                                             }, transaction);
                    }

                    transaction.Commit();
                    result = 1;
                }

                return result;
            }

            catch (Exception ex)
            {
                if (transaction != null)
                    transaction.Rollback();
                throw ex;
            }
        }
    }
}