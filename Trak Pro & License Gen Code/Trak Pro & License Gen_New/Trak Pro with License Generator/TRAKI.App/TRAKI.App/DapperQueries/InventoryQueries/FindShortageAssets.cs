﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TRAKI.App.Interfaces;
using TRAKI.App.Model;
using Dapper;

namespace TRAKI.App.DapperQueries.InventoryQueries
{
    class FindShortageAssets : IQuery<List<Inventory>>
    {
        private string _buildingNumber;

        /// <summary>
        /// initializes the instance of the <see cref="FindDiscrepancies"/> class.
        /// </summary>
        public FindShortageAssets(string BuildingNumber)
        {
            this._buildingNumber = BuildingNumber;
        }

        /// <summary>
        /// excecutes db query against database
        /// </summary>
        /// <param name="db"></param>
        /// <returns>Staus of the AssetNumber</returns>
        public List<Inventory> Execute(System.Data.IDbConnection db)
        {

            string query = @"SELECT -1 as QA, '' as UserId ,Asset.AssetNumber,AssetLocation.BuildingNumber,AssetLocation.RoomNumber, Now() as  InventoryDate,''  as Description, 'Shortage' as  Status
                        from
                        AssetLocation 
                         INNER JOIN
                         Asset on Asset.AssetLocationId =AssetLocation.AssetLocationId
                        WHERE NOT  EXISTS (
                                           SELECT AssetNumber
                                             FROM   UploadData 
                                            WHERE asset.AssetNumber = UploadData.AssetNumber
                                          )   
                        and (AssetLocation.BuildingNumber =@BuildingNumber and AssetLocation.IsActive=-1 and Asset.IsDeleted =0)";
            return (List<Inventory>)db.Query<Inventory>(query, new
            {
                @BuildingNumber = this._buildingNumber,
            });
        }
    }
}



