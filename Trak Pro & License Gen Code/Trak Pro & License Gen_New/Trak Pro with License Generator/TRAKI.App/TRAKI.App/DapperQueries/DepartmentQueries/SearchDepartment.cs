﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dapper;
using System.Threading.Tasks;
using TRAKI.App.Model;
using TRAKI.App.Interfaces;

namespace TRAKI.App.DapperQueries.DepartmentQueries
{
    class SearchDepartment : IQuery<List<Department>>
    {
        private string _searchText;
        private int? _sActive;

        /// <summary>
        /// initializes the instance of the <see cref="SearchDepartment"/> class.
        /// </summary>
        public SearchDepartment(string SearchText, int? IsActive)
        {
            this._searchText = SearchText;
            this._sActive = IsActive;
        }

        /// <summary>
        /// excecutes db query against database
        /// </summary>
        /// <param name="db"></param>
        /// <returns>List of Departments</returns>
        public List<Department> Execute(System.Data.IDbConnection db)
        {

            if (this._sActive > 0)
                this._sActive = null;

            string query = @"SELECT DepartmentId,DepartmentNumber, DepartmentDescription, IsActive FROM Department where ((@IsActive is null or Department.IsActive= @IsActive )and (DepartmentNumber   like '%" + this._searchText + "%' OR DepartmentDescription like '%" + this._searchText + "%' )) order by DepartmentNumber";
            return (List<Department>)db.Query<Department>(query, new { @IsActive = this._sActive });
        }
    }
}
