﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TRAKI.App.Interfaces;
using Dapper;
using TRAKI.App.Model;


namespace TRAKI.App.DapperQueries.DepartmentQueries
{
    class UpdateDepartment : IQuery<int>
    {
        private Department _department;

        /// <summary>
        /// initializes the instance of the <see cref="UpdateDepartment"/> class.
        /// </summary>
        public UpdateDepartment(Department Department)
        {
            this._department = Department;
        }

        /// <summary>
        /// excecutes db query against database
        /// </summary>
        /// <param name="db"></param>
        /// <returns>Number of rows Affected</returns>
        public int Execute(System.Data.IDbConnection db)
        {
            try
            {
                int result = db.Execute(@"UPDATE Department
                                   SET 
                                      DepartmentNumber = @DepartmentNumber,
                                      DepartmentDescription = @DepartmentDescription, 
                                      IsActive = @IsActive
                			          WHERE DepartmentId = @DepartmentId",
                                    new
                                    {
                                        @DepartmentNumber = this._department.DepartmentNumber,
                                        @DepartmentDescription = this._department.DepartmentDescription,
                                        @IsActive = (Convert.ToBoolean(this._department.IsActive)) ? 1 : 0,
                                        @DepartmentId = this._department.DepartmentId
                                    });

                return result;
            }

            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}