﻿using TRAKI.App.Interfaces;
using Dapper;
using System;
using TRAKI.App.Model;
using System.Data;


namespace TRAKI.App.DapperQueries.Common
{
    class CheckAssetNumber : IQuery<int>
    {
        private string _assetNumber;
        public CheckAssetNumber(string AssetNumber)
        {
            this._assetNumber = AssetNumber;
        }
        public int Execute(System.Data.IDbConnection db)
        {
            int result = 0;
            IDbTransaction transaction = null;
            try
            {
                using (transaction = db.BeginTransaction())
                {
                    result = (int)db.ExecuteScalar(@"select count(1) as rowCount  from [Asset]   where  assetnumber =@AssetNumber",
                                          new
                                          {
                                              @AssetNumber = this._assetNumber,
                                          }, transaction);
                    if (result >= 1)
                        return result;
                    result = (int)db.ExecuteScalar(@"select count(1) as rowCount from [AssetBarcode]  where AssetBarcode.PreviousAssetNumber=@AssetNumber",
                                          new
                                          {
                                              @AssetNumber = this._assetNumber,
                                          }, transaction);

                    transaction.Commit();
                }
                return result;
            }
            catch (Exception ex)
            {
                if (transaction != null)
                    transaction.Rollback();
                throw ex;
            }
        }
    }
}
