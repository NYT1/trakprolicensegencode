﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TRAKI.App.Interfaces;
using Dapper;
using TRAKI.App.Model;

namespace TRAKI.App.DapperQueries.Common
{
    class CheckRoom : IQuery<int>
    {
        private string _roomNumber;
        public CheckRoom(string RoomNumber)
        {
            this._roomNumber = RoomNumber;
        }
        public int Execute(System.Data.IDbConnection db)
        {
            try
            {
                return (int)db.ExecuteScalar("SELECT count(1) FROM [Room] where [RoomNumber]=@RoomNumber",
                                new
                                {
                                    @RoomNumber = this._roomNumber
                                });

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
