﻿using TRAKI.App.Interfaces;
using Dapper;
using System;
using TRAKI.App.Model;

namespace TRAKI.App.DapperQueries.Common
{
    class CheckAssetBarcodeNumber : IQuery<int>
    {
        private AssetBarcode _assetBarcode;
        public CheckAssetBarcodeNumber(AssetBarcode AssetNumber)
        {
            this._assetBarcode = AssetNumber;
        }
        public int Execute(System.Data.IDbConnection db)
        {
            try
            {
                return (int)db.ExecuteScalar(@"select count(1) from [Asset] where (AssetNumber =@AssetNumber and AssetId <> @AssetId)",
                                      new
                                      {
                                          @AssetNumber = this._assetBarcode.AssetNumber,
                                          @AssetId = this._assetBarcode.AssetId,
                                      });
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}


