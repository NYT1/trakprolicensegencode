﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TRAKI.App.Interfaces;
using TRAKI.App.Model;
using Dapper;

namespace TRAKI.App.DapperQueries.Common
{
    class GetSettings : IQuery<List<Settings>>
    {

        private string _settingItem;

        /// <summary>
        /// initializes the instance of the <see cref="SearchAcquisition"/> class.
        /// </summary>
        public GetSettings(string SettingItem)
        {
            this._settingItem = SettingItem;
        }

        /// <summary>
        /// excecutes db query against database
        /// </summary>
        /// <param name="db"></param>
        /// <returns>List of Settings</returns>
        public List<Settings> Execute(System.Data.IDbConnection db)
        {
            return (List<Settings>)db.Query<Settings>(@"SELECT SettingItem,SettingName, SettingValue, [DisplayOrder] FROM [Settings] where ((@SettingItem is null or SettingItem= @SettingItem)  and  IsActive=true)",

                new
                {
                    @SettingItem = this._settingItem
                });
        }
    }
}
