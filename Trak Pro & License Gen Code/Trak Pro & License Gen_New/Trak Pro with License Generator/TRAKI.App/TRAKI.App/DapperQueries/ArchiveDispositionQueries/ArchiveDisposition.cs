﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TRAKI.App.Interfaces;
using Dapper;
using System.Threading;
using TRAKI.App.Account;


namespace TRAKI.App.DapperQueries.ArchiveDispositionQueries
{
    class ArchiveDisposition: IQuery<int>
    {


        /// <summary>
        /// initializes the instance of the <see cref="DeleteAsset"/> class.
        /// </summary>
        public ArchiveDisposition()
        {
        }


        /// <summary>
        /// excecutes db query against database
        /// </summary>
        /// <param name="db"></param>
        /// <returns>Number of rows Affected</returns>
        public int Execute(System.Data.IDbConnection db)
        {
            try
            {
                var query = @"Update Asset set IsDeleted = -1, ModifiedBy = @ModifiedBy,  ModifiedDate=@ModifiedDate where DispositionCode is not null";
                int result = db.Execute(query,
                    new
                    {
                        @ModifiedBy = (Thread.CurrentPrincipal as CustomPrincipal).Identity.CurrentUser.UserName,
                        @ModifiedDate = DateTime.Now.ToString(),
                    });
             
                return result;
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
