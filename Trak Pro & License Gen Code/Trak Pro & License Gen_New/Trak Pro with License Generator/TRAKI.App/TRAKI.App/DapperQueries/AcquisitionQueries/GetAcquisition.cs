﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TRAKI.App.Interfaces;
using Dapper;
using TRAKI.App.Model;

namespace TRAKI.App.DapperQueries.AcquisitionQueries
{
    class GetAcquisition : IQuery<List<Acquisition>>
    {
        private int? _acquisitionId;
        private int? _isActive;

        /// <summary>
        /// initializes the instance of the <see cref="GetAcquisition"/> class.
        /// </summary>
        public GetAcquisition(int? AcquisitionId, int? IsActive)
        {
            this._acquisitionId = AcquisitionId;
            this._isActive = IsActive;
        }

        /// <summary>
        /// excecutes db query against database
        /// </summary>
        /// <param name="db"></param>
        /// <returns>List of Acquisitions</returns>
        public List<Acquisition> Execute(System.Data.IDbConnection db)
        {

            if (this._isActive > 0)
                this._isActive = null;
            string query = @"SELECT AcquisitionId, AcquisitionCode, AcquisitionDescription, IsActive from [Acquisition] where ((@AcquisitionId is null or [Acquisition].AcquisitionId=@AcquisitionId) and (@IsActive is null or Acquisition.IsActive=@IsActive)) order by AcquisitionCode";
            return (List<Acquisition>)db.Query<Acquisition>(query,
                new
                {
                    @AcquisitionId = this._acquisitionId,
                    @IsActive = this._isActive
                }
            );
        }
    }
}
