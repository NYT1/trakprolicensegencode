﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using TRAKI.App.Model;
using TRAKI.App.Interfaces;

namespace TRAKI.App.DapperQueries.AcquisitionQueries
{
    public class SearchAcquisition : IQuery<List<Acquisition>>
    {
        private string _searchText;
        private int? _sActive;

        /// <summary>
        /// initializes the instance of the <see cref="SearchAcquisition"/> class.
        /// </summary>
        public SearchAcquisition(string SearchText, int? IsActive)
        {
            this._searchText = SearchText;
            this._sActive = IsActive;
        }

        /// <summary>
        /// excecutes db query against database
        /// </summary>
        /// <param name="db"></param>
        /// <returns>List of Acquisitions</returns>
        public List<Acquisition> Execute(System.Data.IDbConnection db)
        {

            if (this._sActive > 0)
                this._sActive = null;

            string query = @"SELECT [Acquisition].AcquisitionId,[Acquisition].AcquisitionCode, [Acquisition].AcquisitionDescription ,[Acquisition].IsActive
                        from 
                        [Acquisition]  where ((@IsActive is null or [Acquisition].IsActive= @IsActive )and ( [Acquisition].AcquisitionCode  like '%" + this._searchText + "%' OR [Acquisition].AcquisitionDescription like '%" + this._searchText + "%')) order by AcquisitionCode";
            return (List<Acquisition>)db.Query<Acquisition>(query, new { @IsActive = this._sActive });
        }
    }
}

