﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TRAKI.App.Interfaces;
using Dapper;
using TRAKI.App.Model;


namespace TRAKI.App.DapperQueries.SystemControlQueries
{
    class InsertSystemControl : IQuery<int>
    {
        private SystemControl _SystemControl;

        public InsertSystemControl(SystemControl SystemControl)
        {
            this._SystemControl = SystemControl;
        }

        public int Execute(System.Data.IDbConnection db)
        {
            try
            {

                int result = db.Execute(@"INSERT INTO [SystemControl] 
                                            ([Path],[Password],[AssetFlag]) VALUES 
                                            (@Path,@Password,@AssetFlag)",
                                    new
                                    {
                                        @Path = this._SystemControl.Path,
                                        @Password = this._SystemControl.Password,
                                        @AssetFlag = this._SystemControl.AssetFlag

                                    });

                //db.Close();
                return result;
            }

            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}