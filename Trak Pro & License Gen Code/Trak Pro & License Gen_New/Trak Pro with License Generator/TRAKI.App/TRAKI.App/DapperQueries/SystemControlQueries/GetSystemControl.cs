﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TRAKI.App.Interfaces;
using Dapper;
using TRAKI.App.Model;

namespace TRAKI.App.DapperQueries.UserQueries
{
    public class GetSystemControl : IQuery<List<SystemControl>>
    {
       
        public List<SystemControl> Execute(System.Data.IDbConnection db)
        {
            string query = string.Empty;
            query = @"SELECT [SystemControl].[SystemControlId], [SystemControl].[Path], [SystemControl].[Password] FROM [SystemControl]";
            return (List<SystemControl>)db.Query<SystemControl>(query);
        }
    }
}
