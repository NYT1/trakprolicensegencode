﻿ 
using Dapper;
using System.Collections.Generic;
using TRAKI.App.Interfaces;
using TRAKI.App.Model;


namespace TRAKI.App.DapperQueries.BuildingQueries
{
    class GetOpenBuildings: IQuery<List<Building>>
    {

        /// <summary>
        /// excecutes db query against database
        /// </summary>
        /// <param name="db"></param>
        /// <returns>List of Open Buildings</returns>
        public List<Building> Execute(System.Data.IDbConnection db)
        {
             string query =  @" SELECT BuildingNumber, BuildingDescription FROM Building where(IsActive =-1 and IsOpen = -1) order by BuildingNumber";
             return (List<Building>)db.Query<Building>(query);
        }
    }
}

