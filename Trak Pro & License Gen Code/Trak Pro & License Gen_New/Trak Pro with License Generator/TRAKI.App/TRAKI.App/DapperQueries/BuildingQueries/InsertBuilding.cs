﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TRAKI.App.Interfaces;
using Dapper;
using TRAKI.App.Model;


namespace TRAKI.App.DapperQueries.BuildingQueries
{
    class InsertBuilding : IQuery<int>
    {
        private Building _Building;

        public InsertBuilding(Building Building)
        {
            this._Building = Building;
        }

        public int Execute(System.Data.IDbConnection db)
        {
            try
            {
                int result = db.Execute(@"Insert into Building ( BuildingNumber ,  BuildingDescription ,AddressLine1 , AddressLine2 , PointOfContact , Title , Phone , Fax, DateSigned, DateReceived, CurrentStatus, BuildingTypeId,IsOpen,IsActive)
                                         values ( @BuildingNumber, @BuildingDescription, @AddressLine1,@AddressLine2, @PointOfContact, @Title,  @Phone, @Fax, @DateSigned, @DateReceived, @CurrentStatus, @BuildingTypeId,@BuildingStatus, @IsActive ) ",
                                  
                                    new
                                    {
                                        @BuildingNumber = this._Building.BuildingNumber,
                                        @BuildingDescription = this._Building.BuildingDescription,
                                        @AddressLine1 = this._Building.AddressLine1,
                                        @AddressLine2 = this._Building.AddressLine2,
                                        @PointOfContact = this._Building.PointOfContact,
                                        @Title = this._Building.Title,
                                        @Phone = this._Building.Phone,
                                        @Fax = this._Building.Fax,
                                         @DateSigned = this._Building.DateSigned,
                                           @DateReceived = this._Building.DateReceived,
                                        @CurrentStatus = this._Building.CurrentStatus,
                                         @BuildingTypeId = this._Building.BuildingTypeId,
                                        @BuildingStatus = (Convert.ToBoolean(this._Building.BuildingStatus)) ? 1 : 0,
                                         @IsActive = (Convert.ToBoolean(this._Building.IsActive)) ? 1 : 0
                                        
                                    });

                //db.Close();
                return result;
            }

            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}