﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TRAKI.App.Interfaces;
using Dapper;
using TRAKI.App.Model;


namespace TRAKI.App.DapperQueries.BuildingQueries
{
    class UpdateBuilding : IQuery<int>
    {
        private Building _Building;

        public UpdateBuilding(Building Building)
        {
            this._Building = Building;
        }

        public int Execute(System.Data.IDbConnection db)
        {
            try
            {

                int result = db.Execute(@"UPDATE [Building] 
                                   SET 
                                      BuildingNumber=@BuildingNumber,
                                      BuildingDescription = @BuildingDescription,
                                      AddressLine1 = @AddressLine1,
                                      AddressLine2 = @AddressLine2,
                                      PointOfContact = @PointOfContact,
                                      Title = @Title,
                                      Phone = @Phone,
                                      Fax = @Fax,
                                      DateSigned = @DateSigned,
                                      DateReceived = @DateReceived,
                                      CurrentStatus = @CurrentStatus,
                                      BuildingTypeId = @BuildingTypeId,
                                      IsActive = @IsActive,
                                      IsOpen = @BuildingStatus
                			          WHERE [BuildingId] = @BuildingId",
                                    new
                                    {
                                       
                                        @BuildingNumber = this._Building.BuildingNumber,
                                        @BuildingDescription = this._Building.BuildingDescription,
                                        @AddressLine1 = this._Building.AddressLine1,
                                        @AddressLine2 = this._Building.AddressLine2,
                                        @PointOfContact = this._Building.PointOfContact,
                                        @Title = this._Building.Title,
                                        @Phone = this._Building.Phone,
                                        @Fax = this._Building.Fax,
                                        @DateSigned = this._Building.DateSigned,
                                        @DateReceived = this._Building.DateReceived,
                                        @CurrentStatus = this._Building.CurrentStatus,
                                        @BuildingTypeId = this._Building.BuildingTypeId,
                                        @IsActive = (Convert.ToBoolean(this._Building.IsActive)) ? 1 : 0,
                                        @BuildingStatus = (Convert.ToBoolean(this._Building.BuildingStatus)) ? 1 : 0,
                                        @BuildingId=this._Building.BuildingId
                                    });

             
                return result;
            }

            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}