﻿using Dapper;
using System.Collections.Generic;
using TRAKI.App.Interfaces;
using TRAKI.App.Model;

namespace TRAKI.App.DapperQueries.ReportQueries
{
    class GetMissingAssetReport :  IQuery<List<Report>>
    {
        /// <summary>
        /// excecutes db query against database
        /// </summary>
        /// <param name="db"></param>
        /// <returns>Inventory Summary</returns>
        public List<Report> Execute(System.Data.IDbConnection db)
        {
            string query = @"Select AssetNumber, Cost, SerialNumber , CatalogDescription, CatalogNumber, BuildingNumber , BuildingDescription ,  RoomNumber from MissingAssetsReport ";
            return (List<Report>)db.Query<Report>(query);
        }
    }
}