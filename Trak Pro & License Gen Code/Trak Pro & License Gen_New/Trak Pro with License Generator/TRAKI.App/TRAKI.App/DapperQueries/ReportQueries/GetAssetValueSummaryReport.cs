﻿using Dapper;
using System.Collections.Generic;
using TRAKI.App.Interfaces;
using TRAKI.App.Model;

namespace TRAKI.App.DapperQueries.ReportQueries
{
    class GetAssetValueSummaryReport : IQuery<List<Report>>
    {

        /// <summary>
        /// excecutes db query against database
        /// </summary>
        /// <param name="db"></param>
        /// <returns>Asset Value Summary</returns>
        public List<Report> Execute(System.Data.IDbConnection db)
        {
            string query = @"SELECT BuildingNumber,BuildingDescription, Cost From AssetValueSummaryReport";
            return (List<Report>)db.Query<Report>(query);

        }
    }
}
