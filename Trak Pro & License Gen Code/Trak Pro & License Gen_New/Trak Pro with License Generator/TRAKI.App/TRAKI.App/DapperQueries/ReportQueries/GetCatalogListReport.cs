﻿using System.Collections.Generic;
using TRAKI.App.Interfaces;
using TRAKI.App.Model;
using Dapper;

namespace TRAKI.App.DapperQueries.ReportQueries
{
    class GetCatalogListReport: IQuery<List<Catalog>>
    {


        /// <summary>
        /// excecutes db query against database
        /// </summary>
        /// <param name="db"></param>
        /// <returns>List of Catalogs</returns>
        public List<Catalog> Execute(System.Data.IDbConnection db)
        {

            string query = @"SELECT  CatalogNumber, CatalogDescription from [Catalog] where([Catalog].IsActive=-1) order by CatalogDescription";
            return (List<Catalog>)db.Query<Catalog>(query);

        }
    }
}

