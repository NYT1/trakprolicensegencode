﻿using TRAKI.App.Model;
using Dapper;
using TRAKI.App.Interfaces;
using System.Collections.Generic;

namespace TRAKI.App.DapperQueries.ReportQueries
{
    class GetDispositionReport : IQuery<List<Report>>
    {

        private string _dispositionCode;

        /// <summary>
        /// initializes the instance of the <see cref="GetDispositionReport"/> class.
        /// </summary>
        public GetDispositionReport(string DispositionCode)
        {
            this._dispositionCode = DispositionCode;
        }

        /// <summary>
        /// excecutes db query against database
        /// </summary>
        /// <param name="db"></param>
        /// <returns>Asset Value Summary</returns>
        public List<Report> Execute(System.Data.IDbConnection db)
        {
            string query = @"SELECT Asset.AssetNumber, Asset.SerialNumber, Asset.ModelNumber, Asset.Cost, Manufacturer.ManufacturerName,  
                                  [Asset].CatalogNumber, [Catalog].CatalogDescription,  Building.BuildingNumber, Asset.DateAcquired, '' as [Condition]
                                from ((((Asset 
                                INNER JOIN
                                [Catalog] on [Catalog].CatalogNumber =Asset.CatalogNumber)
                                LEFT JOIN
                                 Manufacturer on Manufacturer.ManufacturerCode =Asset.ManufacturerCode)
                                LEFT JOIN
                                 Disposition on Disposition.DispositionCode =Asset.DispositionCode)
                                INNER JOIN
                                AssetLocation on AssetLocation.AssetLocationId= Asset.AssetLocationId)
                                INNER JOIN
                                Building on Building.BuildingNumber =AssetLocation.BuildingNumber where (Asset.IsDeleted=0 and Asset.DispositionCode =@DispositionCode)";
            return (List<Report>)db.Query<Report>(query, new
            {
                @DispositionCode = this._dispositionCode
            }); 
        }
    }
}
        
        
        