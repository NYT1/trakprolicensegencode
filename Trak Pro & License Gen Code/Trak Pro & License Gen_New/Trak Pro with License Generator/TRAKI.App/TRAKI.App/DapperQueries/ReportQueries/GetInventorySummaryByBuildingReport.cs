﻿using Dapper;
using System.Collections.Generic;
using TRAKI.App.Interfaces;
using TRAKI.App.Model;

namespace TRAKI.App.DapperQueries.ReportQueries
{
    class GetInventorySummaryByBuildingReport : IQuery<List<Report>>
    {
    
        /// <summary>
        /// excecutes db query against database
        /// </summary>
        /// <param name="db"></param>
        /// <returns>Inventory Summary</returns>
        public List<Report> Execute(System.Data.IDbConnection db)
        {
            string query = @"Select BuildingNumber, BuildingDescription , DateSigned, CurrentStatus, BuildingType, TotalCost , MissingCost ,  MissingCostPercentage from InventorySummarybyBuildingReport ";
            return (List<Report>)db.Query<Report>(query);
        }
    }
}