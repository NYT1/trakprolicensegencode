﻿using Dapper;
using System.Collections.Generic;
using TRAKI.App.Interfaces;
using TRAKI.App.Model;

namespace TRAKI.App.DapperQueries.ReportQueries
{
    class GetPerkinsReport : IQuery<List<Report>>
    {
        /// <summary>
        /// excecutes db query against database
        /// </summary>
        /// <param name="db"></param>
        /// <returns>Asset Value Summary</returns>
        public List<Report> Execute(System.Data.IDbConnection db)
        {
            string query = @"SELECT  Asset.AssetNumber,  Asset.SerialNumber,  Asset.Cost ,  Asset.InventoryDate, Building.BuildingNumber,  Building.BuildingDescription , AssetLocation.RoomNumber, '' as PONumber
                        from (Asset 
                        INNER JOIN
                        AssetLocation on AssetLocation.AssetLocationId= Asset.AssetLocationId)
                        INNER JOIN
                        Building on Building.BuildingNumber =AssetLocation.BuildingNumber  Where (Asset.AssetNumber like 'P%' and Asset.IsDeleted=0)";
            return (List<Report>)db.Query<Report>(query);
        }
    }
}
        
        
        
        
       