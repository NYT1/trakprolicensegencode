﻿using Dapper;
using System.Collections.Generic;
using TRAKI.App.Interfaces;
using TRAKI.App.Model;

namespace TRAKI.App.DapperQueries.ReportQueries
{
    class GetCafeteriaReport : IQuery<List<Report>>
    {
       
        /// <summary>
        /// excecutes db query against database
        /// </summary>
        /// <param name="db"></param>
        /// <returns>Asset Value Summary</returns>
        public List<Report> Execute(System.Data.IDbConnection db)
        {
            string query = @"Select BuildingNumber, BuildingDescription , DateReceived, CurrentStatus, TotalCost  from 
						(
						SELECT    B.BuildingNumber,    B.BuildingDescription ,B.DateReceived,B.CurrentStatus,       
                                        ( 
                                        Select sum(AST.cost)
                                        from (Asset AST
												INNER JOIN 
												AssetLocation ASL on ASL.AssetLocationId=  AST.AssetLocationId)
												INNER JOIN
												Building BL on BL.BuildingNumber =ASL.BuildingNumber
                                                     Where ( ASL.IsActive=-1  and AST.IsDeleted=0 and ASL.BuildingNumber= B.BuildingNumber and AST.AssetNumber like 'C%')
                                                group by  BL.BuildingNumber,  BL.BuildingDescription , BL.DateSigned, BL.CurrentStatus

                                                  ) as TotalCost 
												from (Asset A
												INNER JOIN 
												AssetLocation AL on AL.AssetLocationId=  A.AssetLocationId)
												INNER JOIN
												Building B on B.BuildingNumber =AL.BuildingNumber Where (A.IsDeleted=0 and  A.AssetNumber like 'C%' ))";
            return (List<Report>)db.Query<Report>(query);

        }
    }
}
        
        
        
        
       