﻿using Dapper;
using System.Collections.Generic;
using TRAKI.App.Interfaces;
using TRAKI.App.Model;

namespace TRAKI.App.DapperQueries.ReportQueries
{
    class GetAssetListByBuildingReport : IQuery<List<Report>>
    {
        private string _startingBuildingNumber;
        private string _endingBuildingNumber;
        /// <summary>
        /// initializes the instance of the <see cref="GetAssetValueSummaryReport"/> class.
        /// </summary>
        public GetAssetListByBuildingReport(string StartingBuildingNumber,string EndingBuildingNumber)
        {
            this._endingBuildingNumber= EndingBuildingNumber;
            this._startingBuildingNumber=StartingBuildingNumber;
        }

        /// <summary>
        /// excecutes db query against database
        /// </summary>
        /// <param name="db"></param>
        /// <returns>Asset Value Summary</returns>
        public List<Report> Execute(System.Data.IDbConnection db)
        {
            string query = @"SELECT AssetNumber,  SerialNumber,  Cost ,  InventoryDate, BuildingNumber,  BuildingDescription, CatalogNumber,
                       CatalogDescription, RoomNumber
                        from AssetListbyBuildingReport Where ((@StartingBuildingNumber is null or @EndingBuildingNumber is null ) OR  (BuildingNumber between @StartingBuildingNumber and @EndingBuildingNumber))";
            return (List<Report>)db.Query<Report>(query, new
            {
                @StartingBuildingNumber = (string.IsNullOrEmpty(this._startingBuildingNumber))?null:this._startingBuildingNumber,
                @EndingBuildingNumber = (string.IsNullOrEmpty(this._endingBuildingNumber)) ? null : this._endingBuildingNumber
            });

        }
    }
}
        
        
        
        
       