﻿using System.Collections.Generic;
using TRAKI.App.Interfaces;
using Dapper;
using TRAKI.App.Model;

namespace TRAKI.App.DapperQueries.ReportQueries
{
    class GetDiscrepancyReport : IQuery<List<Report>>
    {
        private string _discrepancyType;
        private IEnumerable<string> _buildingNumbers;
        /// <summary>
        /// initializes the instance of the <see cref="GetAssetValueSummaryReport"/> class.
        /// </summary>
        public GetDiscrepancyReport(IEnumerable<string> BuildingNumbers, string DiscrepancyType)
        {
            this._buildingNumbers = BuildingNumbers;
            this._discrepancyType = DiscrepancyType;
        }
        /// <summary>
        /// excecutes db query against database
        /// </summary>
        /// <param name="db"></param>
        /// <returns>Discrepancy Asset Report List</returns>
        public List<Report> Execute(System.Data.IDbConnection db)
        {
            string query = @"SELECT AssetNumber,  Status as CurrentStatus , InventoryDate, BuildingNumber,  Description as BuildingDescription, RoomNumber
                        from Inventory Where ((@DiscrepancyType is null or Status = @DiscrepancyType) and  (BuildingNumber in  (" + string.Format("'{0}'", string.Join("','", _buildingNumbers)) + ")))";
            return (List<Report>)db.Query<Report>(query, new
            {
                @DiscrepancyType = (string.IsNullOrEmpty(this._discrepancyType)) ? null : this._discrepancyType,
            });

        }
    }
}


