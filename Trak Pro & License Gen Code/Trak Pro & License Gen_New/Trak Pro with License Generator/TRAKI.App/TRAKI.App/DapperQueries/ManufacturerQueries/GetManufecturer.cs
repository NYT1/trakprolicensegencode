﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TRAKI.App.Interfaces;
using Dapper;
using TRAKI.App.Model;

namespace TRAKI.App.DapperQueries.ManufacturerQueries
{
    public class GetManufacturer : IQuery<List<Manufacturer>>
    {
        private int? _manufacturerId;
        private int? _isActive;

        /// <summary>
        /// initializes the instance of the <see cref="GetManufacturer"/> class.
        /// </summary>
        public GetManufacturer(int? ManufacturerId, int? IsActive)
        {
            this._manufacturerId = ManufacturerId;
            this._isActive = IsActive;
        }

        /// <summary>
        /// excecutes db query against database
        /// </summary>
        /// <param name="db"></param>
        /// <returns>List of Manufacturers</returns>
        public List<Manufacturer> Execute(System.Data.IDbConnection db)
        {
            if (this._isActive > 0)
                this._isActive = null;

            string query = @"SELECT ManufacturerId, ManufacturerCode, ManufacturerName, IsActive from [Manufacturer]
                                where ( (@ManufacturerId is null or [Manufacturer].ManufacturerId=@ManufacturerId) and (@IsActive is null or [Manufacturer].IsActive =@IsActive)) order by  [Manufacturer].ManufacturerCode";
            return (List<Manufacturer>)db.Query<Manufacturer>(query,
               new
               {
                   @ManufacturerId = this._manufacturerId,
                   @IsActive = this._isActive
               });

        }
    }
}

