﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using TRAKI.App.Interfaces;
using TRAKI.App.Model;

namespace TRAKI.App.DapperQueries.ManufacturerQueries
{
    public class InsertManufacturer: IQuery<int>
    {
        private Manufacturer _Manufacturer;

        /// <summary>
        /// initializes the instance of the <see cref="InsertManufacturer"/> class.
        /// </summary>
        public InsertManufacturer(Manufacturer Manufacturer)
        {
            this._Manufacturer = Manufacturer;
        }

        /// <summary>
        /// excecutes db query against database
        /// </summary>
        /// <param name="db"></param>
        /// <returns>Number of rows Affected</returns>
        public int Execute(System.Data.IDbConnection db)
        {
            try
            {
                 int result= db.Execute(@"Insert into [Manufacturer] ( ManufacturerCode , ManufacturerName , IsActive )
                                         values ( @ManufacturerCode, @ManufacturerName,   @IsActive ) ",
                                    new                      
                                    {
                                        @ManufacturerCode = this._Manufacturer.ManufacturerCode,
                                        @ManufacturerName = this._Manufacturer.ManufacturerName,
                                        @IsActive = (Convert.ToBoolean(this._Manufacturer.IsActive)) ? 1 : 0 ,
                                    });

                return result;  
            }

            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}