﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TRAKI.App.Interfaces;
using Dapper;
using TRAKI.App.Model;


namespace TRAKI.App.DapperQueries.UserQueries
{
    class UpdateUser : IQuery<int>
    {
        private User _user;

        /// <summary>
        /// initializes the instance of the <see cref="UpdateUser"/> class.
        /// </summary>
        public UpdateUser(User User)
        {
            this._user = User;
        }

        /// <summary>
        /// excecutes db query against database
        /// </summary>
        /// <param name="db"></param>
        /// <returns>Number of rows Affected</returns>
        public int Execute(System.Data.IDbConnection db)
        {
            try
            {
                int result= db.Execute(@"UPDATE [User]
                                   SET 
                                      FirstName = @FirstName, 
                                      LastName = @LastName, 
                                      Email = @Email,
                                      UserName = @UserName,
                                      [Password] = @Password,
                                      RoleId = @RoleId, 
                                      IsActive = @IsActive,
                                      Gender =  @Gender
                			          WHERE [UserId] = @UserId",
                                    new                      
                                    {
                                        @FirstName = this._user.FirstName,
                                        @LastName = this._user.LastName,
                                        @Email = this._user.Email,
                                        @UserName = this._user.UserName,
                                        @Password = this._user.Password,
                                        @RoleId =this._user.RoleId,
                                        @IsActive = (Convert.ToBoolean(this._user.IsActive)) ? 1 : 0 ,
                                        @Gender = this._user.Gender.ToString(),
                                        @UserId = this._user.UserId
                                    });

                return result;  
            }

            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}