﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TRAKI.App.Interfaces;
using Dapper;
using TRAKI.App.Model;

namespace TRAKI.App.DapperQueries.UserQueries
{
    public class GetUsers : IQuery<List<User>>
    {
        private int? _userID;
        private int? _roleId;

        /// <summary>
        /// initializes the instance of the <see cref="GetUsers"/> class.
        /// </summary>
        public GetUsers(int? UserId, int? RoleId)
        {
            this._userID = UserId;
            this._roleId = RoleId;
        }

        /// <summary>
        /// excecutes db query against database
        /// </summary>
        /// <param name="db"></param>
        /// <returns>List of the Users</returns>
        public List<User> Execute(System.Data.IDbConnection db)
        {
            string query = string.Empty;

            if (this._roleId == 0)
                this._roleId = null;
            query = @"SELECT  User.Userid, User.FirstName,User.LastName, User.Email, User.UserName, User.Password,  User.Password as ConfirmPassword, User.Gender,   User.IsActive,  User.RoleId, ROLE.Name as Role  FROM [User] INNER JOIN [Role] ON User.RoleId = ROLE.RoleId where (( @UserId is null or  User.Userid=@UserId ) and (@RoleId is null or User.RoleId=@RoleId)) order by FirstName";
            return (List<User>)db.Query<User>(query,
                new
                {
                    @UserId = this._userID,
                    @RoleId = this._roleId
                });
        }
    }
}
