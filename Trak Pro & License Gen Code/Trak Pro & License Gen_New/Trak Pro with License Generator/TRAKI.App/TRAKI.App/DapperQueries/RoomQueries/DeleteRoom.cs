﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TRAKI.App.Interfaces;
using Dapper;
using TRAKI.App.Model;


namespace TRAKI.App.DapperQueries.RoomQueries
{
  public class DeleteRoom : IQuery<int>
    {
      private int _roomID;

        public DeleteRoom(int RoomId)
        {
            this._roomID = RoomId;
        }
        public int Execute(System.Data.IDbConnection db)
        {
            try
            {
                var query = @"Delete from  Room  WHERE [RoomId] = @RoomId";
                int result = db.Execute(query, new { @RoomId = this._roomID });
             
                return result;            
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
