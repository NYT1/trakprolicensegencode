﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TRAKI.App.Interfaces;
using Dapper;
using TRAKI.App.Model;
using TRAKI.App.Common;

namespace TRAKI.App.DapperQueries.RoomQueries
{
    class InsertRoom : IQuery<int>
    {
        private Room _room;

        public InsertRoom(Room Room)
        {
            this._room = Room;
        }

        public int Execute(System.Data.IDbConnection db)
        {
            try
            {
                int result = db.Execute(@"Insert into Room (RoomNumber,RoomDescription,BuildingId,IsActive)
                                         values (@RoomNumber,@RoomDescription, @BuildingId,@IsActive) ",
                                    new                      
                                    {
                                        @RoomId=this._room.RoomId,
                                         @RoomNumber=this._room.RoomNumber,
                                        @RoomDescription = this._room.RoomDescription,
                                        @BuildingId = this._room.BuildingId,
                                        @IsActive = (Convert.ToBoolean(this._room.IsActive)) ? 1 : 0
                                    });

                return result;  
            }

            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}