﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TRAKI.App.Account;
using TRAKI.App.Common;
using TRAKI.App.DapperQueries.InventoryQueries;
using TRAKI.App.DapperQueries.InventoryQueries.UploadData;
using TRAKI.App.Interfaces;
using TRAKI.App.Model;

namespace TRAKI.App.ViewModel
{
    class InventoryViewModel : INotifyPropertyChanged, IViewModel
    {
        private ObservableCollection<Inventory> InventoryList;
        protected IDbContext dbContext;
        private bool _emptyMessageVisibility = false;
        // private bool _saveAndCloseButtonVisibility = false;
        private long _recordCount = 0;

        public InventoryViewModel()
        {
            this.EmptyMessageVisibility = true;
        }


        #region Private
        /// <summary>
        /// Method to preform the QA ie if Location is valid it sets QA to true, false otherwise
        /// </summary>
        /// <param name="Inventory Object"></param>
        /// <returns>List of Inventory Objects</returns>
        /// 
        private void PerformQA(Inventory inventory)
        {
            try
            {
                GetConnection getConnection = new GetConnection();
                dbContext = getConnection.InitializeConnection();
                var locationQuery = new ValidateAssetLocation(inventory);
                if (dbContext.Execute(locationQuery) > 0)
                {
                    inventory.QA = BooleanValue.True;
                }
                else
                {
                    inventory.QA = BooleanValue.False;
                }

                // return inventory;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Method to find Discrepancies in the Inventory Data 
        /// </summary>
        /// <param name="Inventory Object"></param>
        /// <returns>List of Inventory Objects</returns>
        /// 
        private void FindDiscrepancy(Inventory inventory)
        {
            try
            {
                GetConnection getConnection = new GetConnection();
                dbContext = getConnection.InitializeConnection();
                var query = new FindDiscrepancies(inventory);
                // if (dbContext.Execute(locationQuery) > 0)
                inventory.Status = dbContext.Execute(query);
                // else
                //  inventory.QA = BooleanValue.False;

                //  return inventory;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }




        /// <summary>
        /// Method to move the upload data file to the spcified destination.
        /// </summary>
        /// <param name="Source file path"></param>
        /// <returns>True if filed moved successfully , false otherwise</returns>
        /// 
        public bool MoveFile(string SrcPath)
        {
            try
            {
                string FileName = Path.GetFileNameWithoutExtension(SrcPath);
                FileName = FileName + "_" + (Thread.CurrentPrincipal as CustomPrincipal).Identity.CurrentUser.UserName + "_" + DateTime.Now.ToString("yyyyMMddHHmm") + ".TXT";

                var DestDir = System.Configuration.ConfigurationManager.AppSettings["Path"];

                if (!Directory.Exists(DestDir))
                {
                    Directory.CreateDirectory(DestDir);
                }
                string DestPath = DestDir + @"\" + FileName;
                if (File.Exists(DestPath))
                    File.Delete(DestPath);

                File.SetAttributes(SrcPath, FileAttributes.Normal);
                File.Move(SrcPath, DestPath);
                if (File.Exists(DestPath))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        #endregion

        #region Internal

        /// <summary>
        /// Method to UPload and read The File Contents
        /// </summary>
        /// <param name="SearchText"></param>
        /// <param name="IsActive"></param>
        /// <returns>List of Inventory Objects</returns>
        /// 
        internal ObservableCollection<Inventory> UploadAndGetFileData(string FilePath)
        {
            // int counter = 0;
            string line = string.Empty;
            ObservableCollection<Inventory> InventoryList = new ObservableCollection<Inventory>();

            try
            {   // Open the text file using a stream reader.
                using (StreamReader sr = new StreamReader(FilePath))
                {
                    // Read the stream to a string, and write the string to the console.
                    while ((!string.IsNullOrEmpty(line = sr.ReadLine())))
                    {
                        Inventory inventory = new Inventory();
                        // line = sr.ReadLine();
                        inventory.UserId = line.Substring(0, 4).Trim();
                        inventory.BuildingNumber = line.Substring(4, 5).Trim();
                        inventory.RoomNumber = line.Substring(9, 7).Trim();
                        inventory.AssetNumber = line.Substring(16, 15).Trim();
                        inventory.Description = line.Substring(31, 3).Trim();
                        string date = line.Substring(34, 10).Trim();
                        date = date + " " + line.Substring(44, 8).Trim();
                        inventory.InventoryDate = Convert.ToDateTime(date);

                        this.PerformQA(inventory);
                        InventoryList.Add(inventory);
                    }
                }
                this.RecordCount = InventoryList.Count;
                if (InventoryList.Count > 0)
                {
                    this.EmptyMessageVisibility = false;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return InventoryList;
        }

        /// <summary>
        /// Method to Save the Uploaded Data 
        /// </summary>
        /// <param name="List of Inventory objects"></param>
        /// <returns>Result of the Operation</returns>
        /// 
        internal int SaveUploadData(List<Inventory> InventoryList)
        {
            try
            {
                GetConnection getConnection = new GetConnection();
                dbContext = getConnection.InitializeConnection();
                var query = new SaveUploadData(InventoryList);
                int result = dbContext.Execute(query);
                if (result > 0)
                {
                    this.RecordCount = 0;
                    this.EmptyMessageVisibility = true;
                }

                return result;

            }
            catch (Exception e)
            {
                throw e;
            }
        }


        /// <summary>
        /// Method to Save the Uploaded Data 
        /// </summary>
        /// <param name="List of Inventory objects"></param>
        /// <returns>Result of the Operation</returns>
        /// 
        internal int UpdateUploadData(List<Inventory> InventoryList)
        {
            try
            {
                GetConnection getConnection = new GetConnection();
                dbContext = getConnection.InitializeConnection();
                var query = new UpdateUploadData(InventoryList);
                int result = dbContext.Execute(query);
                if (result > 0)
                {
                    this.RecordCount = 0;
                    this.EmptyMessageVisibility = true;
                    //  this.SaveAndCloseButtonVisibility = false;
                }
                return result;

            }
            catch (Exception e)
            {
                throw e;
            }
        }



        /// <summary>
        /// Method to Save the Data to Inventory
        /// </summary>
        /// <param name="List of Inventory objects"></param>
        /// <returns>Result of the Operation</returns>
        /// 
        internal int SaveInventoryData(List<Inventory> inventoryList)
        {
            try
            {
                GetConnection getConnection = new GetConnection();
                dbContext = getConnection.InitializeConnection();
                List<Inventory> _inventoryList = new List<Inventory>();
                var shortageList = inventoryList.GroupBy(P => P.BuildingNumber).ToList();
                foreach (var item in shortageList)
                {
                    var shortageQuery = new FindShortageAssets(item.Key);
                    // var d = (List<Inventory>)dbContext.Execute(query);
                    _inventoryList.AddRange((List<Inventory>)dbContext.Execute(shortageQuery));
                }
                //   this.InventoryList.Union()
                //else
                //{
                //    this.EmptyMessageVisibility = true;
                //}
                inventoryList.AddRange(_inventoryList);

                var query = new SaveInventoryData(inventoryList);
                int result = dbContext.Execute(query);
                if (result > 0)
                {
                    this.RecordCount = 0;
                    this.EmptyMessageVisibility = true;
                    //  this.SaveAndCloseButtonVisibility = true;
                    this.DeleteAllUploadData();
                }

                return result;

            }
            catch (Exception e)
            {
                throw e;
            }
        }


        /// <summary>
        /// Method to Delete all upload data that is in local storage.
        /// </summary>
        /// <returns>Result of the Operation</returns>
        /// 
        internal int DeleteAllUploadData()
        {
            try
            {
                GetConnection getConnection = new GetConnection();
                dbContext = getConnection.InitializeConnection();
                var query = new DeleteAllUploadData();
                int result = dbContext.Execute(query);
                if (result > 0)
                {
                    this.RecordCount = 0;
                    this.EmptyMessageVisibility = true;
                    // this.SaveAndCloseButtonVisibility = false;
                }

                return result;

            }
            catch (Exception e)
            {
                throw e;
            }
        }




        /// <summary>
        /// Method to find Discrepancies in the Inventory Data 
        /// </summary>
        /// <param name="Inventory Object"></param>
        /// <returns>List of Inventory Objects</returns>
        /// 
        internal Inventory FindEditRecordDiscrepancy(Inventory inventory)
        {
            try
            {
                GetConnection getConnection = new GetConnection();
                dbContext = getConnection.InitializeConnection();
                this.PerformQA(inventory);
                if (inventory.QA == BooleanValue.False)
                {
                    var query = new FindEditRecordDiscrepancy(inventory);
                    inventory.Status = dbContext.Execute(query);
                    inventory.QA = BooleanValue.True;
                }
                return inventory;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #region Upload Data


        /// <summary>
        /// Method to return the list of Assets
        /// </summary>
        /// <param name="MinIdValue"></param>
        /// <param name="MaxIdValue"></param>
        /// <param name="OperatorValue"></param>
        /// <param name="SearchText"></param>
        /// <param name="NumberOfRecords"></param>
        /// <returns>List of Upload Data</returns>
        /// 
        internal ObservableCollection<Inventory> GetUploadData(long? MinIdValue, long? MaxIdValue, string OperatorValue, string SearchText, int NumberOfRecords)
        {
            try
            {
                GetConnection getConnection = new GetConnection();
                dbContext = getConnection.InitializeConnection();
                var uploadQuery = new GetUploadData(MinIdValue, MaxIdValue, OperatorValue, SearchText, NumberOfRecords);
                InventoryList = new ObservableCollection<Inventory>(dbContext.Execute(uploadQuery));
                if (InventoryList != null || InventoryList.Count > 0)
                {
                    foreach (Inventory inventory in InventoryList)
                    {
                        //this.PerformQA(inventory);
                        if (inventory.QA == BooleanValue.False)
                            this.FindDiscrepancy(inventory);
                    }
                    this.EmptyMessageVisibility = false;
                    //  this.SaveAndCloseButtonVisibility = true;

                }
                //List<Inventory> _inventoryList = new List<Inventory>();
                //var dd = this.InventoryList.GroupBy(P => P.BuildingNumber).ToList();
                //foreach (var item in dd)
                //{

                //    var query = new FindShortageAssets(item.Key);
                //   // var d = (List<Inventory>)dbContext.Execute(query);
                //    _inventoryList.AddRange((List<Inventory>)dbContext.Execute(query));
                //}
                //   this.InventoryList.Union()
                //else
                //{
                //    this.EmptyMessageVisibility = true;
                //}
                return InventoryList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// Method to get the Minimum and maximum of the Id's based on MinIdValue
        /// </summary>
        /// <param name="MinIdValue"></param>
        /// <param name="GetLastRecords"></param>
        /// <param name="SearchText"></param>
        /// <param name="NumberOfRecords"></param>
        /// <returns>List of Upload Data</returns>
        /// 
        internal List<Inventory> GetMinAndMaxIdValuesForPager(long? IdValue, string OperatorValue, string SearchText, bool GetLastRecords, int NumberOfRecords)
        {
            try
            {
                GetConnection getConnection = new GetConnection();
                dbContext = getConnection.InitializeConnection();
                var assetQuery = new GetMinAndMaxIdValuesForPager(IdValue, OperatorValue, SearchText, GetLastRecords, NumberOfRecords);//(">", "> 0");
                return dbContext.Execute(assetQuery);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Method to Asset Record count based on search criteria and Status
        /// </summary>
        /// <param name="SearchText"></param>
        /// <param name="Status"></param>
        /// <returns>returns the operation result</returns>
        /// 
        internal long GetUploadDataRecordCount(string SearchText)
        {
            try
            {
                GetConnection getConnection = new GetConnection();
                dbContext = getConnection.InitializeConnection();
                var countQuery = new GetUploadDataCount(SearchText);//(">", "> 0");
                this.RecordCount = dbContext.Execute(countQuery);
                return this.RecordCount;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion


        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region Private Helpers

        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));

            }
        }

        #endregion

        #region Properties


        public long RecordCount
        {
            get { return _recordCount; }
            set
            {
                _recordCount = value;
                NotifyPropertyChanged("RecordCount");
            }
        }


        #region IViewModel Properties

        public bool EmptyMessageVisibility
        {
            get { return _emptyMessageVisibility; }
            set
            {
                _emptyMessageVisibility = value;
                NotifyPropertyChanged("EmptyMessageVisibility");
            }
        }
        //public bool SaveAndCloseButtonVisibility
        //{
        //    get { return _saveAndCloseButtonVisibility; }
        //    set
        //    {
        //        _saveAndCloseButtonVisibility = value;
        //        NotifyPropertyChanged("SaveAndCloseButtonVisibility");
        //    }
        //}



        public int LoggedInUserRoleId
        {
            get
            {
                return (Thread.CurrentPrincipal as CustomPrincipal).Identity.CurrentUser.RoleId;
            }
        }

        public string LoggedInUserName
        {
            get { throw new NotImplementedException(); }
        }

        #endregion
        #endregion

    }
}


