﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TRAKI.App.Account;
using TRAKI.App.Common;
using TRAKI.App.DapperQueries.AssetQueries;
using TRAKI.App.DapperQueries.Common;
using TRAKI.App.Interfaces;
using TRAKI.App.Model;

namespace TRAKI.App.ViewModel
{
    class AssetBarcodeViewModel : INotifyPropertyChanged, IViewModel
    {

        private long _recordCount = 0;
       // private string _assetNumber = string.Empty;
        protected IDbContext dbContext;
        private bool _emptyMessageVisibility = false;
        public ObservableCollection<AssetBarcode> BarcodeList { get; private set; }

        #region Internal

        /// <summary>
        /// Method to Asset Record count based on search criteria and Status
        /// </summary>
        /// <param name="SearchText"></param>
        /// <param name="Status"></param>
        /// <returns>returns the operation result</returns>
        /// 
        internal long GetAssetRecordCount(string SearchText, string Status, int IsArchived)
        {
            try
            {
                GetConnection getConnection = new GetConnection();
                dbContext = getConnection.InitializeConnection();
                var countQuery = new GetAssetCount(SearchText, Status, IsArchived);//(">", "> 0");
                this.RecordCount = dbContext.Execute(countQuery);
                return this.RecordCount;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// Method to Get Asset Barcode Details
        /// </summary>
        /// <param name="AssetId"></param>
        /// <returns>returns the Asset Barcode detail as list</returns>
        /// 
        internal List<AssetBarcode> GetAssetBarcodeDetail(int AssetId)
        {
            try
            {
                GetConnection getConnection = new GetConnection();
                dbContext = getConnection.InitializeConnection();
                var assetQuery = new GetAssetBarcode(AssetId);
                BarcodeList = new ObservableCollection<AssetBarcode>(dbContext.Execute(assetQuery)); 
                this.RecordCount = BarcodeList.Count;
                if (BarcodeList.Count <= 1 && BarcodeList[0].RetagDate == null)
                {
                    this.EmptyMessageVisibility = true;
                    BarcodeList[0].PreviousAssetNumber = BarcodeList[0].AssetNumber;
                }
               // AssetNumber = BarcodeList[0].AssetNumber;
                return BarcodeList.Where(assetBarcode => assetBarcode.RetagDate != null).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// Update the barcode of an existing Asset
        /// </summary>
        /// <param name="AssetId"></param>
        /// <returns>returns the result of the Operation</returns>
        /// 
        internal int InsertUpdateBarCode(AssetBarcode assetBarcode)
        {
            try
            {
                GetConnection getConnection = new GetConnection();
                dbContext = getConnection.InitializeConnection();
                var userQuery = new UpdateAssetBarcode(assetBarcode);
                return  dbContext.Execute(userQuery);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        

        /// <summary>
        /// Update the barcode of an existing Asset
        /// </summary>
        /// <param name="AssetId"></param>
        /// <returns>returns the result of the Operation</returns>
        /// 
        internal int CheckAssetBarcodeNumber(string AssetNumber)
        {
            try
            {
                GetConnection getConnection = new GetConnection();
                dbContext = getConnection.InitializeConnection();
                var userQuery = new CheckAssetNumber(AssetNumber);
                return  dbContext.Execute(userQuery);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        
        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region Private Helpers

        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));

            }
        }

        #endregion

        #region Properties

        public long RecordCount
        {
            get { return _recordCount; }
            set
            {
                _recordCount = value;
                NotifyPropertyChanged("RecordCount");
            }
        }

       // public string AssetNumber
       // {
       //     get { return _assetNumber; }
        //    set
        //    {
        //        _assetNumber = value;
        //        NotifyPropertyChanged("AssetNumber");
       //     }
       // }

        #region IViewModel Properties

        public bool EmptyMessageVisibility
        {
            get { return _emptyMessageVisibility; }
            set
            {
                _emptyMessageVisibility = value;
                NotifyPropertyChanged("EmptyMessageVisibility");
            }
        }

        public int LoggedInUserRoleId
        {
            get
            {
                return (Thread.CurrentPrincipal as CustomPrincipal).Identity.CurrentUser.RoleId;
            }
        }

        public string LoggedInUserName
        {
            get { throw new NotImplementedException(); }
        }

        #endregion

        #endregion

    }
}



