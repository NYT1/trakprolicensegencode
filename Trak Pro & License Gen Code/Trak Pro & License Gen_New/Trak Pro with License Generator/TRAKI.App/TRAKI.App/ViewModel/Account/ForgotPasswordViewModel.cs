﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using TRAKI.App.Common;
using TRAKI.App.DapperQueries.Common;
using TRAKI.App.DapperQueries.Common.Account;
using TRAKI.App.Interfaces;
using TRAKI.App.Utilities;


namespace TRAKI.App.ViewModel.Account
{
    class ForgotPasswordViewModel : INotifyPropertyChanged, IDataErrorInfo
    {


        protected IDbContext dbContext;

        /// <summary>
        /// Method to get send the email for forgot password request
        /// </summary>
        /// <returns>result whether emails end successfully or not</returns>
        /// 
        public int ForgotPassword()
        {
            int result = 0;
            try
            {
                GetConnection getConnection = new GetConnection();
                dbContext = getConnection.InitializeConnection();
                if (dbContext.Execute(new CheckEmail(this.Email)) <= 0)
                    this.Status = "Then Email Address Entered is not registered with us!";
                else
                {
                    this.GetEmailSettings();
                    string newPassword = dbContext.Execute(new ForgotPassword(this.Email));
                    if (!string.IsNullOrEmpty(newPassword))
                    {
                        result = 1;// Success password change
                        try
                        {
                            EmailService email = new EmailService();
                            string signature = (Constants.AppSettings.Where(setting => setting.SettingName == "EmailSignature").SingleOrDefault().SettingValue).ToString();
                            string body = (Constants.AppSettings.Where(setting => setting.SettingName == "ForgotPasswordEmailBody").SingleOrDefault().SettingValue).ToString() + " <b>" + newPassword + "</b>" + signature;
                            string subject = (Constants.AppSettings.Where(setting => setting.SettingName == "ForgotPasswordEmailSubject").SingleOrDefault().SettingValue).ToString();

                            Thread emailThread = new Thread(delegate()
                            {
                                email.SendEmail(this.Email, subject, body);
                            });

                            emailThread.IsBackground = true;
                            emailThread.Start();
                        }
                        catch (Exception ex) {
                            throw ex;
                        }
                    }
                }
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }


        /// <summary>
        /// Method to get send the email for forgot password request
        /// </summary>
        /// <returns>result whether emails end successfully or not</returns>
        /// 
        private void GetEmailSettings()
        {

            try
            {
                GetConnection getConnection = new GetConnection();
                dbContext = getConnection.InitializeConnection();
                Constants.AppSettings = dbContext.Execute(new GetSettings("Email"));
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }




        #region Properties
        private string _email;
        public string Email
        {
            get { return _email; }
            set
            {
                _email = value;
                NotifyPropertyChanged("Email");
            }
        }

        private string _status;
        public string Status
        {
            get { return _status; }
            set { _status = value; NotifyPropertyChanged("Status"); }
        }

        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region Private Helpers

        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region IDataErrorInfo Members

        string IDataErrorInfo.Error
        {
            get { return null; }
        }


        string IDataErrorInfo.this[string PropertyName]
        {
            get
            {
                this.IsValid = true;
                return GetValidationErrors(PropertyName);
            }
        }


        #endregion

        #region validation

        static readonly string[] ValidatedProperties =
        {
            "Email"
        };

        private bool _isValid;

        public bool IsValid
        {
            get { return _isValid; }
            set
            {
                foreach (string property in ValidatedProperties)
                    if (GetValidationErrors(property) != null)
                    {
                        _isValid = false;
                        NotifyPropertyChanged("IsValid");
                        return;
                    }
                _isValid = true;
                NotifyPropertyChanged("IsValid");
            }
        }

        string GetValidationErrors(string PropertyName)
        {
            string Error = null;
            switch (PropertyName)
            {
                case "Email":
                    Error = ValidateEmail();
                    break;
            }
            return Error;
        }


        private string ValidateEmail()
        {

            if (!string.IsNullOrEmpty(Email))
            {
                return (!Regex.IsMatch(Email, @"^[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$")) ? "Invalid email address!" : null;
            }
            return string.IsNullOrEmpty(Email) ? "Email is Required!" : null;
        }

        #endregion
    }
}

