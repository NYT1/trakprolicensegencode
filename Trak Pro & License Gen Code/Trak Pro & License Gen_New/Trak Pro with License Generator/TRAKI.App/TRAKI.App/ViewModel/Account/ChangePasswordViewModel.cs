﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TRAKI.App.Account;
using TRAKI.App.Common;
using TRAKI.App.DapperQueries.Account;
using TRAKI.App.Interfaces;
using TRAKI.App.Model;
using TRAKI.App.Model.Account;
using TRAKI.App.Utilities;

namespace TRAKI.App.ViewModel.Account
{
    class ChangePasswordViewModel : INotifyPropertyChanged
    {

        //private ObservableCollection<Password> password;
        protected IDbContext dbContext;


        /// <summary>
        /// Method to Return the current user's object for password change
        /// </summary>
        /// <returns>Current users object</returns>
        /// 
        internal ObservableCollection<Password> BindPassword()
        {
            try
            {
                // User user = (Thread.CurrentPrincipal as CustomPrincipal).Identity.CurrentUser;
                Password _password = new Password();
                _password.UserId = (Thread.CurrentPrincipal as CustomPrincipal).Identity.CurrentUser.UserId;
                _password.IsTempPassword = (Thread.CurrentPrincipal as CustomPrincipal).Identity.CurrentUser.IsTempPassword;
                if ((Thread.CurrentPrincipal as CustomPrincipal).Identity.CurrentUser.IsTempPassword == BooleanValue.True)
                {
                    _password.OldPassword = (Thread.CurrentPrincipal as CustomPrincipal).Identity.CurrentUser.Password;
                }
                var password = new ObservableCollection<Password>();
                password.Add(_password);
                return password;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// Method to change user's password
        /// </summary>
        /// <param name="User object"></param>
        /// <returns>result of the operation</returns>
        /// 
        internal int ChangePassword(Password password)
        {
            try
            {
                GetConnection getConnection = new GetConnection();
                dbContext = getConnection.InitializeConnection();
                var userQuery = new ChangeUserPassword(password);
                int result = dbContext.Execute(userQuery);
                if (result > 0)
                {
                    try
                    {
                        if (Constants.AppSettings != null)
                        {

                            EmailService email = new EmailService();
                            string signature = (Constants.AppSettings.Where(setting => setting.SettingName == "EmailSignature").SingleOrDefault().SettingValue).ToString();
                            string body = (Constants.AppSettings.Where(setting => setting.SettingName == "ChangePasswordEmailBody").SingleOrDefault().SettingValue).ToString() + signature;
                            string subject = (Constants.AppSettings.Where(setting => setting.SettingName == "ForgotPasswordEmailSubject").SingleOrDefault().SettingValue).ToString();

                            Thread emailThread = new Thread(delegate()
                            {
                                email.SendEmail((Thread.CurrentPrincipal as CustomPrincipal).Identity.CurrentUser.Email, subject, body);

                            });

                            emailThread.IsBackground = true;
                            emailThread.Start();
                        }
                    }
                    catch (Exception ex)
                    { throw ex; }
                }

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region Private Helpers

        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));

            }
        }

        #endregion

    }
}


