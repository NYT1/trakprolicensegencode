﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TRAKI.App.Account;
using TRAKI.App.Common;
using TRAKI.App.DapperQueries.CatalogSunflowerQueries;
using TRAKI.App.DapperQueries.InventoryQueries.UploadData;
using TRAKI.App.Interfaces;
using TRAKI.App.Model;

namespace TRAKI.App.ViewModel
{
    class CatalogSunflowerViewModel : INotifyPropertyChanged, IViewModel
    {
        private ObservableCollection<CatalogSunflower> CatalogSunflowerList;
        protected IDbContext dbContext;
        private bool _emptyMessageVisibility = false;
        private long _recordCount = 0;

        public CatalogSunflowerViewModel()
        {
            //this.EmptyMessageVisibility = true;
        }


        #region Private
        private ObservableCollection<CatalogSunflower> GetCatalogSunList(int LineNo)
        {
            try
            {

                GetConnection getConnection = new GetConnection();
                dbContext = getConnection.InitializeConnection();
                CatalogSunflowerList = new ObservableCollection<CatalogSunflower>(dbContext.Execute(new GetCatalogSunflower(Convert.ToInt32(LineNo))));


                return CatalogSunflowerList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
        #region Internal

        /// <summary>
        /// Method to return the list of Catalogs
        /// </summary>
        /// <param name="CatalogId"></param>
        /// <param name="IsActive"></param>
        /// <returns>List of Catalogs</returns>
        ///  
        internal ObservableCollection<CatalogSunflower> GetCatalogSunflower(int LineNo)
        {
            try
            {
                if (LineNo == -1)
                {
                    CatalogSunflowerList = new ObservableCollection<CatalogSunflower>();
                    CatalogSunflowerList.Add(new CatalogSunflower());
                }
                else
                {
                    CatalogSunflowerList = this.GetCatalogSunList(LineNo);
                    this.RecordCount = CatalogSunflowerList.Count;
                    if (CatalogSunflowerList == null || CatalogSunflowerList.Count <= 0)
                    {
                        this.EmptyMessageVisibility = true;
                    }
                }
                return CatalogSunflowerList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        internal ObservableCollection<CatalogSunflower> SearchCatalogSun(string SearchText)
        {
            try
            {
                GetConnection getConnection = new GetConnection();
                dbContext = getConnection.InitializeConnection();
                var catalogSunQuery = new SearchCatalogSun(SearchText);
                CatalogSunflowerList = new ObservableCollection<CatalogSunflower>(dbContext.Execute(catalogSunQuery));
                this.RecordCount = CatalogSunflowerList.Count;
                if (CatalogSunflowerList == null || CatalogSunflowerList.Count <= 0)
                {
                    this.EmptyMessageVisibility = true;
                }
                return CatalogSunflowerList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// Method to Insert a new Department or to update the existing one.
        /// </summary>
        /// <param name="Department Object"></param>
        /// <returns>returns the operation result</returns>
        /// 
        internal int InsertUpdateCatlog(CatalogSunflower CatalogSunflower)
        {
            try
            {
                GetConnection getConnection = new GetConnection();
                dbContext = getConnection.InitializeConnection();
                int result = 0;
                dynamic CatalogSunQuery = null;

                if (CatalogSunflower.LineNo == 0)
                {
                    CatalogSunQuery = new InsertCatalogSun(CatalogSunflower);
                    result = dbContext.Execute(CatalogSunQuery);
                    if (result > 0)
                    {
                        result = 1;  //success insert operation
                    }
                }
                else
                {
                    CatalogSunQuery = new UpdateCatalogSun(CatalogSunflower);

                    result = dbContext.Execute(CatalogSunQuery);
                    if (result > 0)
                    {
                        result = 2;  // success update operation
                    }
                }
                return result;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion


        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region Private Helpers

        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));

            }
        }

        #endregion

        #region Properties


        public long RecordCount
        {
            get { return _recordCount; }
            set
            {
                _recordCount = value;
                NotifyPropertyChanged("RecordCount");
            }
        }


        #region IViewModel Properties

        public bool EmptyMessageVisibility
        {
            get { return _emptyMessageVisibility; }
            set
            {
                _emptyMessageVisibility = value;
                NotifyPropertyChanged("EmptyMessageVisibility");
            }
        }






        public int LoggedInUserRoleId
        {
            get
            {
                return (Thread.CurrentPrincipal as CustomPrincipal).Identity.CurrentUser.RoleId;
            }
        }

        public string LoggedInUserName
        {
            get { throw new NotImplementedException(); }
        }

        #endregion
        #endregion



    }

    //internal ObservableCollection<CatalogSunflower> ImportFileData(string FilePath)
    //{
    //    // int counter = 0;
    //    string line = string.Empty;
    //    ObservableCollection<CatalogSunflower> CatalogSunflowerList = new ObservableCollection<CatalogSunflower>();

    //    try
    //    {   // Open the text file using a stream reader.
    //        using (StreamReader sr = new StreamReader(FilePath))
    //        {
    //            // Read the stream to a string, and write the string to the console.
    //            while ((!string.IsNullOrEmpty(line = sr.ReadLine())))
    //            {
    //                CatalogSunflower CatalogSunflower = new CatalogSunflower();
    //                // line = sr.ReadLine();
    //                string[] items = line.Split(new char[] { '\t' });
    //                CatalogSunflower.Catkey = items[0];
    //                CatalogSunflower.orgmfgcode = items[1];
    //                CatalogSunflower.MfgName = items[2];
    //                CatalogSunflower.Model = items[3];
    //                CatalogSunflower.ModelName = items[4];
    //                CatalogSunflower.OffName = items[5];
    //                CatalogSunflower.CDate = Convert.ToDateTime(items[6]);
    //                CatalogSunflower.MDate = Convert.ToDateTime(items[7]);
    //                CatalogSunflower.SDate = Convert.ToDateTime(items[8]);
    //                CatalogSunflower.EDate = Convert.ToDateTime(items[9]);
    //                CatalogSunflower.Flag = items[10];
    //                CatalogSunflower.MfgCode = items[11];



    //                //inventory.UserId = line.Substring(0, 4).Trim();
    //                //inventory.BuildingNumber = line.Substring(4, 5).Trim();
    //                //inventory.RoomNumber = line.Substring(9, 7).Trim();
    //                //inventory.AssetNumber = line.Substring(16, 15).Trim();
    //                //inventory.Description = line.Substring(31, 3).Trim();
    //                //string date = line.Substring(34, 10).Trim();
    //                //date = date + " " + line.Substring(44, 8).Trim();
    //                //inventory.InventoryDate = Convert.ToDateTime(date);

    //               // this.PerformQA(inventory);
    //                CatalogSunflowerList.Add(CatalogSunflower);
    //            }
    //        }
    //        this.RecordCount = CatalogSunflowerList.Count;
    //        if (CatalogSunflowerList.Count > 0)
    //        {
    //            this.EmptyMessageVisibility = false;
    //        }
    //    }
    //    catch (Exception e)
    //    {
    //        throw e;
    //    }
    //    return CatalogSunflowerList;
    //}

}
