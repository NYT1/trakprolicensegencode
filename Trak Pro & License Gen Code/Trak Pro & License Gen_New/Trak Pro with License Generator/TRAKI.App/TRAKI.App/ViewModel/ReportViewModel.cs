﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using TRAKI.App.Account;
using TRAKI.App.Common;
using TRAKI.App.DapperQueries.ReportQueries;
using TRAKI.App.Interfaces;

namespace TRAKI.App.ViewModel
{
    class ReportViewModel
    {
        protected IDbContext dbContext;

        #region Internal
        /// <summary>
        /// Method to return the Asset Value Report Data 
        /// </summary>
        /// <returns>Report Data</returns>
        internal IEnumerable GetAssetValueSummaryReport()
        {
            try
            {
                GetConnection getConnection = new GetConnection();
                dbContext = getConnection.InitializeConnection();
                return dbContext.Execute(new GetAssetValueSummaryReport());
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// Method to return the Perkins Report Data 
        /// </summary>
        /// <returns>Report Data</returns>
        internal IEnumerable GetPerkinsReport()
        {
            try
            {
                GetConnection getConnection = new GetConnection();
                dbContext = getConnection.InitializeConnection();
                return dbContext.Execute(new GetPerkinsReport());
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// Method to return the Disposition Report Data 
        /// </summary>
        /// <returns>Report Data</returns>
        internal IEnumerable GetDispositionReport(string DispositionCode)
        {
            try
            {
                GetConnection getConnection = new GetConnection();
                dbContext = getConnection.InitializeConnection();
                return dbContext.Execute(new GetDispositionReport(DispositionCode));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// Method to return the Cafeteria Report
        /// </summary>
        /// <returns>Report Data</returns>
        internal IEnumerable GetCafeteriaReport()
        {
            try
            {
                GetConnection getConnection = new GetConnection();
                dbContext = getConnection.InitializeConnection();
                return dbContext.Execute(new GetCafeteriaReport());
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Method to return the Inventory Summary Report Data 
        /// </summary>
        /// <returns>Report Data</returns>
        internal IEnumerable GetInventorySummaryByBuildingReport()
        {
            try
            {
                GetConnection getConnection = new GetConnection();
                dbContext = getConnection.InitializeConnection();
                return dbContext.Execute(new GetInventorySummaryByBuildingReport());
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        /// <summary>
        /// Method to return the Asset List By Building Report Data 
        /// </summary>
        /// <returns>Report Data</returns>
        internal IEnumerable GetAssetListByBuildingReport(string StartingBuildingNumber, string EndingBuildingNumber)
        {
            try
            {
                GetConnection getConnection = new GetConnection();
                dbContext = getConnection.InitializeConnection();
                return dbContext.Execute(new GetAssetListByBuildingReport(StartingBuildingNumber, EndingBuildingNumber));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// Method to return the Asset List By Missing Asset Report Data 
        /// </summary>
        /// <returns>Report Data</returns>
        internal IEnumerable GetMissingAssetReport()
        {
            try
            {
                GetConnection getConnection = new GetConnection();
                dbContext = getConnection.InitializeConnection();
                return dbContext.Execute(new GetMissingAssetReport());
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }







        /// <summary>
        /// Method to move the upload data file to the spcified destination.
        /// </summary>
        /// <param name="Source file path"></param>
        /// <returns>True if filed moved successfully , false otherwise</returns>
        /// 
        internal string GetReportName(string Extension, string ReportName)
        {
            try
            {
              //  string DestDir = Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory, "ExportReportData");
                 //string DestDir = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles), "ExportReportData");
               // if (!Directory.Exists(AppDomain.CurrentDomain.BaseDirectory + "ExportReportData"))
            //    {
             //       Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + "ExportReportData");
           //     }

               //./ System.IO.Directory.CreateDirectory(MyNewPath);
              //  if (!Directory.Exists(DestDir))  // if it doesn't exist, create
               //     Directory.CreateDirectory(DestDir);
               // string filePath = Path.Combine(fileDirPath, "AppInfo.txt");
                //this.GiveAccessToFile(fileDirPath);

               // var DestDir = System.Configuration.ConfigurationManager.AppSettings["ReportPath"];
               // if (!Directory.Exists(DestDir))
                //{
              //      Directory.CreateDirectory(DestDir);
              //  }

                string FileName = System.IO.Path.GetFileNameWithoutExtension(ReportName);
                //FileName = FileName + "_" + (Thread.CurrentPrincipal as CustomPrincipal).Identity.CurrentUser.UserName + "_" + DateTime.Now.ToString("yyyyMMddHHmm") + "." + Extension;
                return (FileName+ "." + Extension);
            }
            catch (Exception e)
            {
                throw e;
            }
        }


        /// <summary>
        /// Method to return the list of Catalogs
        /// </summary>
        /// <param name="CatalogId"></param>
        /// <param name="IsActive"></param>
        /// <returns>List of Catalogs</returns>
        /// 
        internal IEnumerable GetCatalogListReport()
        {
            try
            {
                GetConnection getConnection = new GetConnection();
                dbContext = getConnection.InitializeConnection();
                return dbContext.Execute(new GetCatalogListReport());
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Method to Get Discrepancy Report Data
        /// </summary>
        /// <returns>Report Data</returns>
        internal IEnumerable GetDiscrepancyReport(IEnumerable<string> BuildingNumbers, string DiscrepancyType)
        {
            try
            {
                GetConnection getConnection = new GetConnection();
                dbContext = getConnection.InitializeConnection();
                return dbContext.Execute(new GetDiscrepancyReport(BuildingNumbers, DiscrepancyType));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        internal ContextMenu PrepareContextMenu(GrapeCity.ActiveReports.Document.PageDocument pageDocument)
        {
            double opacity = 1;
            bool isEnabled = true;
            if (pageDocument == null)
            {
                opacity = 0.5;
                isEnabled = false;
            }
            var contextMenu = new ContextMenu();
            MenuItem menuItemPDF = new MenuItem { Name = ExportMenuItemName.ExportToPDF.ToString(), Header = "Export To PDF" };
            // menuItemPDF.Click += ExportToPDFItem_Click;
            menuItemPDF.Icon = new Image
            {
                Source = new BitmapImage(new Uri("pack://application:,,,/Images/pdf.png")),
                VerticalAlignment = VerticalAlignment.Center,
                Width = 16,
                Height = 16,
                Margin = new Thickness(2, 3, 2, 2),
                Opacity = opacity
            };
            menuItemPDF.IsEnabled = isEnabled;
            menuItemPDF.Opacity = opacity;
            contextMenu.Items.Add(menuItemPDF);
            contextMenu.Items.Add(new Separator());
            MenuItem menuItemExcel = new MenuItem { Name = ExportMenuItemName.ExportToExcel.ToString(), Header = "Export To Excel" };
            // menuItemExcel.Click += ExportToExcelItem_Click;
            menuItemExcel.Icon = new Image
            {
                Source = new BitmapImage(new Uri("pack://application:,,,/Images/excel.png")),
                VerticalAlignment = VerticalAlignment.Center,
                Width = 16,
                Height = 16,
                Margin = new Thickness(2, 3, 2, 2),
                Opacity = opacity
            };
            menuItemExcel.IsEnabled = isEnabled;
            menuItemExcel.Opacity = opacity;
            contextMenu.Items.Add(menuItemExcel);
            contextMenu.Items.Add(new Separator());
            MenuItem menuItemTextFile = new MenuItem { Name = ExportMenuItemName.ExportToFile.ToString(), Header = "Export To Text File" };
            // menuItemTextFile.Click += ExportToTextFileItem_Click;
            menuItemTextFile.Icon = new Image
            {
                Source = new BitmapImage(new Uri("pack://application:,,,/Images/file.png")),
                VerticalAlignment = VerticalAlignment.Center,
                Width = 16,
                Height = 16,
                Margin = new Thickness(2, 3, 2, 2),
                Opacity = opacity
            };
            menuItemTextFile.IsEnabled = isEnabled;
            menuItemTextFile.Opacity = opacity;
            contextMenu.Items.Add(menuItemTextFile);
            return contextMenu;
        }





        #endregion
    }
}
