﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Collections.ObjectModel;
using TRAKI.App.Model;
using TRAKI.App.Interfaces;
using TRAKI.App.Common;
using TRAKI.App.DapperQueries.Common;
using TRAKI.App.DapperQueries.BuildingQueries;

namespace TRAKI.App.ViewModel
{
    public class ClosedBuildingViewModel : INotifyPropertyChanged
    {

        public ObservableCollection<Building> BuildingList { get; private set; }

        protected IDbContext dbContext;//cu

        public ClosedBuildingViewModel()
        {
        }

        public ObservableCollection<Building> GetBuildingbyBuildingStatus(bool BuildingStatus)
        {
            try
            {
                if (BuildingStatus == true)
                {
                    BuildingList = new ObservableCollection<Building>();
                    BuildingList.Add(new Building());
                }
                else
                {
                    GetConnection getConnection = new GetConnection();
                    dbContext = getConnection.InitializeConnection();
                    var buildingQuery = new GetBuildingbyBuildingStatus(BuildingStatus);
                    BuildingList = new ObservableCollection<Building>(dbContext.Execute(buildingQuery));
                }
                return BuildingList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region Private Helpers

        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion
    }
}



