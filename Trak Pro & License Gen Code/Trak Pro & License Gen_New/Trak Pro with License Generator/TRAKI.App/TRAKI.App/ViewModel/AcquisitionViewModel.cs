﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TRAKI.App.Account;
using TRAKI.App.Common;
using TRAKI.App.DapperQueries.AcquisitionQueries;
using TRAKI.App.DapperQueries.Common;
using TRAKI.App.Interfaces;
using TRAKI.App.Model;

namespace TRAKI.App.ViewModel
{
    class AcquisitionViewModel : INotifyPropertyChanged, IViewModel
    {
        private ObservableCollection<Acquisition> AcquisitionList;
        protected IDbContext dbContext;
        private bool _emptyMessageVisibility = false;
        private long _recordCount = 0;

        public AcquisitionViewModel()
        {
        }

        #region Private

        /// <summary>
        /// Private method to return the list of Acquisitions
        /// </summary>
        /// <param name="AcquisitionId"></param>
        /// <param name="IsActive"></param>
        /// <returns>List of Acquisitions</returns>
        /// 
        private ObservableCollection<Acquisition> GetAcquisitionList(int? AcquisitionId, int? IsActive)
        {
            try
            {
                GetConnection getConnection = new GetConnection();
                dbContext = getConnection.InitializeConnection();
                AcquisitionList = new ObservableCollection<Acquisition>(dbContext.Execute(new GetAcquisition(AcquisitionId, IsActive)));
                return AcquisitionList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Internal

        /// <summary>
        /// Method to return the list of Acquisitions
        /// </summary>
        /// <param name="AcquisitionId"></param>
        /// <param name="IsActive"></param>
        /// <returns>List of Acquisitions</returns>
        /// 
        internal ObservableCollection<Acquisition> GetAcquisition(int? AcquisitionId, int? IsActive)
        {
            try
            {
                if (AcquisitionId == -1)
                {
                    AcquisitionList = new ObservableCollection<Acquisition>();
                    AcquisitionList.Add(new Acquisition());
                }
                else
                {
                    AcquisitionList = this.GetAcquisitionList(AcquisitionId, IsActive);
                    this.RecordCount = AcquisitionList.Count;
                    if (AcquisitionList == null || AcquisitionList.Count <= 0)
                    {
                        this.EmptyMessageVisibility = true;
                    }
                }
                return AcquisitionList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Method to return the list of Acquisitions that matches the search criteria
        /// </summary>
        /// <param name="SearchText"></param>
        /// <param name="IsActive"></param>
        /// <returns>List of Acquisitions</returns>
        /// 
        internal ObservableCollection<Acquisition> SearchAcquisition(string SearchText, int? IsActive)
        {
            try
            {

                GetConnection getConnection = new GetConnection();
                dbContext = getConnection.InitializeConnection();
                var acquisitionQuery = new SearchAcquisition(SearchText, IsActive);
                AcquisitionList = new ObservableCollection<Acquisition>(dbContext.Execute(acquisitionQuery));
                this.RecordCount = AcquisitionList.Count;
                if (AcquisitionList == null || AcquisitionList.Count <= 0)
                {
                    this.EmptyMessageVisibility = true;
                }
                return AcquisitionList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Method to delete the  Acquisition
        /// </summary>
        /// <param name="AcquisitionId"></param>
        /// <returns>returns the operation result</returns>
        /// 
        internal int DeleteExistingAcquisition(int AcquisitionId)
        {
            try
            {
                GetConnection getConnection = new GetConnection();
                dbContext = getConnection.InitializeConnection();
                var acquisitionQuery = new DeleteAcquisition(AcquisitionId);
                return dbContext.Execute(acquisitionQuery);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// Method to Insert a new Acquisition or to update the existing one.
        /// </summary>
        /// <param name="Acquisition Object"></param>
        /// <returns>returns the operation result</returns>
        /// 
        internal int InsertUpdateAcquisition(Acquisition acquisition)
        {
            try
            {
                GetConnection getConnection = new GetConnection();
                dbContext = getConnection.InitializeConnection();
                int result = 0;
                dynamic acquisitionQuery = null;
                if (acquisition.AcquisitionId == 0)
                {
                    acquisitionQuery = new InsertAcquisition(acquisition);
                    result = dbContext.Execute(acquisitionQuery);
                    if (result > 0)
                    {
                        result = 1;  //success insert operation
                    }
                }
                else
                {
                    acquisitionQuery = new UpdateAcquisition(acquisition);
                    result = dbContext.Execute(acquisitionQuery);
                    if (result > 0)
                    {
                        result = 2;  // success update operation
                    }
                }
                return result;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region Private Helpers

        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));

            }
        }

        #endregion

        #region Properties


        public long RecordCount
        {
            get { return _recordCount; }
            set
            {
                _recordCount = value;
                NotifyPropertyChanged("RecordCount");
            }
        }

        #region IViewModel Properties

        public bool EmptyMessageVisibility
        {
            get { return _emptyMessageVisibility; }
            set
            {
                _emptyMessageVisibility = value;
                NotifyPropertyChanged("EmptyMessageVisibility");
            }
        }

        public int LoggedInUserRoleId
        {
            get
            {
                return (Thread.CurrentPrincipal as CustomPrincipal).Identity.CurrentUser.RoleId;
            }
        }

        public string LoggedInUserName
        {
            get { throw new NotImplementedException(); }
        }

        #endregion


        #endregion

    }
}

