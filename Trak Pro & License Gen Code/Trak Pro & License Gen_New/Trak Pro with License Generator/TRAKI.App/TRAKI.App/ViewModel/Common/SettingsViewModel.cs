﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.AccessControl;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using TRAKI.App.Common;
using TRAKI.App.DapperQueries.Common;
using TRAKI.App.Interfaces;
using TRAKI.App.Model;
using TRAKI.App.Utilities;

namespace TRAKI.App.ViewModel.Common
{
    class SettingsViewModel
    {
        protected IDbContext dbContext;

        private bool isValidConnectionString;

        public bool IsValidConnectionString
        {
            get { return isValidConnectionString; }
            set { isValidConnectionString = value; }
        }


        /// <summary>
        /// Method to Give access permission to the File
        /// </summary>
        /// 
        internal void GiveAccessToFile(string FilePath)
        {
            DirectorySecurity sec = Directory.GetAccessControl(FilePath);
            SecurityIdentifier everyone = new SecurityIdentifier(WellKnownSidType.WorldSid, null);
            sec.AddAccessRule(new FileSystemAccessRule(everyone, FileSystemRights.Modify | FileSystemRights.Synchronize, InheritanceFlags.ContainerInherit | InheritanceFlags.ObjectInherit, PropagationFlags.None, AccessControlType.Allow));
            Directory.SetAccessControl(FilePath, sec);
        }


        /// <summary>
        /// Method to Application settings
        /// </summary>
        /// <returns>List of application settings</returns>
        /// 
        public List<Settings> GetSettings()
        {
            try
            {
                GetConnection getConnection = new GetConnection();
                dbContext = getConnection.InitializeConnection();
                return dbContext.Execute(new GetSettings(null));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        /// <summary>
        /// Method to get Pages list that user is authorized to 
        /// </summary>
        /// <param name="RoleId"></param>
        /// <returns>List of pages which are accessible to the user</returns>
        /// 
        public IEnumerable GetAccessiblePages(int RoleId)
        {
            try
            {
                GetConnection getConnection = new GetConnection();
                dbContext = getConnection.InitializeConnection();
                return dbContext.Execute(new GetAccessiblePages(RoleId));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// Method to Application settings
        /// </summary>
        /// <returns>List of application settings</returns>
        /// 
        public bool SetDBFilePath()
        {
            try
            {
                GetConnection getConnection = new GetConnection();
                dbContext = getConnection.InitializeConnection();
                return true; // dbContext.Execute(new GetSettings(null));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// Method to Test the Connection of New DB File Path
        /// </summary>
        /// <returns>True or Flase</returns>
        /// 
        public bool TestDBConnection(string DBPath, string Password, ref string ConnectionString)
        {
            try
            {
                GetConnection getConnection = new GetConnection();
                return this.isValidConnectionString = getConnection.TestConnection(DBPath, Password, ref ConnectionString);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        /// <summary>
        /// OverLoad Method to Test the Connection
        /// </summary>
        /// <returns>True or Flase</returns>
        /// 
        public bool TestDBConnection(string ConnectionString)
        {
            try
            {
                GetConnection getConnection = new GetConnection();
                return getConnection.TestConnection(ConnectionString);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Method to Update The Connection string file With the new Connection string
        /// </summary>
        /// <returns>True or Flase</returns>
        /// 
        internal bool UpdateDBConnectionFile(string ConnectionString)
        {
            bool status = false;
        //    string filePath = new FileInfo(System.AppDomain.CurrentDomain.BaseDirectory + @"Common/AppInfo.txt").ToString();// ; Path.Combine(fileDirPath, "AppInfo.txt");
            string fileDirPath = Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory, "AppData");
            if (!Directory.Exists(fileDirPath))  // if it doesn't exist, create
                Directory.CreateDirectory(fileDirPath);
            this.GiveAccessToFile(fileDirPath);

            string filePath = Path.Combine(fileDirPath, "AppInfo.txt");
            EncryptAndDecrypt encryptAndDecrypt = new EncryptAndDecrypt();
            using (StreamWriter streamWriter = new StreamWriter(filePath))
            {
                streamWriter.WriteLine(encryptAndDecrypt.EncryptTripleDES(ConnectionString));
                status = true;
            }
            return status;
        }

        /// <summary>
        /// Method to Read Connection string from The Connection string file
        /// </summary>
        /// <returns>True or Flase</returns>
        /// 
        internal string GetConnectionString(string FilePath)
        {
            try
            {
                EncryptAndDecrypt encryptAndDecrypt = new EncryptAndDecrypt();
                using (StreamReader streamReader = new StreamReader(FilePath))
                {
                    return encryptAndDecrypt.DecryptTripleDES(streamReader.ReadLine());
                }
            }
            catch (Exception ex)
            {
                if (ex.HResult == -2147024893)
                    return string.Empty;
                else
                    throw ex;
            }
        }
         
    }
}
