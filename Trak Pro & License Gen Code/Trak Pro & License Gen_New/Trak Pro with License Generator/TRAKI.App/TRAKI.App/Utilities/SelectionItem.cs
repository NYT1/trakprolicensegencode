﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TRAKI.App.Utilities
{
    /// <summary>
    /// an item that will be selected in the multi check box
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class SelectionItem<T> : INotifyPropertyChanged
    {
        #region privat fields
        /// <summary>
        /// indicates if the item is selected
        /// </summary>
        private bool _isSelected;
        #endregion

        public SelectionItem(T element, bool isSelected)
        {
            Element = element;
            IsSelected = IsSelected;
        }

        public SelectionItem(T element):this(element,false)
        {

        }

        #region public properties
        /// <summary>
        /// this UI-aware indicates if the element is selected or not
        /// </summary>
        public bool IsSelected
        {
            get
            {
                return _isSelected;
            }
            set
            {
                _isSelected = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("IsSelected"));
            }
        }

        /// <summary>
        /// the element itself
        /// </summary>
        public T Element { get; set; }
        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion
    }
}
