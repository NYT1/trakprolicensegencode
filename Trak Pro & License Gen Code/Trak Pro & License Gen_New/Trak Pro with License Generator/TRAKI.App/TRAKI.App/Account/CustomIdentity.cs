﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using TRAKI.App.Model;

namespace TRAKI.App.Account
{
    class CustomIdentity : IIdentity
    {
        public CustomIdentity(User CurrentUser)
        {
            this.CurrentUser = CurrentUser;
            this.Name = CurrentUser.UserName;
            this.CurrentUser = CurrentUser;
        }

        public User CurrentUser { get; private set; }
        //public string Email { get; private set; }
      //  public string[] Roles { get; private set; }

        #region IIdentity Members
        public string AuthenticationType { get { return "Custom authentication"; } }

        public bool IsAuthenticated { get { return !string.IsNullOrEmpty(this.Name); } }

        public string Name { get; private set; }
    
        #endregion

    }
}