﻿namespace TRAKI.App
{
    partial class LicenseDialog
    {
        /// test2018 
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TextDBFilePath = new System.Windows.Forms.TextBox();
            this.btnSaveDBPath = new System.Windows.Forms.Button();
            this.btnBrowse = new System.Windows.Forms.Button();
            this.lbldbPath = new System.Windows.Forms.Label();
            this.labelTitle = new System.Windows.Forms.Label();
            this.labelHeader = new System.Windows.Forms.Label();
            this.btnCancel = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // TextDBFilePath
            // 
            this.TextDBFilePath.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.TextDBFilePath.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TextDBFilePath.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextDBFilePath.Location = new System.Drawing.Point(146, 111);
            this.TextDBFilePath.Name = "TextDBFilePath";
            this.TextDBFilePath.ReadOnly = true;
            this.TextDBFilePath.Size = new System.Drawing.Size(381, 24);
            this.TextDBFilePath.TabIndex = 0;
            // 
            // btnSaveDBPath
            // 
            this.btnSaveDBPath.BackColor = System.Drawing.Color.LightSeaGreen;
            this.btnSaveDBPath.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSaveDBPath.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.btnSaveDBPath.Location = new System.Drawing.Point(524, 167);
            this.btnSaveDBPath.Name = "btnSaveDBPath";
            this.btnSaveDBPath.Size = new System.Drawing.Size(82, 29);
            this.btnSaveDBPath.TabIndex = 4;
            this.btnSaveDBPath.Text = "Register";
            this.btnSaveDBPath.UseVisualStyleBackColor = false;
            this.btnSaveDBPath.Click += new System.EventHandler(this.btnSaveDBPath_Click);
            // 
            // btnBrowse
            // 
            this.btnBrowse.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnBrowse.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBrowse.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.btnBrowse.Location = new System.Drawing.Point(526, 111);
            this.btnBrowse.Margin = new System.Windows.Forms.Padding(0);
            this.btnBrowse.Name = "btnBrowse";
            this.btnBrowse.Size = new System.Drawing.Size(80, 24);
            this.btnBrowse.TabIndex = 1;
            this.btnBrowse.Text = "Browse..";
            this.btnBrowse.UseVisualStyleBackColor = true;
            this.btnBrowse.Click += new System.EventHandler(this.btnBrowse_Click);
            // 
            // lbldbPath
            // 
            this.lbldbPath.AutoSize = true;
            this.lbldbPath.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.lbldbPath.Location = new System.Drawing.Point(17, 115);
            this.lbldbPath.Name = "lbldbPath";
            this.lbldbPath.Size = new System.Drawing.Size(87, 17);
            this.lbldbPath.TabIndex = 8;
            this.lbldbPath.Text = "License File ";
            // 
            // labelTitle
            // 
            this.labelTitle.AutoSize = true;
            this.labelTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTitle.ForeColor = System.Drawing.Color.DodgerBlue;
            this.labelTitle.Location = new System.Drawing.Point(3, 10);
            this.labelTitle.Name = "labelTitle";
            this.labelTitle.Size = new System.Drawing.Size(205, 24);
            this.labelTitle.TabIndex = 9;
            this.labelTitle.Text = "Trak Pro Registration";
            // 
            // labelHeader
            // 
            this.labelHeader.AutoSize = true;
            this.labelHeader.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelHeader.Location = new System.Drawing.Point(16, 75);
            this.labelHeader.Name = "labelHeader";
            this.labelHeader.Size = new System.Drawing.Size(431, 20);
            this.labelHeader.TabIndex = 10;
            this.labelHeader.Text = "Provide the license file that you have got with this installation";
            // 
            // btnCancel
            // 
            this.btnCancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.btnCancel.Location = new System.Drawing.Point(406, 167);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(100, 29);
            this.btnCancel.TabIndex = 5;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = false;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // LicenseDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(633, 235);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.labelHeader);
            this.Controls.Add(this.labelTitle);
            this.Controls.Add(this.lbldbPath);
            this.Controls.Add(this.btnBrowse);
            this.Controls.Add(this.btnSaveDBPath);
            this.Controls.Add(this.TextDBFilePath);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "LicenseDialog";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Trak Pro Registration";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox TextDBFilePath;
        private System.Windows.Forms.Button btnSaveDBPath;
        private System.Windows.Forms.Button btnBrowse;
        private System.Windows.Forms.Label lbldbPath;
        private System.Windows.Forms.Label labelTitle;
        private System.Windows.Forms.Label labelHeader;
        private System.Windows.Forms.Button btnCancel;
    }
}