﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TRAKI.App.Model
{
    class AssetBarcode : INotifyPropertyChanged, IDataErrorInfo
    {

        private string _assetNumber;
        private DateTime? _retagDate;
        private string _previousAssetNumber;
        private int _assetId;

        #region Properties

        public string AssetNumber
        {
            get { return _assetNumber; }
            set
            {
                _assetNumber = value.Trim();
                NotifyPropertyChanged("AssetNumber");
            }
        }

        public DateTime? RetagDate
        {
            get { return _retagDate; }
            set
            {
                _retagDate = value;
                NotifyPropertyChanged("RetagDate");
            }
        }

        public string PreviousAssetNumber
        {
            get { return _previousAssetNumber; }
            set
            {
                _previousAssetNumber = value.Trim();
                NotifyPropertyChanged("PreviousAssetNumber");
            }
        }

        public int AssetId
        {
            get { return _assetId; }
            set
            {
                _assetId = value;
                NotifyPropertyChanged("AssetId");
            }
        }
      
        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region Private Helpers

        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region IDataErrorInfo Members

        string IDataErrorInfo.Error
        {
            get { return null; }
        }


        string IDataErrorInfo.this[string PropertyName]
        {
            get
            {
                this.IsValid = true;
                return GetValidationErrors(PropertyName);
            }
        }


        #endregion

        #region validation

        static readonly string[] ValidatedProperties =
        {
            "AssetNumber"
        };

        private bool _isValid;

        public bool IsValid
        {
            get { return _isValid; }
            set
            {
                foreach (string property in ValidatedProperties)
                    if (GetValidationErrors(property) != null)
                    {
                        _isValid = false;
                        NotifyPropertyChanged("IsValid");
                        return;
                    }
                _isValid = true;
                NotifyPropertyChanged("IsValid");
            }
        }

        string GetValidationErrors(string PropertyName)
        {
            string Error = null;
            switch (PropertyName)
            {
                case "AssetNumber":
                    Error = ValidateAssetNumber();
                    break;
            }
            return Error;
        }


        private string ValidateAssetNumber()
        {
            if (string.IsNullOrWhiteSpace(AssetNumber))
            {
                return "Barcode is required!";
            }
            else if (AssetNumber.Equals(PreviousAssetNumber))
            {
                return "New & Previous Barcode cannot be same!";
            }
            else
            {
                return null;
            }
        }

        #endregion

    }
}
