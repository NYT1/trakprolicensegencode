﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using TRAKI.App.Common;
namespace TRAKI.App.Model
{
    public class User : INotifyPropertyChanged, IDataErrorInfo
    {
        private int _userId;
        private string _firstName;
        private string _lastName;
        private string _email;
        private string _userName;
        private string _password;
        private string _role;
        private BooleanValue _isActive;
        private Gender _gender;
        private int _roleId;
        private string _confrimPassowrd;
        private BooleanValue _isTempPassword;


        public User()
        {
            IsActive = BooleanValue.True;
        }


        #region Properties


        public int UserId
        {
            get { return _userId; }
            set
            {
                _userId = value;
                NotifyPropertyChanged("UserId");
            }
        }

        public string FirstName
        {
            get { return _firstName; }
            set
            {
                _firstName = value;
                NotifyPropertyChanged("FirstName");
            }
        }

        public string ConfirmPassword
        {
            get { return _confrimPassowrd; }
            set
            {
                _confrimPassowrd = value;
                NotifyPropertyChanged("ConfirmPassword");
            }
        }

        public string LastName
        {
            get { return _lastName; }
            set
            {
                _lastName = value;
                NotifyPropertyChanged("LastName");
            }
        }

        public string Email
        {
            get { return _email; }
            set
            {
                _email = value;
                NotifyPropertyChanged("Email");
            }
        }

        public Gender Gender
        {
            get { return _gender; }
            set
            {
                _gender = value;
                NotifyPropertyChanged("Gender");
            }
        }

        public string UserName
        {
            get { return _userName; }
            set
            {
                _userName = value;
                NotifyPropertyChanged("UserName");
            }
        }

        public string Password
        {
            get { return _password; }
            set
            {
                _password = value;
                NotifyPropertyChanged("Password");
            }
        }
         
        public string Role
        {
            get { return _role; }
            set
            {
                _role = value;
                NotifyPropertyChanged("Role");
            }
        }

        public int RoleId
        {
            get { return _roleId; }
            set
            {
                _roleId = value;
                NotifyPropertyChanged("RoleId");
            }
        }
        
        public BooleanValue IsActive
        {
            get { return _isActive; }
            set
            {
                _isActive = value;
                NotifyPropertyChanged("IsActive");
            }
        }

        public BooleanValue IsTempPassword
        {
            get { return _isTempPassword; }
            set
            {
                _isTempPassword = value;
                NotifyPropertyChanged("IsTempPassword");
            }
        }

        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region Private Helpers

        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region IDataErrorInfo Members

        string IDataErrorInfo.Error
        {
            get { return null; }
        }


        string IDataErrorInfo.this[string PropertyName]
        {
            get
            {
                this.IsValid = true;
                return GetValidationErrors(PropertyName);
            }
        }


        #endregion
        
        #region validation

        static readonly string[] ValidatedProperties =
        {
            "FirstName",
            "LastName",
            "Email",
            "UserName",
            "Password",
             "ConfirmPassword"
        };

        private bool _isValid;

        public bool IsValid
        {
            get { return _isValid; }
            set
            {
                foreach (string property in ValidatedProperties)
                    if (GetValidationErrors(property) != null)
                    {
                        _isValid = false;
                        NotifyPropertyChanged("IsValid");
                        return;
                    }
                _isValid = true;
                NotifyPropertyChanged("IsValid");
            }
        }

        string GetValidationErrors(string PropertyName)
        {
            string Error = null;
            switch (PropertyName)
            {
                case "FirstName":
                    Error = ValidateFirstName();
                    break;
                case "LastName":
                    Error = ValidateLastName();
                    break;
                case "Email":
                    Error = ValidateEmail();
                    break;
                case "UserName":
                    Error = ValidateUserName();
                    break;
                case "Password":
                    Error = ValidatePassword();
                    break;
                case "ConfirmPassword":
                    Error = ValidateConfirmPassword();
                    break;
            }
            return Error;
        }

        private string ValidateFirstName()
        {
            if (string.IsNullOrWhiteSpace(FirstName))
            {
                return "First Name is required!";
            }
            else
            {
                return null;
            }
        }
        private string ValidateLastName()
        {
            if (string.IsNullOrWhiteSpace(LastName))
            {
                return "Last Name is required!";
            }
            else
            {
                return null;
            }
        }
        private string ValidateEmail()
        {

            if (!string.IsNullOrEmpty(Email))
            {
                return (!Regex.IsMatch(Email, @"^[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$")) ? " Enter a valid email, eg. user@newyeartech.com!" : null;
            }
            return string.IsNullOrEmpty(Email) ? "Email is Required!" : null;
        }
        private string ValidateUserName()
        {
            if (string.IsNullOrWhiteSpace(UserName))
            {
                return "User Name is required!";
            }
            else
            {
                return null;
            }
        }
        private string ValidatePassword()
        {


            if (string.IsNullOrWhiteSpace(Password))
            {
                return "Password is required!";
            }
            else if (!string.IsNullOrWhiteSpace(ConfirmPassword) && !ConfirmPassword.Equals(Password))
                return "Password & Confirm Password does not match!";

            else
            {
                return null;
            }
        }


        private string ValidateConfirmPassword()
        {
            if (string.IsNullOrWhiteSpace(ConfirmPassword))
            {

                return "Confirm Password is required!";
            }
            else if (!string.IsNullOrWhiteSpace(Password) && !ConfirmPassword.Equals(Password))
                return "Password & Confirm Password does not match!";

            else
            {
                return null;
            }
        }
        #endregion
    }
}
