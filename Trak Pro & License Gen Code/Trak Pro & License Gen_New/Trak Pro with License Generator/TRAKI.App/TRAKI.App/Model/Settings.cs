﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TRAKI.App.Model
{
    public class Settings
    {
        private string _settingItem;
        private string _settingName;
        private string _settingValue;
        private string _displayOrder;

        #region Properties

        public string SettingItem
        {
            get { return _settingItem; }
            set
            {
                _settingItem = value;
            }
        }

        public string SettingName
        {
            get { return _settingName; }
            set
            {
                _settingName = value;
            }
        }

        public string SettingValue
        {
            get { return _settingValue; }
            set
            {
                _settingValue = value;
            }
        }

        public string DisplayOrder
        {
            get { return _displayOrder; }
            set
            {
                _displayOrder = value;
            }
        }

        #endregion


    }
}