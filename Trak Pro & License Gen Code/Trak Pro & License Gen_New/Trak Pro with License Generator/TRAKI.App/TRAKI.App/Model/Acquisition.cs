﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TRAKI.App.Common;

namespace TRAKI.App.Model
{
    public class Acquisition : INotifyPropertyChanged, IDataErrorInfo
    {
        private int _acquisitionId;
        private string _AcquisitionCode;
        private string _acquisitionDescription;
        private BooleanValue _isActive;

        #region Properties

        public Acquisition()
        {
            IsActive = BooleanValue.True;
        }

        public int AcquisitionId
        {
            get { return _acquisitionId; }
            set
            {
                _acquisitionId = value;
                NotifyPropertyChanged("AcquisitionId");
            }
        }

        public string AcquisitionCode
        {
            get { return _AcquisitionCode; }
            set
            {
                _AcquisitionCode = value;
                NotifyPropertyChanged("AcquisitionCode");
            }
        }

        public string AcquisitionDescription
        {
            get { return _acquisitionDescription; }
            set
            {
                _acquisitionDescription = value;
                NotifyPropertyChanged("AcquisitionDescription");
            }
        }

        public BooleanValue IsActive
        {
            get { return _isActive; }
            set
            {
                _isActive = value;
                NotifyPropertyChanged("IsActive");
            }
        }

        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region Private Helpers

        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region IDataErrorInfo Members

        string IDataErrorInfo.Error
        {
            get { return null; }
        }


        string IDataErrorInfo.this[string PropertyName]
        {
            get
            {
                this.IsValid = true;
                return GetValidationErrors(PropertyName);
            }
        }


        #endregion

        #region validation

        static readonly string[] ValidatedProperties =
        {
            "AcquisitionCode",
            "AcquisitionDescription",
        };

        private bool _isValid;

        public bool IsValid
        {
            get { return _isValid; }
            set
            {
                foreach (string property in ValidatedProperties)
                    if (GetValidationErrors(property) != null)
                    {
                        _isValid = false;
                        NotifyPropertyChanged("IsValid");
                        return;
                    }
                _isValid = true;
                NotifyPropertyChanged("IsValid");
            }
        }

        string GetValidationErrors(string PropertyName)
        {
            string Error = null;
            switch (PropertyName)
            {
                case "AcquisitionCode":
                    Error = ValidateAcquisitionId();
                    break;
                case "AcquisitionDescription":
                    Error = ValidateAcquisitionDescription();
                    break;
            }
            return Error;
        }

        private string ValidateAcquisitionId()
        {
            if (string.IsNullOrWhiteSpace(AcquisitionCode))
            {
                return "Acquisition Code is required!";
            }
            else
            {
                return null;
            }
        }
        private string ValidateAcquisitionDescription()
        {
            if (string.IsNullOrWhiteSpace(AcquisitionDescription))
            {
                return "Acquisition Description is required!";
            }
            else
            {
                return null;
            }
        }

        #endregion
    }
}
