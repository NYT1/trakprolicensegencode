﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TRAKI.App.Common;

namespace TRAKI.App.Model
{
    public class Manufacturer : INotifyPropertyChanged, IDataErrorInfo
    {
        private int _manufacturerId;
        private string _manufacturerCode;
        private string _manufacturerName;
        private BooleanValue _isActive;

        public Manufacturer()
        {
            this.IsActive = BooleanValue.True;
        }

        #region Properties

        public int ManufacturerId
        {
            get { return _manufacturerId; }
            set
            {
                _manufacturerId = value;
                NotifyPropertyChanged("ManufacturerId");
            }
        }

        public string ManufacturerCode
        {
            get { return _manufacturerCode; }
            set
            {
                _manufacturerCode = value;
                NotifyPropertyChanged("ManufacturerCode");
            }
        }

        public string ManufacturerName
        {
            get { return _manufacturerName; }
            set
            {
                _manufacturerName = value;
                NotifyPropertyChanged("ManufacturerName");
            }
        }

        public BooleanValue IsActive
        {
            get { return _isActive; }
            set
            {
                _isActive = value;
                NotifyPropertyChanged("IsActive");
            }
        }

        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region Private Helpers

        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region IDataErrorInfo Members

        string IDataErrorInfo.Error
        {
            get { return null; }
        }


        string IDataErrorInfo.this[string PropertyName]
        {
            get
            {
                this.IsValid = true;
                return GetValidationErrors(PropertyName);
            }
        }


        #endregion

        #region validation

        static readonly string[] ValidatedProperties =
        {
            "ManufacturerCode",
            "ManufacturerName",
        };

        private bool _isValid;

        public bool IsValid
        {
            get { return _isValid; }
            set
            {
                foreach (string property in ValidatedProperties)
                    if (GetValidationErrors(property) != null)
                    {
                        _isValid = false;
                        NotifyPropertyChanged("IsValid");
                        return;
                    }
                _isValid = true;
                NotifyPropertyChanged("IsValid");
            }
        }

        string GetValidationErrors(string PropertyName)
        {
            string Error = null;
            switch (PropertyName)
            {
                case "ManufacturerCode":
                    Error = ValidateCatalogId();
                    break;
                case "ManufacturerName":
                    Error = ValidateCatalogDescription();
                    break;
            }
            return Error;
        }

        private string ValidateCatalogId()
        {
            if (string.IsNullOrWhiteSpace(ManufacturerCode))
            {
                return "Manufacturer Code is required!";
            }
            else
            {
                return null;
            }
        }
        private string ValidateCatalogDescription()
        {
            if (string.IsNullOrWhiteSpace(ManufacturerName))
            {
                return "Manufacturer Name is required!";
            }
            else
            {
                return null;
            }
        }

        #endregion
    }
}
