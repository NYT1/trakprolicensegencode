﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace TRAKI.App.Common.Converters
{
    class AddTimeToDatePickerConverter : IValueConverter
    {
       // private DateTime first = new DateTime(day: 1, month: 1, year: 1);

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null)
            {
                DateTime date = (DateTime)value;
                string dateAcquider = String.Format("{0:G}", date.Add(DateTime.Now.TimeOfDay));//.ToString();
                return DateTime.Parse(dateAcquider);
            }
            else
                return value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null)
            {
                DateTime date = (DateTime)value;
                string dateAcquider = String.Format("{0:G}", date.Add(DateTime.Now.TimeOfDay));//.ToString();
                return DateTime.Parse(dateAcquider);
            }
            else
                return value;
        }
    }
}