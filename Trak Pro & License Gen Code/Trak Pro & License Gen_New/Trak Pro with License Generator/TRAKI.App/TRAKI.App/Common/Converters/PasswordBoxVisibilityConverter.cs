﻿using System;
using System.Windows;
using System.Windows.Data;


namespace TRAKI.App.Common.Converters
{
    class PasswordBoxVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            bool flag = true;
            switch (value.ToString().ToLower())
            {
                case "true":
                    {
                        flag = false;
                        break;
                    }
                case "false":
                    {
                        flag = true;
                        break;
                    }
                case "0":
                    {
                        flag = true;
                        break;
                    }
                default:
                    {
                        flag = false;
                        break;
                    }
            }
            return (flag ? Visibility.Visible : Visibility.Collapsed);
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}