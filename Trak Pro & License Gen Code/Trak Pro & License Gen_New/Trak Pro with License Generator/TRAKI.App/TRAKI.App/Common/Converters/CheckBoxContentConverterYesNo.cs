﻿using System;
using System.Windows;
using System.Windows.Data;


namespace TRAKI.App.Common.Converters
{
    class CheckBoxContentConverterYesNo : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            switch (value.ToString().ToLower())
            {
                case "true":
                    return "Yes";
                case "false":
                    return "No";
            }
            return "No";
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}

