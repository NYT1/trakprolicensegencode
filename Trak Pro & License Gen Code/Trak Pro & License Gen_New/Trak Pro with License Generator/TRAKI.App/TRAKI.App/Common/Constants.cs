﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using TRAKI.App.Model;
using TRAKI.App.ViewModel.Common;

namespace TRAKI.App.Common
{
    public class Constants
    {
        //public static int BuildingId;
        public static int NumberOfRecordsPerPage = 10;  // TO DO fetch it from settings table
        public static string ConnectionString = string.Empty;
        public static List<Settings> AppSettings;
        public static int MaximumStringLength = 30;
        public static int MaximumNumber = 9999999;
        public static int AssetNumber = 0;
        public static List<string> AccessibleModules;
        public static Dictionary<string, string> LicenseDetail;
        public static string TrialStatus = string.Empty;
       // public static bool IsAssetView = false;

    }



    public enum Gender
    {
        Male,
        Female
    }

    public enum BooleanValue
    {
        False,
        True
    }

    public enum InventoryStatus
    {
        Shortage,
        Mismatch,
        Overage
    }

    public enum AssetStatus
    {
        Missing,
        New,
        Active
    }

    public enum PagingMode
    {
        First = 1,
        Next = 2,
        Previous = 3,
        Last = 4,
        PageCountChange = 5,
    };

    public class SqlOperatorValue
    {
        public const string GreaterThan = ">";
        public const string LessThan = "<";
        public const string Equal = "=";
        public const string GreaterThanEqualTo = ">=";
        public const string LessThanEqualTo = "<=";
    }

    public class MoveType
    {
        public const string AssetEdited = "E";
        public const string AcceptInventoryData = "I";
    }

    public enum ExportMenuItemName
    {
        ExportToPDF,
        ExportToExcel,
        ExportToFile
    }
     
    public static class IsMatching
    {
        public static bool Check(string Text)
        {
            var regex = new Regex(@"[^a-zA-Z0-9_\\:@.\s]");
            if (regex.IsMatch(Text))
            {
                return true;
            }
            else
                return false;
        }

        public static bool CheckForNumber(string Text)
        {
            var regex = new Regex(@"^\d$");
            if (!regex.IsMatch(Text))
            {
                return true;
            }
            else
                return false;
        }
        public static bool CheckForValidDate(string Text)
        {
            var regex = new Regex(@"(((0|1)[0-9]|2[0-9]|3[0-1])\/(0[1-9]|1[0-2])\/((19|20)\d\d))$");
            if (!regex.IsMatch(Text))
            {
                return true;
            }
            else
                return false;
        }
    }

    public static class InitializeConnectionString
    {
        public static void SetConnectionString()
        {
            try
            {
                SettingsViewModel settingsViewModel = new SettingsViewModel();
                string fileLocation = Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory, @"AppData\\AppInfo.txt");
                Constants.ConnectionString = settingsViewModel.GetConnectionString(fileLocation);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}



