﻿using FirstFloor.ModernUI.Windows.Controls;
using GrapeCity.ActiveReports;
using GrapeCity.ActiveReports.Document;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using TRAKI.App.Common;
using TRAKI.App.Content.CommonContent;
using TRAKI.App.ViewModel;
using TRAKI.App.ViewModel.Common;

namespace TRAKI.App.Reports
{
    /// <summary>
    /// Interaction logic for AssetValueSummaryReport.xaml
    /// </summary>
    public partial class DiscrepancyReport : UserControl
    {
        FileInfo rptPath;
        PageReport pageReport;
        GrapeCity.ActiveReports.Document.PageDocument pageDocument;
        private string discrepancyType;
        private IEnumerable<string> selectedBuildingNumbers; 


        public DiscrepancyReport()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Loads and shows the report.
        /// </summary>
        private void LoadReport()
        {
            try
            {
                rptPath = new FileInfo(System.AppDomain.CurrentDomain.BaseDirectory + @"Reports/DiscrepancyReport.rdlx");
                pageReport = new PageReport(rptPath);
                pageDocument = new PageDocument(pageReport);
                pageDocument.LocateDataSource += new LocateDataSourceEventHandler(OnLocateDataSource);
                this.AssetReportViewer.LoadDocument(pageDocument);
            }
            catch (Exception ex)
            {
                ErrorLogging.LogException("Error: " + ex.Message + " {0}");
                ModernDialog.ShowMessage("Some error occured, please try again!", "Error!", MessageBoxButton.OK);
            }
        }
        private void OnLocateDataSource(object sender, LocateDataSourceEventArgs LocateDataSourceArgs)
        {
            try
            {
                ReportViewModel reportViewModel = new ReportViewModel();
                LocateDataSourceArgs.Data = reportViewModel.GetDiscrepancyReport(this.selectedBuildingNumbers, this.discrepancyType);
            }
            catch (Exception ex)
            {
                ErrorLogging.LogException("Error: " + ex.Message + " {0}");
                ModernDialog.ShowMessage("Some error occured, please try again!", "Error!", MessageBoxButton.OK);
            }
        }

        private void btnSelectBuilding_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                BuildingSelectionDialog buildingSelectionDialog = new BuildingSelectionDialog();
                buildingSelectionDialog.Title = "Select Building";
                buildingSelectionDialog.CloseButton.Content = "Close";
                buildingSelectionDialog.OkButton.Content = "Select";
                buildingSelectionDialog.Buttons = new Button[] { buildingSelectionDialog.CloseButton, buildingSelectionDialog.OkButton };
                buildingSelectionDialog.ShowDialog();

                if (buildingSelectionDialog.DialogResult.Value)
                {
                    this.selectedBuildingNumbers = buildingSelectionDialog.SelectedBuildingNumbers;
                    this.discrepancyType = buildingSelectionDialog.DiscrepancyType;
                    this.LoadReport();
                }
            }
            catch (Exception ex)
            {
                ErrorLogging.LogException("Error: " + ex.Message + " {0}");
                ModernDialog.ShowMessage("Some error occured, please try again!", "Error!", MessageBoxButton.OK);
            }
        }

        #region Export Section

        private void btnExportReport_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ReportViewModel reportViewModel = new ReportViewModel();
                var contextMenu = reportViewModel.PrepareContextMenu(pageDocument);
                foreach (var item in contextMenu.Items)
                {
                    if (!item.GetType().Name.Equals("Separator"))
                    {
                        MenuItem menuItem = item as MenuItem;
                        if (menuItem.IsEnabled)
                        {
                            if (menuItem.Name.Equals(ExportMenuItemName.ExportToPDF.ToString()))
                                menuItem.Click += ExportToPDFItem_Click;
                            if (menuItem.Name.Equals(ExportMenuItemName.ExportToExcel.ToString()))
                                menuItem.Click += ExportToExcelItem_Click;
                            if (menuItem.Name.Equals(ExportMenuItemName.ExportToFile.ToString()))
                                menuItem.Click += ExportToTextFileItem_Click;
                        }
                    }
                }
                contextMenu.IsOpen = true;
            }
            catch (Exception ex)
            {
                ErrorLogging.LogException("Error: " +  ex.Message  + " {0}");
                ModernDialog.ShowMessage(ex.Message, "Error!", MessageBoxButton.OK);
            }
        }

        private void ExportToTextFileItem_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ThreadStart work = () =>
                {
                    try
                    {
                        ReportViewModel reportViewModel = new ReportViewModel();
                        GrapeCity.ActiveReports.Export.Xml.Section.TextExport textExport = new GrapeCity.ActiveReports.Export.Xml.Section.TextExport();

                        System.Windows.Forms.SaveFileDialog saveFileDialog = new System.Windows.Forms.SaveFileDialog();
                        saveFileDialog.Filter = "TXT file (*.txt)|*.txt";
                        saveFileDialog.FilterIndex = 2;
                        saveFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
                        saveFileDialog.RestoreDirectory = true;
                        saveFileDialog.Title = "Save Asset Value Summary Report";
                        saveFileDialog.FileName = reportViewModel.GetReportName("txt", rptPath.FullName);
                        if (saveFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                        {

                            textExport.Export(pageDocument, saveFileDialog.FileName);
                            Application.Current.Dispatcher.BeginInvoke(new Action(() => CommandManager.InvalidateRequerySuggested()));
                            Application.Current.Dispatcher.Invoke(DispatcherPriority.ContextIdle, new Action(() =>
                            {
                                ModernDialog.ShowMessage("Report Successfully Exported ", "Success!", MessageBoxButton.OK);
                            }));
                        }
                    }
                    catch (Exception ex)
                    {
                        if (ex.HResult == -2147024893)
                            ModernDialog.ShowMessage("Export Directory path not found, Please contact your System Administrator!", "Error!", MessageBoxButton.OK);
                        else
                            ModernDialog.ShowMessage(ex.Message, "Error!", MessageBoxButton.OK);
                        ErrorLogging.LogException("Error: " +  ex.Message  + " {0}");

                    }
                };
                new Thread(work).SetApartmentState(ApartmentState.STA);
                work.Invoke();
            }
            catch (Exception ex)
            {
                ErrorLogging.LogException("Error: " +  ex.Message  + " {0}");
                ModernDialog.ShowMessage(ex.Message, "Error!", MessageBoxButton.OK);
            }

        }

        private void ExportToPDFItem_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ThreadStart work = () =>
                {
                    try
                    {
                        ReportViewModel reportViewModel = new ReportViewModel();
                        GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport PDFExport = new GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport();
                        System.Windows.Forms.SaveFileDialog saveFileDialog = new System.Windows.Forms.SaveFileDialog();
                        saveFileDialog.Filter = "PDF file (*.pdf)|*.pdf";
                        saveFileDialog.FilterIndex = 2;
                        saveFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
                        saveFileDialog.RestoreDirectory = true;
                        saveFileDialog.Title = "Save Asset Value Summary Report";
                        saveFileDialog.FileName = reportViewModel.GetReportName("pdf", rptPath.FullName);
                        if (saveFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                        {
                            PDFExport.Export(pageDocument, saveFileDialog.FileName);
                            Application.Current.Dispatcher.BeginInvoke(new Action(() => CommandManager.InvalidateRequerySuggested()));
                            Application.Current.Dispatcher.Invoke(DispatcherPriority.ContextIdle, new Action(() =>
                            {
                                ModernDialog.ShowMessage("Report Successfully Exported ", "Success!", MessageBoxButton.OK);
                            }));
                        }
                    }
                    catch (Exception ex)
                    {
                        if (ex.HResult == -2147024893)
                            ModernDialog.ShowMessage("Export Directory path not found, Please contact your System Administrator!", "Error!", MessageBoxButton.OK);
                        else
                            ModernDialog.ShowMessage(ex.Message, "Error!", MessageBoxButton.OK);
                        ErrorLogging.LogException("Error: " +  ex.Message  + " {0}");
                    }

                };
                new Thread(work).SetApartmentState(ApartmentState.STA);
                work.Invoke();
            }
            catch (Exception ex)
            {
                ErrorLogging.LogException("Error: " +  ex.Message  + " {0}");
                ModernDialog.ShowMessage(ex.Message, "Error!", MessageBoxButton.OK);
            }
        }

        void ExportToExcelItem_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ThreadStart work = () =>
                {

                    try
                    {
                        ReportViewModel reportViewModel = new ReportViewModel();
                        // Export the report in XLSX format.
                        GrapeCity.ActiveReports.Export.Excel.Section.XlsExport XLSExport = new GrapeCity.ActiveReports.Export.Excel.Section.XlsExport();
                        // Set a file format of the exported excel file to Xlsx to support Microsoft Excel 2007 and newer versions.
                        XLSExport.FileFormat = GrapeCity.ActiveReports.Export.Excel.Section.FileFormat.Xlsx;
                        System.Windows.Forms.SaveFileDialog saveFileDialog = new System.Windows.Forms.SaveFileDialog();
                        saveFileDialog.Filter = "Excel file (*.xlsx)|*.xlsx";
                        saveFileDialog.FilterIndex = 2;
                        saveFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
                        saveFileDialog.RestoreDirectory = true;
                        saveFileDialog.Title = "Save Asset Value Summary Report";
                        saveFileDialog.FileName = reportViewModel.GetReportName("xlsx", rptPath.FullName);
                        if (saveFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                        {
                            XLSExport.Export(pageDocument, saveFileDialog.FileName);
                            // XLSExport.Export(pageDocument, reportViewModel.GetReportName("xlsx", rptPath.FullName));
                            Application.Current.Dispatcher.BeginInvoke(new Action(() => CommandManager.InvalidateRequerySuggested()));
                            Application.Current.Dispatcher.Invoke(DispatcherPriority.ContextIdle, new Action(() =>
                            {
                                ModernDialog.ShowMessage("Report Successfully Exported ", "Success!", MessageBoxButton.OK);
                            }));
                        }
                    }
                    catch (Exception ex)
                    {
                        if (ex.HResult == -2147024893)
                            ModernDialog.ShowMessage("Export Directory path not found, Please contact your System Administrator!", "Error!", MessageBoxButton.OK);
                        else
                            ModernDialog.ShowMessage(ex.Message, "Error!", MessageBoxButton.OK);
                        ErrorLogging.LogException("Error: " +  ex.Message  + " {0}");
                    }
                };
                new Thread(work).SetApartmentState(ApartmentState.STA);
                work.Invoke();
            }
            catch (Exception ex)
            {
                ErrorLogging.LogException("Error: " +  ex.Message  + " {0}");
                ModernDialog.ShowMessage(ex.Message, "Error!", MessageBoxButton.OK);
            }

        }

        #endregion
    }
}

