﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TRAKI.App.ViewModel;

namespace TRAKI.App.Pages
{
    /// <summary>
    /// Interaction logic for LogOutPage.xaml
    /// </summary>
    public partial class LogoutPage : UserControl
    {
        public LogoutPage()
        {
            InitializeComponent();

            LoginViewModel loginViewModel = new LoginViewModel();
            loginViewModel.Logout();
         //  Application.Current.Windows[0].Close();
            loginViewModel.ShowView(true);

           // Application.Current.MainWindow.Close();

         //   UserLoginWindow userLoginwindow = new UserLoginWindow();
          //  userLoginwindow.Show();
        }



    }
}
