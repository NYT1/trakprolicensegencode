﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.Linq;
using System.Threading.Tasks;

namespace TRAKI.App
{
    [RunInstaller(true)]
    public partial class TrakInstaller : System.Configuration.Install.Installer
    {
        public TrakInstaller()
        {
            InitializeComponent();
        }

        LicenseDialog frm = new LicenseDialog();

        protected override void OnCommitted(IDictionary savedState)
        {
            if (LaunchOnBeforeInstall())
            {
                base.OnBeforeInstall(savedState);
            }
            else
            {
                throw new Exception("You cancelled installation");
            }
            //using (System.ServiceProcess.ServiceController serviceController = new System.ServiceProcess.ServiceController(TrakProServiceInstaller.ServiceName))
            //{
            //    serviceController.Start();
            //}
        }

        public bool LaunchOnBeforeInstall()
        {
            frm.ShowDialog();
            if (frm.IsSuccess)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
