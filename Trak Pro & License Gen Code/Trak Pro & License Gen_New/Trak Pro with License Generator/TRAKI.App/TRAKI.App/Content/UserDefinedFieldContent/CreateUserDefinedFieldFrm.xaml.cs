﻿using FirstFloor.ModernUI.Windows.Controls;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TRAKI.App.Common;
using TRAKI.App.DapperQueries.UserQueries;
using TRAKI.App.DapperQueries.Common;
using TRAKI.App.Model;
using TRAKI.App.ViewModel;
using System.ComponentModel;
using TRAKI.App.ViewModel.Common;
using FirstFloor.ModernUI.Windows;

namespace TRAKI.App.Content.UserDefinedFieldContent
{
    /// <summary>
    /// Interaction logic for ControlsStylesSampleForm.xaml
    /// </summary>
    public partial class CreateUserDefinedFieldFrm : UserControl, IContent
    {

        private int FieldId = 0;
        private bool _showConfirmationMsg = true;
        ObservableCollection<CustomField> customField;
        public CreateUserDefinedFieldFrm()
        {
            InitializeComponent();
        }


        private void LoadUserDefinedFields()
        {
            UserDefinedFieldViewModel userDefinedFieldViewModel = new UserDefinedFieldViewModel();
            customField = userDefinedFieldViewModel.GetCustomFields(FieldId, null, null);
            this.DataContext = customField;
            this.ComboFieldType.SelectedValue = customField[0].FieldType; ;
        }


        #region IContent

        public void OnFragmentNavigation(FirstFloor.ModernUI.Windows.Navigation.FragmentNavigationEventArgs e)
        {
            if (!string.IsNullOrEmpty(e.Fragment))
            {
                this.FieldId = Convert.ToInt32(e.Fragment);
            }
            this.LoadUserDefinedFields();
        }

        public void OnNavigatedFrom(FirstFloor.ModernUI.Windows.Navigation.NavigationEventArgs e)
        {
           
        }

        public void OnNavigatedTo(FirstFloor.ModernUI.Windows.Navigation.NavigationEventArgs e)
        {
            this._showConfirmationMsg = true;
        }

        public void OnNavigatingFrom(FirstFloor.ModernUI.Windows.Navigation.NavigatingCancelEventArgs e)
        {
            if (this._showConfirmationMsg)
            {
                string message = "Are you sure you want to leave?";
                if (e.NavigationType == FirstFloor.ModernUI.Windows.Navigation.NavigationType.Refresh)
                {
                    message = "Are you sure you want to refresh the page?";
                }
                var confirm = ModernDialog.ShowMessage(message, "Warning!", MessageBoxButton.YesNo);
                if (confirm == MessageBoxResult.No)
                    e.Cancel = true;
            }

        }

        #endregion


        private void TextDisplayOrder_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = IsMatching.CheckForNumber(e.Text);
        }


        private void TextMinimumNumber_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = IsMatching.Check(e.Text);
        }

        private void TextMaximumNumber_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = IsMatching.Check(e.Text);
        }

        private void TextMaximumDate_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = IsMatching.CheckForValidDate(e.Text);
        }

        private void TextMinimumDate_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = IsMatching.CheckForValidDate(e.Text);
        }

        private void TextMinStringLength_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = IsMatching.CheckForNumber(e.Text);
        }

        private void TextMaxStringLength_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = IsMatching.CheckForNumber(e.Text);
        }

        private void btnSubmit_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                UserDefinedFieldViewModel userDefinedFieldsViewModel = new UserDefinedFieldViewModel();
                CustomField customFiled = ((ObservableCollection<CustomField>)(sender as Button).DataContext)[0]; 
                customFiled.FieldType = this.ComboFieldType.SelectedValue.ToString();
                var result = userDefinedFieldsViewModel.InsertUpdateUserDefinedField(customFiled);
                 if (result ==1)
                 {
                     this._showConfirmationMsg = false;
                     ModernDialog.ShowMessage("User Defined field successfully saved", "Success!", MessageBoxButton.OK);
                    NavigationCommands.GoToPage.Execute("/Content/UserDefinedFieldContent/UserDefinedFieldListFrm.xaml#" + null, this);
                 }
                 else if(result== 2)
                 {
                     this._showConfirmationMsg = false;
                    ModernDialog.ShowMessage("User Defined field Updated successfully", "Success!", MessageBoxButton.OK);
                    NavigationCommands.GoToPage.Execute("/Content/UserDefinedFieldContent/UserDefinedFieldListFrm.xaml#" + null, this);
                 }
                else
                    ModernDialog.ShowMessage("Error while saving User Defined feild", "Error!", MessageBoxButton.OK);
                
            }
            catch (Exception ex)
            {

                ErrorLogging.LogException("Error: " + ex.Message + " {0}");
                ModernDialog.ShowMessage(ex.Message, "Error!", MessageBoxButton.OK);
            }
        }

        private void ComboFieldType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            ComboBox comboBox = (sender as ComboBox);
            if (comboBox.SelectedValue != null)
            { 
                comboBox.SelectedValue = customField[0].FieldType;
                comboBox.Items.Refresh();
            }
        }

        private void dtPickerMaximumDate_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
                customField[0].MaxDate = Convert.ToDateTime(String.Format("{0:G}", this.dtPickerMaximumDate.SelectedDate.Value.Date.Add(DateTime.Now.TimeOfDay)));//.ToString();
        }

        private void dtPickerMinimumDate_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            customField[0].MinDate = Convert.ToDateTime(String.Format("{0:G}", this.dtPickerMinimumDate.SelectedDate.Value.Date.Add(DateTime.Now.TimeOfDay)));
        } 
    }
}
