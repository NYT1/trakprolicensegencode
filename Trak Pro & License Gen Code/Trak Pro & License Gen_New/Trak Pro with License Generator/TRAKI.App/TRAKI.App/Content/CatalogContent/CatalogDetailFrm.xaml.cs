﻿using FirstFloor.ModernUI.Windows;
using FirstFloor.ModernUI.Windows.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TRAKI.App.Common;
using TRAKI.App.Model;
using TRAKI.App.Pages;
using TRAKI.App.ViewModel;
using TRAKI.App.ViewModel.Common;

namespace TRAKI.App.Content.CatalogContent
{
    /// <summary>
    /// Interaction logic for CatalogDetailFrm.xaml
    /// </summary>
    public partial class CatalogDetailFrm : UserControl, IContent
    {

        int pageIndex = 1;
        private CatalogViewModel catalogViewModel;
        List<Catalog> _catalogList = new List<Catalog>();

        public CatalogDetailFrm()
        {
            InitializeComponent();
        }

        #region Private

        private void LoadCatalogList()
        {
            try
            {
                catalogViewModel = new CatalogViewModel();
                _catalogList = new List<Catalog>(catalogViewModel.GetCatalog(null, Convert.ToInt32(this.ComboFilterBy.SelectedValue)).ToList());
                this.CatalogGrid.ItemsSource = CollectionViewSource.GetDefaultView(_catalogList.Take(Constants.NumberOfRecordsPerPage));
                this.CatalogGrid.Items.Refresh();
                if (catalogViewModel.RecordCount == 0)
                    this.TextBlockPages.Text = "0";
                else
                    this.TextBlockPages.Text = "1"; 
                this.Navigate((int)PagingMode.First);
                this.DataContext = catalogViewModel;
            }
            catch (Exception ex)
            {
                ModernDialog.ShowMessage("Some error occured, please try again! " + ex.Message, "Error!", MessageBoxButton.OK);
            }
        }

        private void LoadFilterList()
        {
            this.ComboFilterBy.ItemsSource = null;
            this.ComboFilterBy.ItemsSource = Constants.AppSettings.Where(setting => setting.SettingItem == "FilterBy").OrderBy(p => p.DisplayOrder); // new SettingsViewModel().GetFilterByList();
            this.ComboFilterBy.SelectedIndex = 0;
        }

        private void ComboFilterBy_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (this.ComboFilterBy.SelectedValue != null)
                {
                    this.LoadSearchData(this.SearchTextBox.Text.Trim());
                }
            }
            catch (Exception ex)
            {
                ModernDialog.ShowMessage("Some error occured, please try again! " + ex.Message, "Error!", MessageBoxButton.OK);
            }

        }


        private void btnEditCatalog_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Catalog catalog = CatalogGrid.SelectedItem as Catalog;
                NavigationCommands.GoToPage.Execute("/Content/CatalogContent/CreateEditCatalogFrm.xaml#" + catalog.CatalogId, this);
            }
            catch (Exception ex)
            {
                ModernDialog.ShowMessage(ex.Message, "Error!", MessageBoxButton.OK);
            }
        }

        private void btnCreateNewCatalog_Click(object sender, RoutedEventArgs e)
        {
            NavigationCommands.GoToPage.Execute("/Content/CatalogContent/CreateEditCatalogFrm.xaml#" + null, this);
        }

        private void SearchTextBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = IsMatching.Check(e.Text);
        }

#endregion

        #region Pagination

        private void btnFirst_Click(object sender, System.EventArgs e)
        {

            Navigate((int)PagingMode.First);
            this.TextBlockPages.Text = "1";
        }

        private void btnNext_Click(object sender, System.EventArgs e)
        {
            Navigate((int)PagingMode.Next);
            this.TextBlockPages.Text = this.pageIndex.ToString();
        }

        private void btnPrev_Click(object sender, System.EventArgs e)
        {
            Navigate((int)PagingMode.Previous);
            this.TextBlockPages.Text = this.pageIndex.ToString();
        }

        private void btnLast_Click(object sender, System.EventArgs e)
        {
            Navigate((int)PagingMode.Last);
            this.TextBlockPages.Text = this.pageIndex.ToString();
        }

        //private void cbNumberOfRecords_SelectionChanged(object sender, SelectionChangedEventArgs e)
        //{
        //    Navigate((int)PagingMode.PageCountChange);
        //}

        private void Navigate(int mode)
        {
            int count;
            switch (mode)
            {
                case (int)PagingMode.Next:
                    btnPrev.IsEnabled = true;
                    btnFirst.IsEnabled = true;
                    if (_catalogList.Count >= (pageIndex * Constants.NumberOfRecordsPerPage))
                    {

                        if (_catalogList.Skip(pageIndex * Constants.NumberOfRecordsPerPage).Take(Constants.NumberOfRecordsPerPage).Count() == 0)
                        {
                            this.CatalogGrid.ItemsSource = null;
                            this.CatalogGrid.ItemsSource = _catalogList.Skip((pageIndex * Constants.NumberOfRecordsPerPage) - Constants.NumberOfRecordsPerPage).Take(Constants.NumberOfRecordsPerPage);
                            count = (pageIndex * Constants.NumberOfRecordsPerPage) + (_catalogList.Skip(pageIndex * Constants.NumberOfRecordsPerPage).Take(Constants.NumberOfRecordsPerPage)).Count();
                        }
                        else
                        {
                            this.CatalogGrid.ItemsSource = null;
                            this.CatalogGrid.ItemsSource = _catalogList.Skip(pageIndex * Constants.NumberOfRecordsPerPage).Take(Constants.NumberOfRecordsPerPage);
                            count = (pageIndex * Constants.NumberOfRecordsPerPage) + (_catalogList.Skip(pageIndex * Constants.NumberOfRecordsPerPage).Take(Constants.NumberOfRecordsPerPage)).Count();
                            pageIndex++;
                        }

                        if ((pageIndex * Constants.NumberOfRecordsPerPage) >= _catalogList.Count)
                        {
                            btnNext.IsEnabled = false;
                            btnLast.IsEnabled = false;
                        }

                    }
                    else
                    {
                        btnNext.IsEnabled = false;
                        btnLast.IsEnabled = false;
                    }

                    break;
                case (int)PagingMode.Previous:
                    btnNext.IsEnabled = true;
                    btnLast.IsEnabled = true;
                    pageIndex -= 1;

                    if (pageIndex >= 1)
                    {
                        this.CatalogGrid.ItemsSource = null;
                        if (pageIndex == 1)
                        {
                            btnPrev.IsEnabled = false;
                            btnFirst.IsEnabled = false;
                            this.CatalogGrid.ItemsSource = _catalogList.Take(Constants.NumberOfRecordsPerPage);
                            count = _catalogList.Take(Constants.NumberOfRecordsPerPage).Count();
                        }
                        else
                        {
                            this.CatalogGrid.ItemsSource = _catalogList.Skip((pageIndex - 1) * Constants.NumberOfRecordsPerPage).Take(Constants.NumberOfRecordsPerPage);
                            count = Math.Min((pageIndex - 1) * Constants.NumberOfRecordsPerPage, _catalogList.Count);
                        }
                    }
                    else
                    {
                        btnPrev.IsEnabled = false;
                        btnFirst.IsEnabled = false;
                    }

                    if (_catalogList.Count <= Constants.NumberOfRecordsPerPage)
                    {
                        btnNext.IsEnabled = false;
                        btnLast.IsEnabled = false;
                        btnPrev.IsEnabled = false;
                        btnFirst.IsEnabled = false;
                    }
                    break;

                case (int)PagingMode.First:
                    pageIndex = 2;
                    Navigate((int)PagingMode.Previous);
                    break;
                case (int)PagingMode.Last:
                    pageIndex = (_catalogList.Count / Constants.NumberOfRecordsPerPage);
                    Navigate((int)PagingMode.Next);
                    break;

                case (int)PagingMode.PageCountChange:
                    pageIndex = 1;
                    this.CatalogGrid.ItemsSource = null;
                    this.CatalogGrid.ItemsSource = _catalogList.Take(Constants.NumberOfRecordsPerPage);
                    count = (_catalogList.Take(Constants.NumberOfRecordsPerPage)).Count();
                    btnNext.IsEnabled = true;
                    btnPrev.IsEnabled = true;
                    break;
            }
        }

        #endregion

        #region Search Region

        private void btnSearch_Click(object sender, RoutedEventArgs e)
        {
            string searchText = this.SearchTextBox.Text.Trim();
            if (string.IsNullOrEmpty(searchText))
                return;
            try
            {
                this.LoadSearchData(searchText);
            }
            catch (Exception ex)
            {
                ModernDialog.ShowMessage("Some error occured, please try again! " + ex.Message, "Error!", MessageBoxButton.OK);
            }
        }

        private void SearchTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            string searchText = ((TextBox)sender).Text.Trim();
            if (!string.IsNullOrEmpty(searchText))
                return;
            try
            {
                this.LoadSearchData(searchText);
            }
            catch (Exception ex)
            {
                ModernDialog.ShowMessage("Some error occured, please try again! " + ex.Message, "Error!", MessageBoxButton.OK);
            }

        }


        private void SearchTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                string searchText = ((TextBox)sender).Text.Trim();
                if (string.IsNullOrEmpty(searchText))
                    return;
                if (e.Key == Key.Enter)
                    this.LoadSearchData(searchText);
            }
            catch (Exception ex)
            {
                ModernDialog.ShowMessage("Some error occured, please try again! " + ex.Message, "Error!", MessageBoxButton.OK);
            }
        }

        private void LoadSearchData(string SearchText)
        {
            try
            {
                catalogViewModel = new CatalogViewModel();
                _catalogList = new List<Catalog>(catalogViewModel.SearchCatalog(SearchText, Convert.ToInt32(this.ComboFilterBy.SelectedValue)).ToList());
                this.CatalogGrid.ItemsSource = CollectionViewSource.GetDefaultView(_catalogList.Take(Constants.NumberOfRecordsPerPage));
                 this.CatalogGrid.Items.Refresh();
                 if (catalogViewModel.RecordCount == 0)
                     this.TextBlockPages.Text = "0";
                 else
                     this.TextBlockPages.Text = "1";
                this.Navigate((int)PagingMode.First);
                this.DataContext = catalogViewModel;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Icontent

        public void OnFragmentNavigation(FirstFloor.ModernUI.Windows.Navigation.FragmentNavigationEventArgs e)
        {
        }

        public void OnNavigatedFrom(FirstFloor.ModernUI.Windows.Navigation.NavigationEventArgs e)
        {
        }

        public void OnNavigatedTo(FirstFloor.ModernUI.Windows.Navigation.NavigationEventArgs e)
        {
            //this.Content = new CatalogPage(); ;
            // this.LoadCatalogList();
            this.LoadFilterList();
        }

        public void OnNavigatingFrom(FirstFloor.ModernUI.Windows.Navigation.NavigatingCancelEventArgs e)
        {
        }

        #endregion

       
    }
}





