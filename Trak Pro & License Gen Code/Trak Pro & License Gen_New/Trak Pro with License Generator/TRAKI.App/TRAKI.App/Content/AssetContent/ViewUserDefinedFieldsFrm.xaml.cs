﻿using FirstFloor.ModernUI.Windows;
using FirstFloor.ModernUI.Windows.Controls;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TRAKI.App.Common;
using TRAKI.App.Content.CommonContent;
using TRAKI.App.Model;
using TRAKI.App.ViewModel;
using TRAKI.App.ViewModel.Common;

namespace TRAKI.App.Content.AssetContent
{
    /// <summary>
    /// Interaction logic for CreateEditAssetFrm.xaml
    /// </summary>
    public partial class ViewUserDefinedFieldsFrm : UserControl, IContent
    {
        // private bool _showConfirmationMsg = true;

        //
        private static UserDefinedFieldViewModel userDefinedFieldViewModel;
        private static ObservableCollection<CustomField> _userDefinedFieldList;//= new ObservableCollection<CustomField>();
        // CustomField customField;
        //
        public ViewUserDefinedFieldsFrm()
        {
            InitializeComponent();
            this.LoadUserDefinedFields();
            Loaded += UserDefinedFieldsFrm_Loaded;
        }

        void UserDefinedFieldsFrm_Loaded(object sender, RoutedEventArgs e)
        {
            this.LoadUserDefinedFields();
        }

        #region custom section

        

        private void LoadUserDefinedFields()
        {
            try
            {
                userDefinedFieldViewModel = new UserDefinedFieldViewModel();
                _userDefinedFieldList = userDefinedFieldViewModel.GetUserDefinedFieldValues(string.Empty, -1, Constants.AssetNumber);
                this.CustomFieldPanel.Children.Clear();
                foreach (CustomField customField in _userDefinedFieldList)
                {
                    if (_userDefinedFieldList != null && _userDefinedFieldList.Count > 0)
                    {
                        // if (Constants.AssetId == 0)
                        //   customField.FieldValue = string.Empty;
                        if (customField.FieldType.Equals("textbox"))
                            this.AddCustomTextBoxField(customField);
                        else if (customField.FieldType.Equals("datepicker"))
                            this.AddCustomDateField(customField);
                        else if (customField.FieldType.Equals("radiobutton"))
                            this.AddCustomRadioButtonField(customField);
                        else if (customField.FieldType.Equals("checkbox"))
                            this.AddCustomCheckBoxField(customField);
                        else if (customField.FieldType.Equals("dropdownlist"))
                            this.AddCustomDropdownListField(customField);
                    }
                    // this.customFieldPanelMain.Visibility = Visibility.Visible;
                }
                this.DataContext = userDefinedFieldViewModel;
             //   Constants.IsAssetView = false;
            }
            catch (Exception ex)
            {
                throw ex;

            }
        }

        #region text Box Field

        private void AddCustomTextBoxField(CustomField customField)
        {
            StackPanel stPanel = new StackPanel();
            stPanel.Margin = new Thickness(-5, 10, 0, 0);
            stPanel.Orientation = Orientation.Horizontal;
            stPanel.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
            stPanel.VerticalAlignment = System.Windows.VerticalAlignment.Center;
            TextBlock textBlockCaption = new TextBlock();
            textBlockCaption.Text = customField.FieldCaption;
            textBlockCaption.Width = 165;

            // textBlockCaption.Margin = new Thickness(0, 0, 10, 0);
            TextBox textField = new TextBox();
            textField.Width = 250;

            Binding binding = new Binding();
            binding.Source = customField; // set the source object instead of ElementName
            binding.Mode = BindingMode.TwoWay;
            binding.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
            binding.Path = new PropertyPath("FieldValue");

            BindingOperations.SetBinding(textField, TextBox.TextProperty, binding);

            if (customField.DataType.Equals("string"))
            {
                textField.Name = "TextField" + customField.UserDefinedFieldId + "_" + customField.MaxStringLength + "_" + customField.MinStringLength;
                textField.MaxLength = customField.MaxStringLength;
                textField.LostKeyboardFocus += textField_LostKeyboardFocus;
                textField.ToolTip = customField.UserDefinedField + " with minimum string length of " + customField.MinStringLength + " max string length of " + customField.MaxStringLength;
            }

            if (customField.DataType.Equals("number"))
            {
                textField.Name = "TextField" + customField.UserDefinedFieldId + "_" + customField.MaxNumber + "_" + customField.MinNumber;
                textField.PreviewTextInput += TextField_PreviewTextInput;
                textField.MaxLength = 9;
                // tFieldName.LostKeyboardFocus += tFieldNumberName_LostKeyboardFocus;
                textField.ToolTip = customField.UserDefinedField + " with minimum Number " + customField.MinNumber + " and max Number " + customField.MaxNumber;

            }

            //Button image = new Button
            //{
            //    VerticalAlignment = VerticalAlignment.Center,
            //    Width = 28,
            //    Height = 28,
            //    Margin = new Thickness(0, 2, 2, 2),
            //};
            //image.Content = new BitmapImage(new Uri("pack://application:,,,/Images/add.png"));
            //image.Click += image_Click;
            stPanel.Children.Add(textBlockCaption);
            stPanel.Children.Add(textField);
            // stPanel.Children.Add(image);
            // DataGridRow gr = new DataGridRow();
            // RowDefinition rowDefinition = new RowDefinition();
            //rowDefinition.Height = GridLength.Auto;
            // this.CustomFieldPanel.RowDefinitions.Add(rowDefinition);
            this.CustomFieldPanel.Children.Add(stPanel);
        }

        void textField_LostKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
        {
            TextBox textFieldBox = (sender as TextBox);
            string[] txtBoxName = textFieldBox.Name.Split('_');
            string txtBoxValue = textFieldBox.Text;
            if (txtBoxValue.Length > Convert.ToInt32(txtBoxName[1]) || txtBoxValue.Length < Convert.ToInt32(txtBoxName[2]))
            {
                ModernDialog.ShowMessage("Field Values Exceed the Min or Max Limit!", "Error!", MessageBoxButton.OK);
                userDefinedFieldViewModel.IsValid = false;
            }
            else
            {
                userDefinedFieldViewModel.IsValid = true;
            }
        }

        /* validate the number field.
         * 
        void tFieldNumberName_LostKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
        {
            TextBox textFieldBox = (sender as TextBox);
            string[] txtBoxName = textFieldBox.Name.Split('_');
            int txtBoxValue = Convert.ToInt32(textFieldBox.Text);
            if (txtBoxValue > Convert.ToInt32(txtBoxName[1]) || txtBoxValue < Convert.ToInt32(txtBoxName[2]))
            {
                ModernDialog.ShowMessage("Field Values Exceed the Min or Max Limit!", "Error!", MessageBoxButton.OK);
                userDefinedFieldViewModel.IsValid = false;
            }
            else
                userDefinedFieldViewModel.IsValid = true;
        }
        */
        /*

        void tFieldNameText_TextChanged(object sender, TextChangedEventArgs e)
        {
            //  throw new NotImplementedException();
            TextBox textFieldBox = (sender as TextBox);
            string[] txtBoxName = textFieldBox.Name.Split('_');
            string txtBoxValue = textFieldBox.Text;
            if (txtBoxValue.Length > Convert.ToInt32(txtBoxName[1]) || txtBoxValue.Length < Convert.ToInt32(txtBoxName[2]))
            {
                ModernDialog.ShowMessage("Field Values Exceed the Min or Max Limit!", "Error!", MessageBoxButton.OK);   
            }
        }

        void tFieldNameNumber_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox textFieldBox = (sender as TextBox);
            string[] txtBoxName = textFieldBox.Name.Split('_');
            int txtBoxValue = Convert.ToInt32(textFieldBox.Text);
            if (txtBoxValue > Convert.ToInt32(txtBoxName[1]) || txtBoxValue < Convert.ToInt32(txtBoxName[2]))
            {
                 ModernDialog.ShowMessage("Field Values Exceed the Min or Max Limit!", "Error!", MessageBoxButton.OK);
            }
        }

         * */
        private void TextField_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = IsMatching.CheckForNumber(e.Text);
        }


        #endregion

        #region radio button

        private void AddCustomRadioButtonField(CustomField customField)
        {
            if (!string.IsNullOrEmpty(customField.PickListValues))
            {
                string[] content = customField.PickListValues.Split(';');
                StackPanel stPanel = new StackPanel();
                stPanel.Margin = new Thickness(-5, 10, 0, 0);
                stPanel.Orientation = Orientation.Horizontal;
                stPanel.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
                stPanel.VerticalAlignment = System.Windows.VerticalAlignment.Center;
                TextBlock textBlockCaption = new TextBlock();
                textBlockCaption.Text = customField.FieldCaption;
                textBlockCaption.Width = 165;
                // textBlockCaption.Margin   = new Thickness(0, 0, 0, 0);

                //RadioButton rb = new RadioButton() { Content = obj, };
                //sp.Children.Add(rb);
                //rb.Checked += new RoutedEventHandler(rb_Checked);
                //rb.Unchecked += new RoutedEventHandler(rb_Unchecked);


                //<RadioButton  TabIndex="9" Content="Yes"  IsChecked="True">
                //        </RadioButton>
                //        <RadioButton TabIndex="10" IsChecked="{Binding Path=IsActive, 
                //    Converter={StaticResource RadioButtonCheckedConverter}, 
                //    ConverterParameter={x:Static common:BooleanValue.False}}" Content="No" Margin="8,0,0,0" />
                stPanel.Children.Add(textBlockCaption);
                foreach (var item in content)
                {
                    if (!string.IsNullOrWhiteSpace(item))
                    {
                        RadioButton radioBtnField = new RadioButton();
                        radioBtnField.GroupName = "RadioButtonField_" + customField.UserDefinedFieldId;
                        radioBtnField.Content = item;
                        radioBtnField.Margin = new Thickness(0, 0, 10, 0);
                        radioBtnField.Name = "TextDateField_" + customField.UserDefinedFieldId;
                        radioBtnField.Checked += AadioBtnField_Checked;
                        radioBtnField.Unchecked += radioBtnField_Unchecked;
                        if (!string.IsNullOrEmpty(customField.FieldValue))
                            if (customField.FieldValue.Trim().Contains(radioBtnField.Content.ToString().Trim()))
                                radioBtnField.IsChecked = true;
                        stPanel.Children.Add(radioBtnField);
                    }

                }
                // int loc = Array.IndexOf(content, customField.FieldValue);
                //RadioButton rbFieldName = new RadioButton();
                //rbFieldName.GroupName = "RadioButtonField";
                //rbFieldName.Content = content[0];
                //rbFieldName.Margin = new Thickness(0, 0, 10, 0);
                //rbFieldName.Name = "TextDateField_" + customField.UserDefinedFieldId;
                //rbFieldName.Checked += rbFieldName_Checked;
                //RadioButton rbFieldName2 = new RadioButton();
                //rbFieldName2.GroupName = "RadioButtonField";
                //rbFieldName2.Content = content[1];
                //rbFieldName2.Margin = new Thickness(0, 0, 10, 0);
                //rbFieldName2.Name = "TextDateField_" + customField.UserDefinedFieldId;
                //rbFieldName2.Checked += rbFieldName2_Checked;



                //Binding binding = new Binding() ;//{ Converter = new MyAwesomeConverter() };
                //binding.Source = customField; // set the source object instead of ElementName
                //binding.Mode = BindingMode.TwoWay;
                //binding.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
                //binding.Path = new PropertyPath("FieldValue");
                //BindingOperations.SetBinding(rbFieldName2, CheckBox.IsCheckedProperty, binding);




                // stPanel.Children.Add(rbFieldName2);
                // RowDefinition rowDefinition = new RowDefinition();
                //rowDefinition.Height = GridLength.Auto;

                //  this.CustomFieldPanel.RowDefinitions.Add(rowDefinition);
                this.CustomFieldPanel.Children.Add(stPanel);
            }

        }

        private void radioBtnField_Unchecked(object sender, RoutedEventArgs e)
        {
            RadioButton rBtn = (sender as RadioButton);
            string[] rBtnName = rBtn.Name.Split('_');
            CustomField result = _userDefinedFieldList.Where(x => x.UserDefinedFieldId == Convert.ToInt32(rBtnName[1])).FirstOrDefault();
            if (!string.IsNullOrEmpty(result.FieldValue))
            {
                string[] res = result.FieldValue.Split(';');
                res = res.Where(x => x != rBtn.Content.ToString()).ToArray();
                result.FieldValue = String.Join(";", res);
            }
        }

        private void AadioBtnField_Checked(object sender, RoutedEventArgs e)
        {
            RadioButton rBtn = (sender as RadioButton);
            string[] rBtnName = rBtn.Name.Split('_');

            // result.FieldValue = rBtn.Content.ToString();
            CustomField result = _userDefinedFieldList.Where(x => x.UserDefinedFieldId == Convert.ToInt32(rBtnName[1])).FirstOrDefault();

            if (string.IsNullOrEmpty(result.FieldValue))
                result.FieldValue = rBtn.Content.ToString();
            else if (result.FieldValue.Contains(rBtn.Content.ToString()))
            {
                return;
            }
            else
                result.FieldValue += ";" + rBtn.Content.ToString();
        }

        #endregion

        #region Check Box

        private void AddCustomCheckBoxField(CustomField customField)
        {
            if (!string.IsNullOrEmpty(customField.PickListValues))
            {
                string[] content = customField.PickListValues.Split(';');
                StackPanel stPanel = new StackPanel();
                stPanel.Margin = new Thickness(-5, 10, 0, 0);
                stPanel.Orientation = Orientation.Horizontal;
                stPanel.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
                stPanel.VerticalAlignment = System.Windows.VerticalAlignment.Center;
                TextBlock textBlockCaption = new TextBlock();
                textBlockCaption.Text = customField.FieldCaption;
                textBlockCaption.Width = 165;
                // textBlockCaption.Margin   = new Thickness(0, 0, 10, 0);

                //RadioButton rb = new RadioButton() { Content = obj, };
                //sp.Children.Add(rb);
                //rb.Checked += new RoutedEventHandler(rb_Checked);
                //rb.Unchecked += new RoutedEventHandler(rb_Unchecked);


                //<RadioButton  TabIndex="9" Content="Yes"  IsChecked="True">
                //        </RadioButton>
                //        <RadioButton TabIndex="10" IsChecked="{Binding Path=IsActive, 
                //    Converter={StaticResource RadioButtonCheckedConverter}, 
                //    ConverterParameter={x:Static common:BooleanValue.False}}" Content="No" Margin="8,0,0,0" />
                stPanel.Children.Add(textBlockCaption);
                foreach (var item in content)
                {
                    if (!string.IsNullOrWhiteSpace(item))
                    {
                        CheckBox chkBoxField = new CheckBox();
                        chkBoxField.Content = item;
                        chkBoxField.Margin = new Thickness(0, 0, 10, 0);
                        chkBoxField.Name = "TextDateField_" + customField.UserDefinedFieldId;
                        chkBoxField.Checked += ChkBoxField_Checked;
                        chkBoxField.Unchecked += ChkBoxField_Unchecked;
                        if (!string.IsNullOrEmpty(customField.FieldValue))
                            if (customField.FieldValue.Trim().Contains(chkBoxField.Content.ToString().Trim()))
                                chkBoxField.IsChecked = true;
                        stPanel.Children.Add(chkBoxField);
                    }
                }
                // int loc = Array.IndexOf(content, customField.FieldValue);
                //RadioButton rbFieldName = new RadioButton();
                //rbFieldName.GroupName = "RadioButtonField";
                //rbFieldName.Content = content[0];
                //rbFieldName.Margin = new Thickness(0, 0, 10, 0);
                //rbFieldName.Name = "TextDateField_" + customField.UserDefinedFieldId;
                //rbFieldName.Checked += rbFieldName_Checked;
                //RadioButton rbFieldName2 = new RadioButton();
                //rbFieldName2.GroupName = "RadioButtonField";
                //rbFieldName2.Content = content[1];
                //rbFieldName2.Margin = new Thickness(0, 0, 10, 0);
                //rbFieldName2.Name = "TextDateField_" + customField.UserDefinedFieldId;
                //rbFieldName2.Checked += rbFieldName2_Checked;



                //Binding binding = new Binding() ;//{ Converter = new MyAwesomeConverter() };
                //binding.Source = customField; // set the source object instead of ElementName
                //binding.Mode = BindingMode.TwoWay;
                //binding.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
                //binding.Path = new PropertyPath("FieldValue");
                //BindingOperations.SetBinding(rbFieldName2, CheckBox.IsCheckedProperty, binding);




                // stPanel.Children.Add(rbFieldName2);
                // RowDefinition rowDefinition = new RowDefinition();
                //rowDefinition.Height = GridLength.Auto;

                //  this.CustomFieldPanel.RowDefinitions.Add(rowDefinition);
                this.CustomFieldPanel.Children.Add(stPanel);
            }

        }

        private void ChkBoxField_Unchecked(object sender, RoutedEventArgs e)
        {
            CheckBox chkBox = (sender as CheckBox);
            string[] chkBoxName = chkBox.Name.Split('_');
            CustomField result = _userDefinedFieldList.Where(x => x.UserDefinedFieldId == Convert.ToInt32(chkBoxName[1])).FirstOrDefault();
            if (!string.IsNullOrEmpty(result.FieldValue))
            {
                string[] res = result.FieldValue.Split(';');
                res = res.Where(x => x != chkBox.Content.ToString()).ToArray();
                result.FieldValue = String.Join(";", res);
            }
        }

        private void ChkBoxField_Checked(object sender, RoutedEventArgs e)
        {
            CheckBox chkBox = (sender as CheckBox);
            string[] chkBoxName = chkBox.Name.Split('_');
            CustomField result = _userDefinedFieldList.Where(x => x.UserDefinedFieldId == Convert.ToInt32(chkBoxName[1])).FirstOrDefault();

            if (string.IsNullOrEmpty(result.FieldValue))
                result.FieldValue = chkBox.Content.ToString();
            else if (result.FieldValue.Contains(chkBox.Content.ToString()))
            {
                return;
            }
            else
                result.FieldValue += ";" + chkBox.Content.ToString();


        }

        #endregion

        #region DateField

        private void AddCustomDateField(CustomField customField)
        {
            //ModernFrame newFrame = new ModernFrame();
            //newFrame.Source = new Uri(customField.Description + "#" + new UserDefinedFieldViewModel(), UriKind.Relative);
            //TextBlock text = new TextBlock();
            //text.Text = customField.FieldCaption;
            //this.CustomFieldPanel.Children.Add(text);
            //this.CustomFieldPanel.Children.Add(newFrame);

            //<TextBox x:Name="TextDateField" TabIndex="13" IsReadOnly="True" Width="250" 
            //               Text="{Binding FieldValue, Mode=TwoWay, UpdateSourceTrigger=PropertyChanged, ValidatesOnDataErrors=True}" />
            //      <DatePicker x:Name="dtPicker" TabIndex="14" SelectedDateChanged="dtPicker_SelectedDateChanged"  Width="24" Height="24" Margin="0 0 0 0" Padding="0 0 0 0">
            //     </DatePicker>

            StackPanel stPanel = new StackPanel();
            stPanel.Margin = new Thickness(-5, 10, 0, 0);
            stPanel.Orientation = Orientation.Horizontal;
            stPanel.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
            stPanel.VerticalAlignment = System.Windows.VerticalAlignment.Center;
            TextBlock textBlockCaption = new TextBlock();
            textBlockCaption.Text = customField.FieldCaption;
            textBlockCaption.Width = 165;
            // textBlockCaption.Margin   = new Thickness(0, 0, 10, 0);
            TextBox tFieldName = new TextBox();
            tFieldName.Width = 225;
            //tFieldName.Name = "TextDateField_"+ customField.UserDefinedFieldId;
            Binding binding = new Binding();
            binding.Source = customField; // set the source object instead of ElementName
            binding.Mode = BindingMode.TwoWay;
            binding.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
            binding.Path = new PropertyPath("FieldValue");
            BindingOperations.SetBinding(tFieldName, TextBox.TextProperty, binding);
            DatePicker datepicker = new DatePicker();
            datepicker.Width = 24;
            datepicker.Height = 24;
            datepicker.Margin = new Thickness(0, 0, 0, 0);
            datepicker.Padding = new Thickness(0, 0, 0, 0);
            datepicker.DisplayDateStart = customField.MinDate;
            datepicker.DisplayDateEnd = customField.MaxDate;
            datepicker.SelectedDateChanged += Datepicker_SelectedDateChanged;
            datepicker.Name = "TextDateField_" + customField.UserDefinedFieldId;
            stPanel.Children.Add(textBlockCaption);
            stPanel.Children.Add(tFieldName);
            stPanel.Children.Add(datepicker);
            // RowDefinition rowDefinition = new RowDefinition();
            //rowDefinition.Height = GridLength.Auto;

            //  this.CustomFieldPanel.RowDefinitions.Add(rowDefinition);
            this.CustomFieldPanel.Children.Add(stPanel);

        }

        private void Datepicker_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            DatePicker picker = (sender as DatePicker);
            string[] pickerName = picker.Name.Split('_');

            CustomField result = _userDefinedFieldList.Where(x => x.UserDefinedFieldId == Convert.ToInt32(pickerName[1])).FirstOrDefault();
            result.FieldValue = String.Format("{0:G}", picker.SelectedDate.Value.Date.Add(DateTime.Now.TimeOfDay));//.ToString();
        }

        #endregion

        #region Dropdown List

        private void AddCustomDropdownListField(CustomField customField)
        {
            if (!string.IsNullOrEmpty(customField.PickListValues))
            {
                string[] content = customField.PickListValues.Split(';');
                StackPanel stPanel = new StackPanel();
                stPanel.Margin = new Thickness(-5, 10, 0, 0);
                stPanel.Orientation = Orientation.Horizontal;
                stPanel.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
                stPanel.VerticalAlignment = System.Windows.VerticalAlignment.Center;
                TextBlock textBlockCaption = new TextBlock();
                textBlockCaption.Text = customField.FieldCaption;
                textBlockCaption.Width = 165;
                // textBlockCaption.Margin   = new Thickness(0, 0, 10, 0);

                stPanel.Children.Add(textBlockCaption);
                ComboBox comboBoxField = new ComboBox();
                comboBoxField.Width = 250;
                // <ComboBox ItemsSource="{Binding cbItems}" SelectedItem="{Binding SelectedcbItem}"/>
                // ObservableCollection<ComboBoxItem> cbItems = new ObservableCollection<ComboBoxItem>();
                //var cbItem = new ComboBoxItem { Content = "<--Select-->" };
                //SelectedcbItem = cbItem;
                //cbItems.Add(cbItem);
                //cbItems.Add(new ComboBoxItem { Content = "Option 1" });
                // cbItems.Add(new ComboBoxItem { Content = "Option 2" });

                foreach (var item in content)
                {
                    if (!string.IsNullOrWhiteSpace(item))
                    {
                        //  cbItems.Add(new ComboBoxItem { Content = item });
                        // cbItems.Add(new ComboBoxItem { Content = item });

                        ComboBoxItem cmboBoxFieldItem = new ComboBoxItem();
                        cmboBoxFieldItem.Content = item;
                        cmboBoxFieldItem.Name = "ComboItemField_" + customField.UserDefinedFieldId;
                        // chkBoxFieldName.Margin = new Thickness(0, 0, 10, 0);
                        // chkBoxFieldName.Name = "TextDateField_" + customField.UserDefinedFieldId;
                        // chkBoxFieldName.Checked += chkBoxFieldName_Checked;
                        // chkBoxFieldName.Unchecked += chkBoxFieldName_Unchecked;
                        if (!string.IsNullOrEmpty(customField.FieldValue))
                            if (customField.FieldValue.Trim().Contains(cmboBoxFieldItem.Content.ToString().Trim()))
                                cmboBoxFieldItem.IsSelected = true;

                        comboBoxField.Items.Add(cmboBoxFieldItem);

                    }
                }
                //  comboBoxField.ItemsSource = cbItems;

                //  comboBoxField.SelectionChanged += comboBoxField_SelectionChanged;

                stPanel.Children.Add(comboBoxField);

                // int loc = Array.IndexOf(content, customField.FieldValue);
                //RadioButton rbFieldName = new RadioButton();
                //rbFieldName.GroupName = "RadioButtonField";
                //rbFieldName.Content = content[0];
                //rbFieldName.Margin = new Thickness(0, 0, 10, 0);
                //rbFieldName.Name = "TextDateField_" + customField.UserDefinedFieldId;
                //rbFieldName.Checked += rbFieldName_Checked;
                //RadioButton rbFieldName2 = new RadioButton();
                //rbFieldName2.GroupName = "RadioButtonField";
                //rbFieldName2.Content = content[1];
                //rbFieldName2.Margin = new Thickness(0, 0, 10, 0);
                //rbFieldName2.Name = "TextDateField_" + customField.UserDefinedFieldId;
                //rbFieldName2.Checked += rbFieldName2_Checked;



                //Binding binding = new Binding() ;//{ Converter = new MyAwesomeConverter() };
                //binding.Source = customField; // set the source object instead of ElementName
                //binding.Mode = BindingMode.TwoWay;
                //binding.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
                //binding.Path = new PropertyPath("FieldValue");
                //BindingOperations.SetBinding(rbFieldName2, CheckBox.IsCheckedProperty, binding);




                // stPanel.Children.Add(rbFieldName2);
                // RowDefinition rowDefinition = new RowDefinition();
                //rowDefinition.Height = GridLength.Auto;

                //  this.CustomFieldPanel.RowDefinitions.Add(rowDefinition);
                this.CustomFieldPanel.Children.Add(stPanel);
            }
        }

        private void ComboBoxField_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            ComboBoxItem chkBox = (sender as ComboBoxItem);
            string[] chkBoxName = chkBox.Name.Split('_');
            CustomField result = _userDefinedFieldList.Where(x => x.UserDefinedFieldId == Convert.ToInt32(chkBoxName[1])).FirstOrDefault();
            if (!string.IsNullOrEmpty(result.FieldValue))
            {
                string[] res = result.FieldValue.Split(';');
                res = res.Where(x => x != chkBox.Content.ToString()).ToArray();
                result.FieldValue = String.Join(";", res);
            }

        }

        #endregion

        #endregion

        #region IContent

        public void OnFragmentNavigation(FirstFloor.ModernUI.Windows.Navigation.FragmentNavigationEventArgs e)
        {
            // LoadUserDefinedFields();
        }

        public void OnNavigatedFrom(FirstFloor.ModernUI.Windows.Navigation.NavigationEventArgs e)
        {
            //  throw new NotImplementedException();
        }

        public void OnNavigatedTo(FirstFloor.ModernUI.Windows.Navigation.NavigationEventArgs e)
        {
            //this.LoadUserDefinedFields();
        }

        public void OnNavigatingFrom(FirstFloor.ModernUI.Windows.Navigation.NavigatingCancelEventArgs e)
        {
            //if (this._showConfirmationMsg)
            //{
            //    string message = "Are you sure you want to leave?";
            //    if (e.NavigationType == FirstFloor.ModernUI.Windows.Navigation.NavigationType.Refresh)
            //    {
            //        message = "Are you sure you want to refresh the page?";
            //    }
            //    if (e.NavigationType == FirstFloor.ModernUI.Windows.Navigation.NavigationType.Back)
            //    {
            //       // this.assetId = -1;
            //    }
            //    var confirm = ModernDialog.ShowMessage(message, "Warning!", MessageBoxButton.YesNo);
            //    if (confirm == MessageBoxResult.No)
            //        e.Cancel = true;
            //}
        }

        #endregion

        private void btnSubmitButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                userDefinedFieldViewModel = new UserDefinedFieldViewModel();
                var result = userDefinedFieldViewModel.InsertUpdateUserDefinedFieldValues(_userDefinedFieldList.ToList(), Constants.AssetNumber);
                if (result > 0)
                    ModernDialog.ShowMessage("Custom fields Successfully Saved!", "Success", MessageBoxButton.OK);

            }
            catch (Exception ex)
            {
                if (ex.HResult == -2147467259)
                    ModernDialog.ShowMessage("Asset Number already exists, Please try another Number!", "Error!", MessageBoxButton.OK);
                else
                {
                    ModernDialog.ShowMessage(ex.Message, "Error!", MessageBoxButton.OK);
                }
            }
        }

        internal static int SaveUserDefinedFields()
        {
            try
            {
                userDefinedFieldViewModel = new UserDefinedFieldViewModel();
                return userDefinedFieldViewModel.InsertUpdateUserDefinedFieldValues(_userDefinedFieldList.ToList(), Constants.AssetNumber);
                // if (result > 0)
                //  ModernDialog.ShowMessage("Custom fields Successfully Saved!", "Success", MessageBoxButton.OK);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


    }
}
