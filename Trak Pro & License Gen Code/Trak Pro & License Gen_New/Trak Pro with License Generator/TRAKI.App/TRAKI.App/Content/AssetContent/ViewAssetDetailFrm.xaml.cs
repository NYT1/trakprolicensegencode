﻿using FirstFloor.ModernUI.Windows;
using FirstFloor.ModernUI.Windows.Controls;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TRAKI.App.Content.CommonContent;
using TRAKI.App.Model;
using TRAKI.App.ViewModel;

namespace TRAKI.App.Content.AssetContent
{
    /// <summary>
    /// Interaction logic for ViewAssetDetailFrm.xaml
    /// </summary>
    public partial class ViewAssetDetailFrm : UserControl, IContent
    {
        // private int buildingId = -1;
        private AssetViewModel assetViewModel;
        private int assetId = -1;
        private string old_BuildingNumber = string.Empty;
        private string old_RoomNumber = string.Empty;
        private ObservableCollection<Asset> asset;
        // private bool _showConfirmationMsg = true;

        public ViewAssetDetailFrm()
        {
            InitializeComponent();
        }

        #region Private

        private void LoadAssetDetail()
        {
            assetViewModel = new AssetViewModel();
            asset = assetViewModel.GetAssetForEdit(this.assetId);
            this.DataContext = asset;// assetViewModel;
        }


        private void btnChangeBarcode_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var content = new BarcodeHistoryDialog(this.assetId);
                ModernDialog dialog = new ModernDialog();
                dialog.Title = "Barcode Change History";
                dialog.CloseButton.Content = "Close";
                dialog.Content = content;
                dialog.Buttons = new Button[] { dialog.CloseButton };
                dialog.ShowDialog();
                if (dialog.DialogResult.Value == false)
                {
                    this.LoadAssetDetail();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        private void btnPreviousLocation_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Asset asset = ((ObservableCollection<Asset>)(sender as Button).DataContext)[0];
                PreviousLocationPopUpDialog previousLocationPopUpDialog = new PreviousLocationPopUpDialog(asset.AssetId);
                previousLocationPopUpDialog.MinWidth = 900;
                previousLocationPopUpDialog.Buttons = new Button[] { previousLocationPopUpDialog.CloseButton };
                previousLocationPopUpDialog.ShowDialog();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region IContent

        public void OnFragmentNavigation(FirstFloor.ModernUI.Windows.Navigation.FragmentNavigationEventArgs e)
        {
            this.assetId = -1;
            if (!string.IsNullOrEmpty(e.Fragment))
            {
                this.assetId = Convert.ToInt32(e.Fragment);
            }
            this.LoadAssetDetail();
        }

        public void OnNavigatedFrom(FirstFloor.ModernUI.Windows.Navigation.NavigationEventArgs e)
        {
            //  throw new NotImplementedException();
        }

        public void OnNavigatedTo(FirstFloor.ModernUI.Windows.Navigation.NavigationEventArgs e)
        {
            //  this._showConfirmationMsg = true;
        }

        public void OnNavigatingFrom(FirstFloor.ModernUI.Windows.Navigation.NavigatingCancelEventArgs e)
        {
            //if (this._showConfirmationMsg)
            //{
            //    string message = "Are you sure you want to leave?";
            //    if (e.NavigationType == FirstFloor.ModernUI.Windows.Navigation.NavigationType.Refresh)
            //    {
            //        message = "Are you sure you want to refresh the page?";
            //    }
            //    var confirm = ModernDialog.ShowMessage(message, "Warning!", MessageBoxButton.YesNo);
            //    if (confirm == MessageBoxResult.No)
            //        e.Cancel = true;
            //}
        }

        #endregion
    }
}
