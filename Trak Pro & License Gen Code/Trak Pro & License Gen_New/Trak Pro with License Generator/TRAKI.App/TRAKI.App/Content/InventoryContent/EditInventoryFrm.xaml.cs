﻿using FirstFloor.ModernUI.Windows;
using FirstFloor.ModernUI.Windows.Controls;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using TRAKI.App.Common;
using TRAKI.App.Model;
using TRAKI.App.ViewModel;

namespace TRAKI.App.Content.InventoryContent
{
    /// <summary>
    /// Interaction logic for EditInventoryFrm.xaml
    /// </summary>
    public partial class EditInventoryFrm : UserControl
    {

        string result = "Cancel";

        private long recordCounter = 0;//1;
        private long recordCount = 0;
        private long minIdValue = 0;
        private long maxIdValue = 0;
        private InventoryViewModel inventoryViewModel;
        List<Inventory> _inventoryList = new List<Inventory>();
        List<Inventory> _updatedInventoryList;
        Inventory _orginalInventory;
        public EditInventoryFrm()
        {
            InitializeComponent();
            this.Loaded += EditInventoryFrm_Loaded;
        }

        #region Private


        private void EditInventoryFrm_Loaded(object sender, RoutedEventArgs e)
        {
            this.LoadInventoryData(string.Empty);
            _updatedInventoryList = new List<Inventory>();
        }

        private void LoadInventoryData(string SearchText)
        {
            try
            {
                inventoryViewModel = new InventoryViewModel();
                Application.Current.Dispatcher.Invoke(DispatcherPriority.Normal, new Action(() =>
                 {
                     this.BusyBar.IsBusy = true;
                 }));

                ThreadStart work = () =>
                 {
                     _inventoryList = new List<Inventory>(inventoryViewModel.GetUploadData(null, null, SqlOperatorValue.GreaterThan, SearchText, Constants.NumberOfRecordsPerPage));
                     this.recordCount = inventoryViewModel.GetUploadDataRecordCount(string.Empty);
                     if (_inventoryList != null && _inventoryList.Count > 0)
                         foreach (Inventory _inv in _inventoryList)
                         {
                             Inventory InItem = _updatedInventoryList.Find(item => item.UploadDataId == _inv.UploadDataId);
                             if (InItem == null) // check item isn't null
                             {
                                 _updatedInventoryList.Add(_inv);
                             }

                         }
                     Application.Current.Dispatcher.BeginInvoke(new Action(() => CommandManager.InvalidateRequerySuggested()));
                     Application.Current.Dispatcher.Invoke(DispatcherPriority.ContextIdle, new Action(() =>
                  {
                      inventoryViewModel.RecordCount = recordCount;
                      this.recordCounter = Constants.NumberOfRecordsPerPage;
                      this.SetMaxAndMinIdValue(_inventoryList);
                      if (_inventoryList.Count > 0 && _inventoryList != null)
                      {
                          this.minIdValue = _inventoryList.Min(uploadData => uploadData.UploadDataId);
                          this.maxIdValue = _inventoryList.Max(uploadData => uploadData.UploadDataId);
                      }
                      // inventoryViewModel.EmptyMessageVisibility = false;
                      //  this.DataContext = inventoryViewModel;
                      this.InventoryGrid.ItemsSource = CollectionViewSource.GetDefaultView(_inventoryList);
                      this.InventoryGrid.Items.Refresh();
                      this.BusyBar.IsBusy = false;
                      if (inventoryViewModel.RecordCount == 0)
                          this.TextBlockPages.Text = "0";
                      else
                          this.TextBlockPages.Text = "1";
                  }));
                 };
                new Thread(work).Start();
                this.DataContext = inventoryViewModel;
            }
            catch (Exception ex)
            {
                ModernDialog.ShowMessage("Some error occured, please try again! " + ex.Message, "Error!", MessageBoxButton.OK);
            }
        }

        private void InventoryGrid_SelectionChanged(object sender, RoutedEventArgs e)
        {
            try
            {
                var datagrid = sender as DataGrid;
                if (datagrid.SelectedItems.Count > 1)
                {
                    ModernDialog dialog = new ModernDialog();
                    dialog.SizeToContent = SizeToContent.Manual;
                    dialog.YesButton.Content = "Accept";
                    dialog.NoButton.Content = "Reject";
                    dialog.CancelButton.Content = "Cancel";
                    dialog.YesButton.Click += YesButton_Click;
                    dialog.NoButton.Click += NoButton_Click;
                    dialog.Content = "\n\nIf Accepted QA of all the selected records will be set to TRUE and if Rejected the QA will be set to FALSE!\n You can cancel the Action by pressing Cancel!";
                    dialog.Title = "Confirmation!";
                    dialog.Buttons = new Button[] { dialog.CancelButton, dialog.YesButton, dialog.NoButton };
                    dialog.MaxHeight = 50;
                    dialog.ShowDialog();
                    if (result.Equals("Yes"))
                    {
                        for (int i = 0; i < datagrid.SelectedItems.Count; i++)
                        {
                            Inventory _inventory = (Inventory)datagrid.SelectedItems[i];
                            if (_updatedInventoryList.Contains(_inventory))
                                _updatedInventoryList.Remove(_inventory);

                            _inventory.QA = BooleanValue.True;
                            _updatedInventoryList.Add(_inventory);
                        }
                    }
                    else if (result.Equals("No"))
                    {
                        for (int i = 0; i < datagrid.SelectedItems.Count; i++)
                        {
                            Inventory _inventory = (Inventory)datagrid.SelectedItems[i];
                            _updatedInventoryList.Remove(_inventory);
                            _inventory.QA = BooleanValue.False;
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        void NoButton_Click(object sender, RoutedEventArgs e)
        {
            result = "No";
        }

        void YesButton_Click(object sender, RoutedEventArgs e)
        {
            result = "Yes";
        }

        private void btnSaveAndClose_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                inventoryViewModel = new InventoryViewModel();
                Application.Current.Dispatcher.Invoke(DispatcherPriority.Normal, new Action(() =>
                 {
                     this.BusyBar.IsBusy = true;
                 }));

                ThreadStart work = () =>
                 {
                     int result = inventoryViewModel.SaveInventoryData(this._updatedInventoryList.Where(inventory => inventory.QA == BooleanValue.True).ToList());

                     Application.Current.Dispatcher.BeginInvoke(new Action(() => CommandManager.InvalidateRequerySuggested()));
                     Application.Current.Dispatcher.Invoke(DispatcherPriority.ContextIdle, new Action(() =>
                  {
                      if (result > 0)
                      {
                          this.BusyBar.IsBusy = false;
                          ModernDialog.ShowMessage("Inventory Data sucessfully Saved", "Success", MessageBoxButton.OK);
                          this.InventoryGrid.ItemsSource = null;
                          this.InventoryGrid.Items.Refresh();
                      }
                      this.DataContext = inventoryViewModel;
                  }));
                 };
                new Thread(work).Start();
            }
            catch (Exception ex)
            {
                ModernDialog.ShowMessage(ex.Message, "Error!", MessageBoxButton.OK);
            }
        }

        private void InventoryGrid_CellEditEnding(object sender, DataGridCellEditEndingEventArgs e)
        {
            try
            {
                DataGrid dataGrid = sender as DataGrid;
                bool errors = false;
                errors = (from c in
                              (from object i in dataGrid.ItemsSource
                               select dataGrid.ItemContainerGenerator.ContainerFromItem(i))
                          where c != null
                          select Validation.GetHasError(c)).FirstOrDefault(x => x);

                Inventory _inventory = e.Row.Item as Inventory;
                var InItem = _updatedInventoryList.Where(item => item.AssetNumber == _inventory.AssetNumber).ToList();
                if (InItem.Count > 1) // check item isn't null
                {
                    ModernDialog.ShowMessage("AssetNumber already in the List, Please entere differnt one!", "Error!", MessageBoxButton.OK);
                    errors = true;
                }

                if (errors)
                {
                    (e.Row.Item as Inventory).AssetNumber = _orginalInventory.AssetNumber;
                    (e.Row.Item as Inventory).RoomNumber = _orginalInventory.RoomNumber;
                    (e.Row.Item as Inventory).BuildingNumber = _orginalInventory.BuildingNumber;
                    return;
                }
                inventoryViewModel.FindEditRecordDiscrepancy(_inventory);
            }
            catch (Exception ex)
            {
                ModernDialog.ShowMessage(ex.Message, "Error!", MessageBoxButton.OK);
            }
        }

        private void InventoryGrid_BeginningEdit(object sender, DataGridBeginningEditEventArgs e)
        {
            try
            {
                _orginalInventory = (Inventory)(e.Row.Item as Inventory).Clone();
            }
            catch (Exception ex)
            {
                ModernDialog.ShowMessage(ex.Message, "Error!", MessageBoxButton.OK);
            }
        }

        private void btnDeleteAll_Click(object sender, RoutedEventArgs e)
        {

            try
            {
                InventoryViewModel inventoryViewModel = new InventoryViewModel();
                if (ModernDialog.ShowMessage("Are you sure you want to Delete all the data from local storage? This action cannot be undone!", "Warning!", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                {
                    int result = inventoryViewModel.DeleteAllUploadData();

                    if (result > 0)
                    {
                        ModernDialog.ShowMessage("Upload Data sucessfully deleted from local storage", "Success", MessageBoxButton.OK);
                        this.InventoryGrid.ItemsSource = null;
                        this.InventoryGrid.Items.Refresh();
                    }
                    this.DataContext = inventoryViewModel;
                }

            }
            catch (Exception ex)
            {
                ModernDialog.ShowMessage(ex.Message, "Error!", MessageBoxButton.OK);
            }
        }

        #endregion

        #region Pagination

        private void btnNext_Click(object sender, System.EventArgs e)
        {
            try
            {
                //this.recordCounter += Constants.NumberOfRecordsPerPage;
                //this.LoadInventoryData(this.minIdValue, this.maxIdValue, string.Empty, string.Empty, false, Constants.NumberOfRecordsPerPage);
                //if (_inventoryList.Count > 0 && _inventoryList != null)
                //{
                //    this.minIdValue = _inventoryList.Min(Inventory => Inventory.UploadDataId);
                //    this.maxIdValue = _inventoryList.Max(Inventory => Inventory.UploadDataId);
                //}

                this.recordCounter += Constants.NumberOfRecordsPerPage;
                // inventoryViewModel = new InventoryViewModel();
                List<Inventory> minMaxIdValuesForPager = (List<Inventory>)inventoryViewModel.GetMinAndMaxIdValuesForPager(this.minIdValue, SqlOperatorValue.LessThan, string.Empty, false, Constants.NumberOfRecordsPerPage);
                if (minMaxIdValuesForPager.Count > 0 && minMaxIdValuesForPager != null)
                {
                    this.minIdValue = minMaxIdValuesForPager.Min(Inventory => Inventory.UploadDataId);
                    this.maxIdValue = minMaxIdValuesForPager.Max(Inventory => Inventory.UploadDataId);
                }
                this.LoadInventoryData(this.minIdValue, this.maxIdValue, string.Empty, string.Empty, false, Constants.NumberOfRecordsPerPage);
            }
            catch (Exception ex)
            {
                ModernDialog.ShowMessage(ex.Message, "Error!", MessageBoxButton.OK);
            }
        }

        private void btnPrev_Click(object sender, System.EventArgs e)
        {
            try
            {
                this.recordCounter -= Constants.NumberOfRecordsPerPage;
                //  inventoryViewModel = new InventoryViewModel();
                List<Inventory> minMaxIdValuesForPager = (List<Inventory>)inventoryViewModel.GetMinAndMaxIdValuesForPager(this.maxIdValue, SqlOperatorValue.GreaterThan, string.Empty, false, Constants.NumberOfRecordsPerPage);
                if (minMaxIdValuesForPager.Count > 0 && minMaxIdValuesForPager != null)
                {
                    this.minIdValue = minMaxIdValuesForPager.Min(Inventory => Inventory.UploadDataId);
                    this.maxIdValue = minMaxIdValuesForPager.Max(Inventory => Inventory.UploadDataId);
                }
                this.LoadInventoryData(this.minIdValue, this.maxIdValue, string.Empty, string.Empty, false, Constants.NumberOfRecordsPerPage);
            }
            catch (Exception ex)
            {
                ModernDialog.ShowMessage(ex.Message, "Error!", MessageBoxButton.OK);
            }
        }

        private void btnFirst_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                this.TextBlockPages.Text = "1";
                this.LoadInventoryData(string.Empty);
            }
            catch (Exception ex)
            {
                ModernDialog.ShowMessage(ex.Message, "Error!", MessageBoxButton.OK);
            }
        }

        private void btnLast_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var _numberofRecords = Convert.ToInt32(recordCount % Constants.NumberOfRecordsPerPage);

                List<Inventory> minMaxIdValuesForPager = (List<Inventory>)inventoryViewModel.GetMinAndMaxIdValuesForPager(this.minIdValue, SqlOperatorValue.GreaterThan, string.Empty, true, (_numberofRecords > 0) ? _numberofRecords : Constants.NumberOfRecordsPerPage);
                if (minMaxIdValuesForPager.Count > 0 && minMaxIdValuesForPager != null)
                {
                    this.minIdValue = minMaxIdValuesForPager.Min(Inventory => Inventory.UploadDataId);
                    this.maxIdValue = minMaxIdValuesForPager.Max(Inventory => Inventory.UploadDataId);
                }

                this.LoadInventoryData(this.minIdValue, this.maxIdValue, SqlOperatorValue.GreaterThan, string.Empty, true, (_numberofRecords > 0) ? _numberofRecords : Constants.NumberOfRecordsPerPage);
                this.TextBlockPages.Text = Math.Ceiling((string.Empty.Length > 0) ? (Convert.ToDecimal(this.recordCount) / Constants.NumberOfRecordsPerPage) : (this.recordCount / Constants.NumberOfRecordsPerPage)).ToString();
            }
            catch (Exception ex)
            {
                ModernDialog.ShowMessage(ex.Message, "Error!", MessageBoxButton.OK);
            }
        }

        private void LoadInventoryData(long? MinIdValue, long? MaxIdValue, string OperatorValue, string SearchText, bool GetLastRecords, int NumberOfRecords)
        {
            try
            {
                Application.Current.Dispatcher.Invoke(DispatcherPriority.Normal, new Action(() =>
                  {
                      this.BusyBar.IsBusy = true;
                  }));

                ThreadStart work = () =>
                {
                    _inventoryList = new List<Inventory>(inventoryViewModel.GetUploadData(MinIdValue, MaxIdValue, OperatorValue, SearchText, NumberOfRecords));
                    // this._updatedInventoryList.AddRange(_inventoryList);
                    foreach (Inventory _inv in _inventoryList)
                    {
                        Inventory InItem = _updatedInventoryList.Find(item => item.UploadDataId == _inv.UploadDataId);
                        if (InItem == null) // check item isn't null
                        {
                            _updatedInventoryList.Add(_inv);
                        }
                    }

                    Application.Current.Dispatcher.BeginInvoke(new Action(() => CommandManager.InvalidateRequerySuggested()));
                    Application.Current.Dispatcher.Invoke(DispatcherPriority.ContextIdle, new Action(() =>
                  {
                      if (GetLastRecords)
                          this.recordCounter = this.recordCount;
                      this.SetMaxAndMinIdValue((List<Inventory>)_inventoryList);
                      this.InventoryGrid.ItemsSource = CollectionViewSource.GetDefaultView(_inventoryList);
                      this.InventoryGrid.Items.Refresh();
                      this.DataContext = inventoryViewModel;
                      this.BusyBar.IsBusy = false;
                  }));
                };
                new Thread(work).Start();
            }
            catch (Exception ex)
            {
                ModernDialog.ShowMessage(ex.Message, "Error!", MessageBoxButton.OK);
            }
        }

        private void SetMaxAndMinIdValue(List<Inventory> InventoryList)
        {

            long _recordCount = recordCount;
            //if (string.Empty.Length > 0)
            //{
            //    _recordCount = searchedRecordCount;
            //}
            //else
            //{
            //    _recordCount = recordCount;
            //}
            if (this.recordCounter >= _recordCount)
            {
                this.btnNext.IsEnabled = false;
                this.btnLast.IsEnabled = false;
                this.btnFirst.IsEnabled = true;
                this.btnPrev.IsEnabled = true;
            }

            if (_recordCount <= Constants.NumberOfRecordsPerPage)
            {
                this.btnNext.IsEnabled = false;
                this.btnPrev.IsEnabled = false;
                this.btnFirst.IsEnabled = false;
                this.btnLast.IsEnabled = false;
            }

            if (this.recordCounter <= Constants.NumberOfRecordsPerPage)
            {
                this.btnPrev.IsEnabled = false;
                this.btnFirst.IsEnabled = false;
            }

            if (this.recordCounter > Constants.NumberOfRecordsPerPage)
            {
                this.btnPrev.IsEnabled = true;
                this.btnFirst.IsEnabled = true;
            }

            if (_recordCount > this.recordCounter)
            {
                this.btnNext.IsEnabled = true;
                this.btnLast.IsEnabled = true;
            }
            this.TextBlockPages.Text = Math.Ceiling(Convert.ToDecimal(this.recordCounter) / Constants.NumberOfRecordsPerPage).ToString();
        }

        #endregion


    }
}
