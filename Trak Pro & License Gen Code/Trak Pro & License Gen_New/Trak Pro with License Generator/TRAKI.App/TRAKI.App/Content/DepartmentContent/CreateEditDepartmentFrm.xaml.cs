﻿using FirstFloor.ModernUI.Windows;
using FirstFloor.ModernUI.Windows.Controls;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TRAKI.App.Common;
using TRAKI.App.Interfaces;
using TRAKI.App.Model;
using TRAKI.App.ViewModel;

namespace TRAKI.App.Content.ClosedBuildingContent
{
    /// <summary>
    /// Interaction logic for CreateEditDepartment.xaml
    /// </summary>
    public partial class CreateEditDepartmentFrm : UserControl, IContent
    {

        private DepartmentViewModel departmentViewModel;
        private int DepartmentId = -1;
        private bool _showConfirmationMsg = true;


        public CreateEditDepartmentFrm()
        {
            InitializeComponent();
        }

        #region Private

        private void LoadDepartmentInfo()
        {
            departmentViewModel = new DepartmentViewModel();
            this.DataContext = departmentViewModel.GetDepartments(this.DepartmentId, null);
        }

        private void Submit_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Department department = ((ObservableCollection<Department>)(sender as Button).DataContext)[0];
                string message = string.Empty;
                var result = departmentViewModel.InsertUpdateDepartment(department);
                if (result == 1)
                {
                    message = "Department Successfully Created";
                }
                if (result == 2)
                {
                    message = "Department Successfully updated";
                }
                ModernDialog.ShowMessage(message, "Success", MessageBoxButton.OK);
                this._showConfirmationMsg = false;
                NavigationCommands.GoToPage.Execute("/Content/DepartmentContent/DepartmentDetailFrm.xaml#" + null, this);
            }
            catch (Exception ex)
            {
                if (ex.HResult == -2147467259)
                {
                    this.TextDepartmentNumber.Focus();
                    ModernDialog.ShowMessage("Department Code already exists, Please try another Code!", "Error!", MessageBoxButton.OK);
                }
                else
                {
                    ModernDialog.ShowMessage(ex.Message, "Error!", MessageBoxButton.OK);

                }
            }
        }

        private void TextDepartmentNumber_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = IsMatching.Check(e.Text);

        }
        private void TextDepartmentDescription_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = IsMatching.Check(e.Text);

        }

        #endregion

        #region IContent



        public void OnFragmentNavigation(FirstFloor.ModernUI.Windows.Navigation.FragmentNavigationEventArgs e)
        {
            this.DepartmentId = -1;
            if (!string.IsNullOrEmpty(e.Fragment))
            {
                this.DepartmentId = Convert.ToInt32(e.Fragment);
            }
            this.LoadDepartmentInfo();
        }

        public void OnNavigatedFrom(FirstFloor.ModernUI.Windows.Navigation.NavigationEventArgs e)
        {
            // throw new NotImplementedException();
        }

        public void OnNavigatedTo(FirstFloor.ModernUI.Windows.Navigation.NavigationEventArgs e)
        {
            this._showConfirmationMsg = true;
        }

        public void OnNavigatingFrom(FirstFloor.ModernUI.Windows.Navigation.NavigatingCancelEventArgs e)
        {
            if (this._showConfirmationMsg)
            {
                string message = "Are you sure you want to leave?";
                if (e.NavigationType == FirstFloor.ModernUI.Windows.Navigation.NavigationType.Refresh)
                {
                    message = "Are you sure you want to refresh the page?";
                }
                var confirm = ModernDialog.ShowMessage(message, "Warning!", MessageBoxButton.YesNo);
                if (confirm == MessageBoxResult.No)
                    e.Cancel = true;
            }

        }
        #endregion


    }
}
