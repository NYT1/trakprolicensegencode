﻿using FirstFloor.ModernUI.Windows.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TRAKI.App.Common;
using TRAKI.App.Model;
using TRAKI.App.Utilities;
using TRAKI.App.ViewModel;
using TRAKI.App.ViewModel.Common;

namespace TRAKI.App.Content.CommonContent
{
    /// <summary>
    /// Interaction logic for PreviousLocationPopUpDialog.xaml
    /// </summary>
    public partial class UCSystemControl : UserControl
    {
        string connectionString = string.Empty;
        bool isSuccess;

        private bool isStartupDialog;

        public bool IsSuccess
        {
            get { return isSuccess; }
            set { isSuccess = value; }
        }

        public UCSystemControl(bool IsStartUpDialog)
        {
            try
            {
                InitializeComponent();
                this.isStartupDialog = IsStartUpDialog;
                this.btnClose.Visibility = Visibility.Collapsed;
                this.DataContext = new SettingsViewModel();
            }
            catch (Exception ex)
            {
                ErrorLogging.LogException("Error: " + ex.Message + " {0}");
                ModernDialog.ShowMessage("Some error occured, please try again!", "Error!", MessageBoxButton.OK);
            }
        }

        public UCSystemControl()
        {
            try
            {
                InitializeComponent();
                this.btnCloseDialog.Visibility = Visibility.Collapsed;
                this.isStartupDialog = this.IsSuccess = false;
                this.DataContext = new SettingsViewModel();
            }
            catch (Exception ex)
            {
                ErrorLogging.LogException("Error: " + ex.Message + " {0}");
                ModernDialog.ShowMessage("Some error occured, please try again!", "Error!", MessageBoxButton.OK);

            }
        }

        #region Private

        private void btnBrowse_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                // Create OpenFileDialog
                Microsoft.Win32.OpenFileDialog dialog = new Microsoft.Win32.OpenFileDialog();
                dialog.Filter = "Access DB File (*.mdb)|*.mdb";
                dialog.InitialDirectory = @"C:\";
                dialog.Title = "Please select a Databse file.";
                dialog.Multiselect = false;
                if (dialog.ShowDialog() != null)
                {
                    this.TextDBFilePath.Text = dialog.FileName;
                }
            }
            catch (Exception ex)
            {
                ErrorLogging.LogException("Error: " + ex.Message + " {0}");
                ModernDialog.ShowMessage("Some error occured, please try again!", "Error!", MessageBoxButton.OK);

            }
        }

        private void btnChangeUpdateDBPath_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (!this.isStartupDialog)
                {

                    if (!this.ShowMessageDialog("You are attempting to change the database Path, the Action will be saved and can not be undone." + Environment.NewLine + "You will be logged out of the application and any pending workig will be lost." + Environment.NewLine + "Are you sure you want to Continue?", "Warning!"))
                        return;
                }
                SettingsViewModel settingViewModel = new SettingsViewModel();
                if (settingViewModel.UpdateDBConnectionFile(connectionString))
                {
                    this.statusMessage.Text = "Database Path Updated Successfully";
                    this.statusMessage.Foreground = Brushes.Green;
                    this.IsSuccess = true;
                    if (!this.isStartupDialog)
                    {
                        ModernDialog.ShowMessage("The Database path has been Successfully Changed, You need to loggin again to continue working!", "Success!", MessageBoxButton.OK);
                        NavigationCommands.GoToPage.Execute(" /Pages/LogoutPage.xaml", this);
                    }
                }
                else
                {
                    this.statusMessage.Text = "Database Path Updation Failed!";
                }

                this.statusMessage.Visibility = Visibility.Visible;
                // }
            }

            catch (Exception ex)
            {
                ErrorLogging.LogException("Error: " + ex.Message + " {0}");
                this.statusMessage.Text = "Error while Updating the Database Path!" + ex.Message;
            }

        }

        private bool ShowMessageDialog(string Message, string Title)
        {
            ModernDialog dialog = new ModernDialog();
            dialog.Title = Title;
            dialog.OkButton.Content = "Yes";
            dialog.CancelButton.Content = "No";
            dialog.Content = Message;
            dialog.Buttons = new Button[] { dialog.OkButton, dialog.CancelButton };
            dialog.ShowDialog();
            return dialog.DialogResult.Value;
        }

        private void btnTestConnection_Click(object sender, RoutedEventArgs e)
        {
            SettingsViewModel settingViewModel = new SettingsViewModel();
            try
            {
                if (settingViewModel.TestDBConnection(this.TextDBFilePath.Text, this.TextDBPassword.Password, ref connectionString))
                {
                    this.statusMessage.Text = "Database Connection Test Successfull";
                    this.statusMessage.Foreground = Brushes.Green;
                }
            }
            catch (Exception ex)
            {
                if (ex.HResult == -2147467259)
                    this.statusMessage.Text = "Error: " + this.TextDBFilePath.Text + " is Invalid File Name or Path ";
                else
                    this.statusMessage.Text = "Error: " + ex.Message;
                this.statusMessage.Foreground = Brushes.Red;
            }
            this.statusMessage.Visibility = Visibility.Visible;
            this.DataContext = settingViewModel;
        }

        private void btnCloseDialog_Click(object sender, RoutedEventArgs e)
        {
            (sender as Button).IsCancel = true;
        }

        private void TextDBFilePath_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = IsMatching.Check(e.Text);
        }

        private void Label_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = IsMatching.Check(e.Text);
        }

        #endregion
    }
}
