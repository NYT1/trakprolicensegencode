﻿using FirstFloor.ModernUI.Windows.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TRAKI.App.Common;
using TRAKI.App.Model;
using TRAKI.App.ViewModel;

namespace TRAKI.App.Content.CommonContent
{
    /// <summary>
    /// Interaction logic for DepartmentPopUpDialog.xaml
    /// </summary>
    public partial class DepartmentPopUpDialog : ModernDialog
    {
        private string _departmentDialogResult;
        private int pageIndex = 1;
        private DepartmentViewModel departmentViewModel;
        private List<Department> _departmentList = new List<Department>();

        public DepartmentPopUpDialog()
        {
            InitializeComponent();
            this.Loaded += OnLoaded;
        }

        #region Private

        void OnLoaded(object sender, RoutedEventArgs e)
        {
            this.LoadDepartmentList();

        }

        private void LoadDepartmentList()
        {
            try
            {
                departmentViewModel = new DepartmentViewModel();
                _departmentList = new List<Department>(departmentViewModel.GetDepartments(null, -1).ToList());
                foreach (var Department in _departmentList)
                {
                    Department.IsActive = Common.BooleanValue.False;
                }
                this.DepartmentDataGrid.ItemsSource = CollectionViewSource.GetDefaultView(_departmentList.Take(Constants.NumberOfRecordsPerPage));
                this.EmptyMessageBlock.DataContext = departmentViewModel;
                this.DepartmentDataGrid.Items.Refresh();
                if (departmentViewModel.RecordCount == 0)
                    this.TextBlockPages.Text = "0";
                else
                    this.TextBlockPages.Text = "1";
                this.Navigate((int)PagingMode.First);
                this.DataContext = departmentViewModel;
            }
            catch (Exception ex)
            {
                ModernDialog.ShowMessage(ex.Message, "Error!", MessageBoxButton.OK);
            }
        }

        private void DepartmentDataGrid_SelectionChanged(object sender, RoutedEventArgs e)
        {
            try
            {
                var datagrid = sender as DataGrid;
                ((Department)datagrid.SelectedItem).IsActive = BooleanValue.True;
                this.DepartmentDialogResult = ((Department)datagrid.SelectedItem).DepartmentNumber + "•" + ((Department)datagrid.SelectedItem).DepartmentDescription;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void SearchTextBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = IsMatching.Check(e.Text);
        }

        #endregion

        #region search

        private void SearchTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                string searchText = ((TextBox)sender).Text.Trim();
                if (!string.IsNullOrEmpty(searchText))
                    return;
                this.LoadDepartmentList();
            }
            catch (Exception ex)
            {
                ModernDialog.ShowMessage("Some error occured, please try again! " + ex.Message, "Error!", MessageBoxButton.OK);
            }
        }

        private void btnSearch_Click(object sender, RoutedEventArgs e)
        {
            string searchText = this.SearchTextBox.Text.Trim();
            if (string.IsNullOrEmpty(searchText))
                return;
            try
            {
                this.LoadSearchData(searchText);
            }
            catch (Exception ex)
            {
                ModernDialog.ShowMessage("Some error occured, please try again! " + ex.Message, "Error!", MessageBoxButton.OK);
            }
        }

        private void SearchTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                string searchText = ((TextBox)sender).Text.Trim();
                if (e.Key == Key.Enter)
                    this.LoadSearchData(searchText);
            }
            catch (Exception ex)
            {
                ModernDialog.ShowMessage("Some error occured, please try again! " + ex.Message, "Error!", MessageBoxButton.OK);
            }
        }


        private void LoadSearchData(string SearchText)
        {
            try
            {
                departmentViewModel = new DepartmentViewModel();
                _departmentList = new List<Department>(departmentViewModel.SearchDepartment(SearchText, null));
                foreach (var Department in _departmentList)
                {
                    Department.IsActive = Common.BooleanValue.False;
                }
                this.DepartmentDataGrid.ItemsSource = CollectionViewSource.GetDefaultView(_departmentList.Take(Constants.NumberOfRecordsPerPage));
                this.DepartmentDataGrid.Items.Refresh();
                if (departmentViewModel.RecordCount == 0)
                    this.TextBlockPages.Text = "0";
                else
                    this.TextBlockPages.Text = "1";
                this.Navigate((int)PagingMode.First);
                this.DataContext = departmentViewModel;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        #endregion

        #region Pagination

        private void btnFirst_Click(object sender, System.EventArgs e)
        {

            Navigate((int)PagingMode.First);
            this.TextBlockPages.Text = "1";
        }

        private void btnNext_Click(object sender, System.EventArgs e)
        {
            Navigate((int)PagingMode.Next);
            this.TextBlockPages.Text = this.pageIndex.ToString();
        }

        private void btnPrev_Click(object sender, System.EventArgs e)
        {
            Navigate((int)PagingMode.Previous);
            this.TextBlockPages.Text = this.pageIndex.ToString();
        }

        private void btnLast_Click(object sender, System.EventArgs e)
        {
            Navigate((int)PagingMode.Last);
            this.TextBlockPages.Text = this.pageIndex.ToString();
        }

        //private void cbNumberOfRecords_SelectionChanged(object sender, SelectionChangedEventArgs e)
        //{
        //    Navigate((int)PagingMode.PageCountChange);
        //}

        private void Navigate(int mode)
        {
            int count;
            switch (mode)
            {
                case (int)PagingMode.Next:
                    btnPrev.IsEnabled = true;
                    btnFirst.IsEnabled = true;
                    if (_departmentList.Count >= (pageIndex * Constants.NumberOfRecordsPerPage))
                    {

                        if (_departmentList.Skip(pageIndex * Constants.NumberOfRecordsPerPage).Take(Constants.NumberOfRecordsPerPage).Count() == 0)
                        {
                            this.DepartmentDataGrid.ItemsSource = null;
                            this.DepartmentDataGrid.ItemsSource = _departmentList.Skip((pageIndex * Constants.NumberOfRecordsPerPage) - Constants.NumberOfRecordsPerPage).Take(Constants.NumberOfRecordsPerPage);
                            count = (pageIndex * Constants.NumberOfRecordsPerPage) + (_departmentList.Skip(pageIndex * Constants.NumberOfRecordsPerPage).Take(Constants.NumberOfRecordsPerPage)).Count();
                        }
                        else
                        {
                            this.DepartmentDataGrid.ItemsSource = null;
                            this.DepartmentDataGrid.ItemsSource = _departmentList.Skip(pageIndex * Constants.NumberOfRecordsPerPage).Take(Constants.NumberOfRecordsPerPage);
                            count = (pageIndex * Constants.NumberOfRecordsPerPage) + (_departmentList.Skip(pageIndex * Constants.NumberOfRecordsPerPage).Take(Constants.NumberOfRecordsPerPage)).Count();
                            pageIndex++;
                        }

                        if ((pageIndex * Constants.NumberOfRecordsPerPage) >= _departmentList.Count)
                        {
                            btnNext.IsEnabled = false;
                            btnLast.IsEnabled = false;
                        }

                    }
                    else
                    {
                        btnNext.IsEnabled = false;
                        btnLast.IsEnabled = false;
                    }

                    break;
                case (int)PagingMode.Previous:
                    btnNext.IsEnabled = true;
                    btnLast.IsEnabled = true;
                    pageIndex -= 1;

                    if (pageIndex >= 1)
                    {
                        this.DepartmentDataGrid.ItemsSource = null;
                        if (pageIndex == 1)
                        {
                            btnPrev.IsEnabled = false;
                            btnFirst.IsEnabled = false;
                            this.DepartmentDataGrid.ItemsSource = _departmentList.Take(Constants.NumberOfRecordsPerPage);
                            count = _departmentList.Take(Constants.NumberOfRecordsPerPage).Count();
                        }
                        else
                        {
                            this.DepartmentDataGrid.ItemsSource = _departmentList.Skip((pageIndex - 1) * Constants.NumberOfRecordsPerPage).Take(Constants.NumberOfRecordsPerPage);
                            count = Math.Min((pageIndex - 1) * Constants.NumberOfRecordsPerPage, _departmentList.Count);
                        }
                    }
                    else
                    {
                        btnPrev.IsEnabled = false;
                        btnFirst.IsEnabled = false;
                    }

                    if (_departmentList.Count <= Constants.NumberOfRecordsPerPage)
                    {
                        btnNext.IsEnabled = false;
                        btnLast.IsEnabled = false;
                        btnPrev.IsEnabled = false;
                        btnFirst.IsEnabled = false;
                    }
                    break;

                case (int)PagingMode.First:
                    pageIndex = 2;
                    Navigate((int)PagingMode.Previous);
                    break;
                case (int)PagingMode.Last:
                    pageIndex = (_departmentList.Count / Constants.NumberOfRecordsPerPage);
                    Navigate((int)PagingMode.Next);
                    break;

                case (int)PagingMode.PageCountChange:
                    pageIndex = 1;
                    this.DepartmentDataGrid.ItemsSource = null;
                    this.DepartmentDataGrid.ItemsSource = _departmentList.Take(Constants.NumberOfRecordsPerPage);
                    count = (_departmentList.Take(Constants.NumberOfRecordsPerPage)).Count();
                    btnNext.IsEnabled = true;
                    btnPrev.IsEnabled = true;
                    break;
            }
        }

        #endregion

        public string DepartmentDialogResult
        {
            get { return _departmentDialogResult; }
            set { _departmentDialogResult = value; }
        }

    }
}
