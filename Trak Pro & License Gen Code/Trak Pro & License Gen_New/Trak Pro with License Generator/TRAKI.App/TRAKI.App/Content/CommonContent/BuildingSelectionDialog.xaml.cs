﻿using FirstFloor.ModernUI.Windows.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TRAKI.App.Common;
using TRAKI.App.Model;
using TRAKI.App.Utilities;
using TRAKI.App.ViewModel;

namespace TRAKI.App.Content.CommonContent
{
    /// <summary>
    /// Interaction logic for PreviousLocationPopUpDialog.xaml
    /// </summary>
    public partial class BuildingSelectionDialog : ModernDialog
    {

        private string _discrepancyType;
        private SelectionList<Building> buildingList;
        private IEnumerable<string> _selectedBuildingNumbers;
        private BuildingViewModel buildingViewModel;

        public BuildingSelectionDialog()
        {
            InitializeComponent();
            this.LoadFilterList();
            this.BindData();
        }


        #region Private

        private void BindData()
        {
            buildingViewModel = new BuildingViewModel();
            buildingList = new SelectionList<Building>(buildingViewModel.GetOpenBuildings().ToList());
            myList.ItemsSource = buildingList;// buildingViewModel.GetOpenBuildings();
            buildingList.PropertyChanged += buildingList_PropertyChanged;
            this.OkButton.IsEnabled = buildingList.SelectionCount > 0;
        }

        private void buildingList_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            // the save button is enabled if at least one item is checked
            this.OkButton.IsEnabled = buildingList.SelectionCount > 0;
            SelectedBuildingNumbers = buildingList.GetSelection(elt => elt.Element.BuildingNumber);
            DiscrepancyType = (string)this.ComboDiscrepancyType.SelectedValue;
        }


        private void LoadFilterList()
        {
            this.ComboDiscrepancyType.ItemsSource = null;
            this.ComboDiscrepancyType.ItemsSource = Constants.AppSettings.Where(setting => setting.SettingItem == "DiscrepancyType").OrderBy(p => p.DisplayOrder);
            this.ComboDiscrepancyType.Items.Refresh();
            this.ComboDiscrepancyType.SelectedIndex = 0;
        }


        private void CheckBox_Checked(object sender, RoutedEventArgs e)
        {
            foreach (var item in buildingList)
            {
                item.IsSelected = true;
            }
            myList.ItemsSource = buildingList;
            myList.Items.Refresh();
        }

        private void CheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            foreach (var item in buildingList)
            {
                item.IsSelected = false;
            }
            myList.ItemsSource = buildingList;
            myList.Items.Refresh();
        }
        #endregion


        #region Properties

        public string DiscrepancyType
        {
            get { return _discrepancyType; }
            set { _discrepancyType = value; }
        }
        public IEnumerable<string> SelectedBuildingNumbers
        {
            get { return _selectedBuildingNumbers; }
            set { _selectedBuildingNumbers = value; }
        }

        #endregion
    }
}
