﻿using FirstFloor.ModernUI.Windows;
using FirstFloor.ModernUI.Windows.Controls;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TRAKI.App.Common;
using TRAKI.App.Model;
using TRAKI.App.ViewModel;

namespace TRAKI.App.Content.ManufacturerContent
{
    /// <summary>
    /// Interaction logic for CreateEditManufacturerFrm.xaml
    /// </summary>
    public partial class CreateEditManufacturerFrm : UserControl, IContent
    {

        private ManufacturerViewModel ManufacturerViewModel;

        private int? ManufacturerID = -1;
        private bool _showConfirmationMsg = true;


        public CreateEditManufacturerFrm()
        {
            InitializeComponent();
        }

        #region Private

        private void LoadManufacturerInfo()
        {
            ManufacturerViewModel = new ManufacturerViewModel();
            this.DataContext = ManufacturerViewModel.GetManufacturer(this.ManufacturerID, null);
        }

        private void Submit_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Manufacturer Manufacturer = ((ObservableCollection<Manufacturer>)(sender as Button).DataContext)[0];
                string message = string.Empty;
                var result = ManufacturerViewModel.InsertUpdateManufacturer(Manufacturer);

                if (result == 1)
                {
                    message = "Manufacturer Successfully Created";
                }
                if (result == 2)
                {
                    message = "Manufacturer Successfully updated";
                }
                this._showConfirmationMsg = false;
                ModernDialog.ShowMessage(message, "Success", MessageBoxButton.OK);
                NavigationCommands.GoToPage.Execute("/Content/ManufacturerContent/ManufacturerDetailFrm.xaml#" + null, this);
            }
            catch (Exception ex)
            {
                if (ex.HResult == -2147467259)
                {
                    this.TextManufacturerCode.Focus();
                    ModernDialog.ShowMessage("Manufacturer Code already exists, Please try another Code!", "Error!", MessageBoxButton.OK);
                }
                else
                {
                    ModernDialog.ShowMessage(ex.Message, "Error!", MessageBoxButton.OK);

                }
            }
        }

        private void TextManufacturerCode_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = IsMatching.Check(e.Text);

        }

        private void TextManufacturerName_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = IsMatching.Check(e.Text);

        }
        #endregion

        #region IContent

        public void OnFragmentNavigation(FirstFloor.ModernUI.Windows.Navigation.FragmentNavigationEventArgs e)
        {
            this.ManufacturerID = -1;
            if (!string.IsNullOrEmpty(e.Fragment))
            {
                this.ManufacturerID = Convert.ToInt32(e.Fragment);
            }
            this.LoadManufacturerInfo();
        }

        public void OnNavigatedFrom(FirstFloor.ModernUI.Windows.Navigation.NavigationEventArgs e)
        {
        }

        public void OnNavigatedTo(FirstFloor.ModernUI.Windows.Navigation.NavigationEventArgs e)
        {
            this._showConfirmationMsg = true;
        }

        public void OnNavigatingFrom(FirstFloor.ModernUI.Windows.Navigation.NavigatingCancelEventArgs e)
        {

            if (this._showConfirmationMsg)
            {
                string message = "Are you sure you want to leave?";
                if (e.NavigationType == FirstFloor.ModernUI.Windows.Navigation.NavigationType.Refresh)
                {
                    message = "Are you sure you want to refresh the page?";
                }
                var confirm = ModernDialog.ShowMessage(message, "Warning!", MessageBoxButton.YesNo);
                if (confirm == MessageBoxResult.No)
                    e.Cancel = true;
            }

        }

        #endregion


    }
}
