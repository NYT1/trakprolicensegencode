﻿using FirstFloor.ModernUI.Windows;
using FirstFloor.ModernUI.Windows.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using TRAKI.App.Common;
using TRAKI.App.Model;
using TRAKI.App.ViewModel;
using TRAKI.App.ViewModel.Common;

namespace TRAKI.App.Content.ArchiveDispositionContent
{
    /// <summary>
    /// Interaction logic for ArchiveAssetsFrm.xaml
    /// </summary>
    public partial class ArchiveAssetsFrm : UserControl, IContent
    {
        private long recordCounter = 0;//1;
        private long recordCount = 0;
        private long minIdValue = 0;
        private long maxIdValue = 0;
        private ArchiveDispositionViewModel archiveDispositionViewModel;
        List<Asset> _assetList = new List<Asset>();
        
        public ArchiveAssetsFrm()
        {
            InitializeComponent();
            this.DataContext = new ArchiveDispositionViewModel();

        }

      
        #region Private

        private void LoadAssetList(string FilterValue, string SearchText)
        {
            try
            {
                archiveDispositionViewModel = new ArchiveDispositionViewModel();
                Application.Current.Dispatcher.Invoke(DispatcherPriority.Normal, new Action(() =>
                 {
                     this.BusyBar.IsBusy = true;
                 }));

                ThreadStart work = () =>
                 {
                     _assetList = new List<Asset>(archiveDispositionViewModel.GetDisposedAssetList(0, null, SqlOperatorValue.GreaterThan, FilterValue, SearchText, Constants.NumberOfRecordsPerPage, 0).ToList());
                     recordCount = archiveDispositionViewModel.GetDisposedAssetRecordCount(SearchText, FilterValue, 0);
                     Application.Current.Dispatcher.BeginInvoke(new Action(() => CommandManager.InvalidateRequerySuggested()));
                     Application.Current.Dispatcher.Invoke(DispatcherPriority.ContextIdle, new Action(() =>
                  {
                      archiveDispositionViewModel.RecordCount = recordCount;
                      this.recordCounter = Constants.NumberOfRecordsPerPage;
                      this.SetMaxAndMinIdValue(_assetList);
                      if (_assetList.Count > 0 && _assetList != null)
                      {
                          this.minIdValue = _assetList.Min(asset => asset.AssetId);
                          this.maxIdValue = _assetList.Max(asset => asset.AssetId);
                      }
                      this.DataContext = archiveDispositionViewModel;
                      this.AssetDispositionDataGrid.ItemsSource = CollectionViewSource.GetDefaultView(_assetList);
                      this.AssetDispositionDataGrid.Items.Refresh();
                      if (archiveDispositionViewModel.RecordCount == 0)
                          this.TextBlockPages.Text = "0";
                      else
                          this.TextBlockPages.Text = "1";
                      this.BusyBar.IsBusy = false;
                  }));
                 };
                new Thread(work).Start();
            }
            catch (Exception ex)
            {
                ErrorLogging.LogException("Error: " + ex.Message + " {0}");
                ModernDialog.ShowMessage(ex.Message, "Error!", MessageBoxButton.OK);
            }
        }

        private void LoadFilterList()
        {
            this.ComboFilterBy.ItemsSource = null;
            this.ComboFilterBy.ItemsSource = Constants.AppSettings.Where(setting => setting.SettingItem == "AssetFilterBy").OrderBy(p => p.DisplayOrder);// new SettingsViewModel().GetFilterByList();
            this.ComboFilterBy.Items.Refresh();
            this.ComboFilterBy.SelectedIndex = 0;
        }

        private void ComboFilterBy_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (this.ComboFilterBy.SelectedValue != null)
                {
                    this.LoadAssetList((string)this.ComboFilterBy.SelectedValue, this.SearchTextBox.Text.Trim());
                }
            }
            catch (Exception ex)
            {
                ErrorLogging.LogException("Error: " + ex.Message + " {0}");
                ModernDialog.ShowMessage(ex.Message, "Error!", MessageBoxButton.OK);
            }

        }

        private void btnArchiveDisposedAssets_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                archiveDispositionViewModel = new ArchiveDispositionViewModel();
                var confirm = ModernDialog.ShowMessage("Are you sure you want to Archive those assets that are marked for Auction or Disposition?. This action will be saved and cannot be undone!", "Confirmation!", MessageBoxButton.YesNo);
                if (confirm == MessageBoxResult.Yes)
                {
                    var result = archiveDispositionViewModel.ArchiveDisposition();
                    if (result > 0)
                    {
                        ModernDialog.ShowMessage("Disposed Assets successfully Archived", "Success", MessageBoxButton.OK);
                        this.UpdateDataGrid();
                         
                    }
                    else
                    {
                        ModernDialog.ShowMessage("Error while Archiving the Assets!", "Error!", MessageBoxButton.OK);
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorLogging.LogException("Error: " + ex.Message + " {0}");
                ModernDialog.ShowMessage(ex.Message, "Error!", MessageBoxButton.OK);
            }
        }
        
        private void SearchTextBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = IsMatching.Check(e.Text);
        }

        private void btnRefreshDisposedAssets_Click(object sender, RoutedEventArgs e)
        {
            this.LoadFilterList();
        }

        #endregion


        #region Search Region

        private void btnSearch_Click(object sender, RoutedEventArgs e)
        {
            string searchText = this.SearchTextBox.Text.Trim();
            if (string.IsNullOrEmpty(searchText))
                return;
            try
            {
                this.LoadSearchData(searchText, (string)this.ComboFilterBy.SelectedValue);
            }
            catch (Exception ex)
            {
                ModernDialog.ShowMessage("Some error occured, please try again! " + ex.Message, "Error!", MessageBoxButton.OK);
            }
        }

        private void SearchTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            string searchText = ((TextBox)sender).Text.Trim();
            if (!string.IsNullOrEmpty(searchText))
                return;
            try
            {
                this.LoadSearchData(searchText, (string)this.ComboFilterBy.SelectedValue);
            }
            catch (Exception ex)
            {
                ModernDialog.ShowMessage("Some error occured, please try again! " + ex.Message, "Error!", MessageBoxButton.OK);
            }

        }

        private void SearchTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                // if user didn't press Enter, do nothing
                if (!e.Key.Equals(Key.Enter)) return;
                else
                {
                    string searchText = ((TextBox)sender).Text.Trim();
                    if (string.IsNullOrEmpty(searchText))
                        return;

                    this.LoadSearchData(searchText, (string)this.ComboFilterBy.SelectedValue);
                }
            }
            catch (Exception ex)
            {
                ModernDialog.ShowMessage("Some error occured, please try again! " + ex.Message, "Error!", MessageBoxButton.OK);
            }
        }

        private void LoadSearchData(string SearchText, string FilterValue)
        {
            try
            {
                archiveDispositionViewModel = new ArchiveDispositionViewModel();
                Application.Current.Dispatcher.Invoke(DispatcherPriority.Normal, new Action(() =>
                 {
                     this.BusyBar.IsBusy = true;
                 }));

                ThreadStart work = () =>
                {
                    _assetList = new List<Asset>(archiveDispositionViewModel.GetDisposedAssetList(0, null, SqlOperatorValue.GreaterThan, FilterValue, SearchText, Constants.NumberOfRecordsPerPage, 0)); // new List<User>(userViewModel.GetUser().ToList());
                    //if (_assetList != null && _assetList.Count == Constants.NumberOfRecordsPerPage)
                    this.recordCount = archiveDispositionViewModel.GetDisposedAssetRecordCount(SearchText, FilterValue, 0);
                    Application.Current.Dispatcher.BeginInvoke(new Action(() => CommandManager.InvalidateRequerySuggested()));
                    Application.Current.Dispatcher.Invoke(DispatcherPriority.ContextIdle, new Action(() =>
                    {
                        // else
                        //    assetViewModel.RecordCount = searchedRecordCount = _assetList.Count;
                        archiveDispositionViewModel.RecordCount = this.recordCount;
                        this.recordCounter = Constants.NumberOfRecordsPerPage;
                        this.SetMaxAndMinIdValue(_assetList);
                        this.AssetDispositionDataGrid.ItemsSource = CollectionViewSource.GetDefaultView(_assetList);
                        this.DataContext = archiveDispositionViewModel;
                        this.AssetDispositionDataGrid.Items.Refresh();
                        if (archiveDispositionViewModel.RecordCount == 0)
                            this.TextBlockPages.Text = "0";
                        else
                            this.TextBlockPages.Text = "1";
                        this.BusyBar.IsBusy = false;
                    }));
                };
                new Thread(work).Start();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void UpdateDataGrid()
        {
            this.recordCounter = 0;//1;
            this.recordCount = 0;
            this.minIdValue = 0;
            this.maxIdValue = 0;
            this.LoadFilterList();
        }

        #endregion

        #region Pagination

        private void btnNext_Click(object sender, System.EventArgs e)
        {
            this.recordCounter += Constants.NumberOfRecordsPerPage;
            this.LoadAssetData(this.maxIdValue, null, SqlOperatorValue.GreaterThan, (string)this.ComboFilterBy.SelectedValue, this.SearchTextBox.Text.Trim(), false, Constants.NumberOfRecordsPerPage, true);

        }

        private void btnPrev_Click(object sender, System.EventArgs e)
        {
            this.recordCounter -= Constants.NumberOfRecordsPerPage;
            archiveDispositionViewModel = new ArchiveDispositionViewModel();
            List<Asset> minMaxIdValuesForPager = (List<Asset>)archiveDispositionViewModel.GetMinAndMaxIdValuesForDisposedAssetPager(this.minIdValue, (string)this.ComboFilterBy.SelectedValue, this.SearchTextBox.Text.Trim(), false, Constants.NumberOfRecordsPerPage, 0);
            if (minMaxIdValuesForPager.Count > 0 && minMaxIdValuesForPager != null)
            {
                this.minIdValue = minMaxIdValuesForPager.Min(asset => asset.AssetId);
                this.maxIdValue = minMaxIdValuesForPager.Max(asset => asset.AssetId);
            }
            this.LoadAssetData(this.minIdValue, this.maxIdValue, string.Empty, (string)this.ComboFilterBy.SelectedValue, this.SearchTextBox.Text.Trim(), false, Constants.NumberOfRecordsPerPage, false);
        }

        private void btnFirst_Click(object sender, RoutedEventArgs e)
        {
            this.TextBlockPages.Text = "1";
            this.LoadAssetList((string)this.ComboFilterBy.SelectedValue, this.SearchTextBox.Text.Trim());
        }

        private void btnLast_Click(object sender, RoutedEventArgs e)
        {

            var _numberofRecords = Convert.ToInt32(recordCount % Constants.NumberOfRecordsPerPage);

            List<Asset> minMaxIdValuesForPager = (List<Asset>)archiveDispositionViewModel.GetMinAndMaxIdValuesForDisposedAssetPager(this.minIdValue, (string)this.ComboFilterBy.SelectedValue, this.SearchTextBox.Text.Trim(), true, (_numberofRecords > 0) ? _numberofRecords : Constants.NumberOfRecordsPerPage, 0);
            if (minMaxIdValuesForPager.Count > 0 && minMaxIdValuesForPager != null)
            {
                this.minIdValue = minMaxIdValuesForPager.Min(asset => asset.AssetId);
                this.maxIdValue = minMaxIdValuesForPager.Max(asset => asset.AssetId);
            }

            this.LoadAssetData(this.minIdValue, this.maxIdValue, SqlOperatorValue.LessThan, (string)this.ComboFilterBy.SelectedValue, this.SearchTextBox.Text.Trim(), true, (_numberofRecords > 0) ? _numberofRecords : Constants.NumberOfRecordsPerPage, false);
            this.TextBlockPages.Text = Math.Ceiling((this.SearchTextBox.Text.Trim().Length > 0) ? (Convert.ToDecimal(this.recordCount) / Constants.NumberOfRecordsPerPage) : (this.recordCount / Constants.NumberOfRecordsPerPage)).ToString();
        }

        private void LoadAssetData(long? MinIdValue, long? MaxIdValue, string OperatorValue, string Status, string SearchText, bool GetLastRecords, int NumberOfRecords, Boolean IsNext)
        {
            archiveDispositionViewModel = new ArchiveDispositionViewModel();
            Application.Current.Dispatcher.Invoke(DispatcherPriority.Normal, new Action(() =>
             {
                 this.BusyBar.IsBusy = true;
             }));

            ThreadStart work = () =>
            {
                _assetList = new List<Asset>(archiveDispositionViewModel.GetDisposedAssetList(MinIdValue, MaxIdValue, OperatorValue, Status, SearchText, NumberOfRecords, 0).ToList());
                Application.Current.Dispatcher.BeginInvoke(new Action(() => CommandManager.InvalidateRequerySuggested()));
                Application.Current.Dispatcher.Invoke(DispatcherPriority.ContextIdle, new Action(() =>
              {
                  if (GetLastRecords)
                      this.recordCounter = this.recordCount;
                  if (IsNext)
                  {
                      if (_assetList.Count > 0 && _assetList != null)
                      {
                          this.minIdValue = _assetList.Min(asset => asset.AssetId);
                          this.maxIdValue = _assetList.Max(asset => asset.AssetId);
                      }
                  }
                  this.SetMaxAndMinIdValue(_assetList);
                  this.AssetDispositionDataGrid.ItemsSource = CollectionViewSource.GetDefaultView(_assetList);
                  this.AssetDispositionDataGrid.Items.Refresh();
                  this.BusyBar.IsBusy = false;
              }));
            };
            new Thread(work).Start();
        }

        private void SetMaxAndMinIdValue(List<Asset> AssetList)
        {
            //long _recordCount = 0;
            // if (this.SearchTextBox.Text.Trim().Length > 0)
            // {
            //    _recordCount = searchedRecordCount;
            // }
            //else
            // {
            // _recordCount = recordCount;
            // }
            if (this.recordCounter >= recordCount)
            {
                this.btnNext.IsEnabled = false;
                this.btnLast.IsEnabled = false;
                this.btnFirst.IsEnabled = true;
                this.btnPrev.IsEnabled = true;
            }

            if (recordCount <= Constants.NumberOfRecordsPerPage)
            {
                this.btnNext.IsEnabled = false;
                this.btnPrev.IsEnabled = false;
                this.btnFirst.IsEnabled = false;
                this.btnLast.IsEnabled = false;
            }

            if (this.recordCounter <= Constants.NumberOfRecordsPerPage)
            {
                this.btnPrev.IsEnabled = false;
                this.btnFirst.IsEnabled = false;
            }

            if (this.recordCounter > Constants.NumberOfRecordsPerPage)
            {
                this.btnPrev.IsEnabled = true;
                this.btnFirst.IsEnabled = true;
            }

            if (recordCount > this.recordCounter)
            {
                this.btnNext.IsEnabled = true;
                this.btnLast.IsEnabled = true;
            }
            this.TextBlockPages.Text = Math.Ceiling(Convert.ToDecimal(this.recordCounter) / Constants.NumberOfRecordsPerPage).ToString();
        }

        #endregion

        #region IContent

        public void OnFragmentNavigation(FirstFloor.ModernUI.Windows.Navigation.FragmentNavigationEventArgs e)
        {

        }

        public void OnNavigatedFrom(FirstFloor.ModernUI.Windows.Navigation.NavigationEventArgs e)
        {

        }

        public void OnNavigatedTo(FirstFloor.ModernUI.Windows.Navigation.NavigationEventArgs e)
        {
            this.LoadFilterList();
        }

        public void OnNavigatingFrom(FirstFloor.ModernUI.Windows.Navigation.NavigatingCancelEventArgs e)
        {

        }

        #endregion

       

    }
}

