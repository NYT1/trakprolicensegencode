﻿using FirstFloor.ModernUI.Windows.Controls;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TRAKI.App.Common;
using TRAKI.App.DapperQueries.BuildingQueries;
using TRAKI.App.Interfaces;
using TRAKI.App.Model;
using TRAKI.App.ViewModel;

namespace TRAKI.App.Content.ClosedBuildingContent
{
    /// <summary>
    /// Interaction logic for ClosedBuildingDetailFrm.xaml
    /// </summary>
    public partial class ClosedBuildingDetailFrm : UserControl
    {
        List<Building> OpenBuilding;
        List<Building> ClosedBuilding;

        //string[] OpenIdCollector=new string[]{};
        //string[] ClosedIdCollector=new string[]{};       
        ArrayList OpenIdCollector = new ArrayList();
        ArrayList ClosedIdCollector = new ArrayList();

        protected IDbContext dbContext;
        public ObservableCollection<Building> BuildingList { get; private set; }
        //private ClosedBuildingViewModel buildingViewModel;
        //int pageIndex = 1;
        List<Building> _buildingList = new List<Building>();

        public ClosedBuildingDetailFrm()
        {
            InitializeComponent();

            LoadOpenList();
            LoadClosedList();
        }

        private void LoadOpenList()
        {
            GetConnection getConnection = new GetConnection();
            dbContext = getConnection.InitializeConnection();
            var BuildingQuery = new GetBuildingbyBuildingStatus(true);
            this.OpenBuilding = new ObservableCollection<Building>(dbContext.Execute(BuildingQuery)).ToList();
            this.OpenBuildingList.ItemsSource = OpenBuilding;

        }

        private void LoadClosedList()
        {
            GetConnection getConnection = new GetConnection();
            dbContext = getConnection.InitializeConnection();
            var BuildingQuery = new GetBuildingbyBuildingStatus(false);
            ClosedBuilding = new ObservableCollection<Building>(dbContext.Execute(BuildingQuery)).ToList();
            this.ClosedBuildingList.ItemsSource = ClosedBuilding;

        }

        private void BtnAdd_Click(object sender, RoutedEventArgs e)
        {
            if (OpenBuildingList.SelectedIndex == -1)
            {
                ModernDialog.ShowMessage("Please, select a proper action", "Sorry", MessageBoxButton.OK);

            }
            else
            {
                 
                foreach (Building item in OpenBuildingList.SelectedItems)
                {
                    if (ClosedIdCollector.Count > 0)
                    {
                        if (!ClosedIdCollector.Contains(item.BuildingId))
                            ClosedIdCollector.Add(item.BuildingId);

                    }
                    else
                    {
                        ClosedIdCollector.Add(item.BuildingId);
                    }

                    if (OpenIdCollector.Count > 0)
                    {
                        if (OpenIdCollector.Contains(item.BuildingId))
                            OpenIdCollector.Remove(item.BuildingId);

                    }




                    ClosedBuilding.Add(item);
                    OpenBuilding.RemoveAll(building => building.BuildingId == item.BuildingId);

                }
                //var it = OpenBuilding;
                OpenBuildingList.ItemsSource = OpenBuilding;
                OpenBuildingList.Items.SortDescriptions.Remove(
 new System.ComponentModel.SortDescription("BuildingDescription",
    System.ComponentModel.ListSortDirection.Ascending));
                OpenBuildingList.Items.Refresh();

                ClosedBuildingList.ItemsSource = ClosedBuilding;
                ClosedBuildingList.Items.SortDescriptions.Add(
   new System.ComponentModel.SortDescription("BuildingDescription",
      System.ComponentModel.ListSortDirection.Ascending));
                ClosedBuildingList.Items.Refresh();

            }
        }

        private void bindnewlist()
        {
            OpenBuildingList.ItemsSource = null;
            OpenBuildingList.ItemsSource = OpenBuilding;
            ClosedBuildingList.ItemsSource = null;
            ClosedBuildingList.ItemsSource = ClosedBuilding;
        }

        private void BtnRemove_Click(object sender, RoutedEventArgs e)
        {
            if (ClosedBuildingList.SelectedIndex == -1)
            {
                ModernDialog.ShowMessage("Please, select a proper action", "Sorry", MessageBoxButton.OK);

            }
            else
            {

                foreach (Building item in ClosedBuildingList.SelectedItems)
                {
                    if (OpenIdCollector.Count > 0)
                    {
                        if (!OpenIdCollector.Contains(item.BuildingId))
                            OpenIdCollector.Add(item.BuildingId);

                    }
                    else
                    {
                        OpenIdCollector.Add(item.BuildingId);
                    }

                    if (ClosedIdCollector.Count > 0)
                    {
                        if (ClosedIdCollector.Contains(item.BuildingId))
                            ClosedIdCollector.Remove(item.BuildingId);

                    }

                    OpenBuilding.Add(item);
                    ClosedBuilding.RemoveAll(building => building.BuildingId == item.BuildingId);

                }
                var it = OpenBuilding;
                ClosedBuildingList.ItemsSource = ClosedBuilding;
                ClosedBuildingList.Items.SortDescriptions.Remove(
     new System.ComponentModel.SortDescription("BuildingDescription",
        System.ComponentModel.ListSortDirection.Ascending));
                ClosedBuildingList.Items.Refresh();


                OpenBuildingList.ItemsSource = OpenBuilding;
                OpenBuildingList.Items.SortDescriptions.Add(
  new System.ComponentModel.SortDescription("BuildingDescription",
     System.ComponentModel.ListSortDirection.Ascending));
                OpenBuildingList.Items.Refresh();

            }

        }

        private void BtnSelectAll_Click(object sender, RoutedEventArgs e)
        {
            if (OpenBuildingList.Items.Count <= 0)
            {
                ModernDialog.ShowMessage("No Building found", "Sorry", MessageBoxButton.OK);

            }
            foreach (Building item in OpenBuildingList.Items)
            {
                if (ClosedIdCollector.Count > 0)
                {
                    if (!ClosedIdCollector.Contains(item.BuildingId))
                        ClosedIdCollector.Add(item.BuildingId);

                }
                else
                {
                    ClosedIdCollector.Add(item.BuildingId);
                }

                if (OpenIdCollector.Count > 0)
                {
                    if (OpenIdCollector.Contains(item.BuildingId))
                        OpenIdCollector.Remove(item.BuildingId);

                }
                ClosedBuilding.Add(item);
            }
            OpenBuilding.Clear();
            bindnewlist();
        }

        private void BtnRemoveAll_Click(object sender, RoutedEventArgs e)
        {
            if (ClosedBuildingList.Items.Count <= 0)
            {
                ModernDialog.ShowMessage("No Building found", "Sorry", MessageBoxButton.OK);

            }
            foreach (Building item in ClosedBuildingList.Items)
            {
                if (OpenIdCollector.Count > 0)
                {
                    if (!OpenIdCollector.Contains(item.BuildingId))
                        OpenIdCollector.Add(item.BuildingId);

                }
                else
                {
                    OpenIdCollector.Add(item.BuildingId);
                }

                if (ClosedIdCollector.Count > 0)
                {
                    if (ClosedIdCollector.Contains(item.BuildingId))
                        ClosedIdCollector.Remove(item.BuildingId);

                }
                OpenBuilding.Add(item);
            }
            ClosedBuilding.Clear();
            bindnewlist();
        }

        private void BtnCancel_Click(object sender, RoutedEventArgs e)
        {
            //OpenBuilding = ClosedBuilding = null;         
            //LoadClosedList();
            //LoadOpenList();
            NavigationCommands.GoToPage.Execute("/Content/BuildingContent/BuildingDetailFrm.xaml#" + null, this);
        }

        private void BtnSave_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                GetConnection getConnection = new GetConnection();
                dbContext = getConnection.InitializeConnection();
                int result = 0;
                dynamic buildingQuery = null;
                string message = string.Empty;

                buildingQuery = new UpdateBuildingbyBuildingStatus(OpenIdCollector, ClosedIdCollector);
                result = dbContext.Execute(buildingQuery);
                if (result > 0)
                {
                    ModernDialog.ShowMessage("Records Saved", "Success", MessageBoxButton.OK);// success update operation
                }

                //return result;

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        private void BtnReset_Click(object sender, RoutedEventArgs e)
        {
            OpenBuilding = ClosedBuilding = null;
            LoadClosedList();
            LoadOpenList();
        }
    }
}
